package com.vice_hotel.presenter.helper

import java.text.SimpleDateFormat
import java.util.*
const val SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
class DateUtil {

    companion object {

        val ddMMMyyyy = "dd MMM yyyy"
        val dd_MMM_yyyy = "dd MMM yyyy"

        fun parserServerDateTo(dateType: String, withServerDate: String?): String? {
            if (withServerDate!=null) {
                val dateFormat = SimpleDateFormat(SERVER_DATE_FORMAT)
                dateFormat.timeZone = TimeZone.getTimeZone("UTC")
                val parsedDate = dateFormat.parse(withServerDate)
                val toFormat = SimpleDateFormat(dateType)
                return toFormat.format(parsedDate)
            }
            return ""
        }
    }

}