package com.vice_hotel.presenter.helper


class Constant {
    companion object {
        const val REQUEST_CODE_CAMERA = 100
        const val REQUEST_LOCATION = 101
        const val REQUEST_SELECT_PICTURE = 102
        const val USER_STATUS_NEW = "new"
        const val USER_STATUS_NO_PLAN = "no_plan"
        const val USER_STATUS_HAS_PLAN = "has_plan"
        const val ACTION_DASHBOARD_TO_PLAN_PICKER = "action_dashboard_to_plan_picker"
        const val CARD_VISA = "Visa"
        const val CARD_MASTER = "Mastercard"
        const val REWARD = "rewards"
        const val WEB_FAQApp = "FAQApp"
        const val WEB_HowWisePassWorkApp = "HowWisePassWorkApp"
        const val BRAND_GRAB = "grab"
        const val BRAND_SHOPEE = "shopee"
        const val PROMOTION = "promotion"

    }
}