package com.vice_hotel.presenter.helper

import android.content.Context
import androidx.navigation.fragment.findNavController
import com.facebook.login.LoginManager
import com.vice_hotel.R
import com.vice_hotel.model.data.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.activity.MainActivity

class AppManager {


    companion object {
        val instance: AppManager by lazy {
            AppManager()
        }

        fun logOut(activity: BaseActivity) {
            PreferenceUtils(activity).clear()
            MainActivity.startActivity(activity)
            LoginManager.getInstance().logOut()
        }
    }

    private var appConfig: AppConfig? = null
    private var requestLogin: RequestLogin? = null
    private var searchData = SearchData()

    fun setSearchBrands(brands: ArrayList<Brand>) {
        searchData.brands = brands
    }

    fun setSearchVenueByStatus(searchString: String) {
        searchData.venue_status = searchString
    }

    fun getSearchData() = searchData


    fun getRequestLogin(): RequestLogin {
        if (requestLogin!=null) {
            return requestLogin!!
        }
        return RequestLogin()
    }

    fun setRequestLogin(requestLogin: RequestLogin) {
        this.requestLogin = requestLogin
    }

    fun setAppConfig(appConfig: AppConfig) {
        this.appConfig = appConfig
    }

    fun getAppConfig() = appConfig

    fun getAppConfig(context: Context?, complete: (AppConfig?) -> Unit) {
        if (appConfig==null) {
            getLocationByWifi(context) {
                if (it!=null) {
                    appConfig = AppConfig(
                        latitude = it.lat,
                        longitude = it.lon,
                        country_code = it.countryCode
                    )
                    complete.invoke(appConfig)
                } else {
                    complete.invoke(null)
                }
            }
        } else {
            complete.invoke(appConfig)
        }
    }

    fun getLocationByWifi(context: Context?, complete: (WifiCountry?) -> Unit) {
        val requestCountry = ApiService.createWithoutHost(context,"http://ip-api.com/json/")?.getCountryByIpInternet()
        RetrofitUtil.requestAPIForWifiCountry(context, requestCountry) {
            complete(it)
        }
    }

    fun loginSuccess(context: Context?, account: UserAccount, complete: () -> Unit) {
        if (!account.token.isNullOrEmpty()) {
            PreferenceUtils(context).put(PreferenceUtils.ACCESS_KEY_API, account.token)
            PreferenceUtils(context).put(PreferenceUtils.EMAIL_REGISTRATION, account.email?.trim())
            AppManager.instance.getAppConfig(context) { appConfig ->
                if (appConfig==null) {
                    AppUtil.toast(context, R.string.network_offline, false)
                    return@getAppConfig
                }
                val requestCheckVersion = ApiService.create(context)?.setConfigAndCheckVersion(appConfig)
                RetrofitUtil.requestAPI(context, requestCheckVersion) { config ->
                    complete.invoke()
                }
            }
        }
    }

}