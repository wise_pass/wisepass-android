package com.vice_hotel.presenter.helper.camera

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.view.BaseActivity
import java.io.File
import java.util.ArrayList
import java.util.concurrent.Executors

class GalleryModel: ViewModel() {

    private var data = MutableLiveData<ArrayList<Uri>>()

    fun getImages(): LiveData<ArrayList<Uri>>? = data

    fun queryImagesFromGallery(activity: BaseActivity) {
        val service =  Executors.newSingleThreadExecutor()
        service.run {
            data.postValue(AppUtil.getAllPictureFromGallery(activity))
        }
    }

}