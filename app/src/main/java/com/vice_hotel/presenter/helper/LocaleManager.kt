package com.vice_hotel.presenter.helper

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import java.util.*

class LocaleManager {

    companion object {

        fun getLanguage(context: Context?): String {
            val lang = PreferenceUtils(context).get(PreferenceUtils.APP_LANGUAGE, String.javaClass) as String
            if (lang.isEmpty()) {
                return Configuration(context?.resources?.configuration).locale.language
            }
            return lang
        }

        fun changeLocateContext(context: Context?, lang: String?, callback: (Context?) -> Unit) {
            val locale = Locale(lang)
            Locale.setDefault(locale)
            val res = context?.resources;
            val config = Configuration(res?.configuration)
            config.setLocale(locale)
            PreferenceUtils(context).put(PreferenceUtils.APP_LANGUAGE, lang)
            callback(context?.createConfigurationContext(config))
        }
    }

    class LANGUAGE {
        companion object {
            const val THAILAND = "th"
            const val CHINESE = "zh"
            const val ENGLISH = ""
        }
    }

}