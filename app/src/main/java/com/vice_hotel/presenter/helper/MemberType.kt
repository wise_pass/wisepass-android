package com.vice_hotel.presenter.helper

enum class MemberType() {
    NORMAL,
    DISCOVERY,
    STANDARD,
    PREMIUM;

    companion object {
        fun detect(type: String?) : MemberType = when(type) {
            "Discovery" -> DISCOVERY
            "Standard" -> STANDARD
            "Premium" -> PREMIUM
            else -> NORMAL
        }
    }
}