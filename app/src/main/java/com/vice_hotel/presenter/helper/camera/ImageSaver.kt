package com.vice_hotel.presenter.helper.camera

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

internal class ImageSaver(
/**
 * The JPEG image
 */
private val image: Image,

/**
 * The file we save the image into.
 */
private val file: File
) : Runnable {

    override fun run() {
        val buffer = image.planes[0].buffer
        val bytes = ByteArray(buffer.remaining())
        buffer.get(bytes)
        val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        val bitmapSquare = Bitmap.createBitmap(bitmap, 0,0, bitmap.width, bitmap.width)
        val stream = ByteArrayOutputStream()
        bitmapSquare.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        var output: FileOutputStream? = null
        try {
            output = FileOutputStream(file).apply {
                write(stream.toByteArray())
            }
        } catch (e: IOException) {
            Log.e(TAG, e.toString())
        } finally {
            image.close()
            output?.let {
                try {
                    it.close()
                } catch (e: IOException) {
                    Log.e(TAG, e.toString())
                }
            }
        }
    }

    companion object {
        /**
         * Tag for the [Log].
         */
        private val TAG = "ImageSaver"
    }
}