package com.vice_hotel.presenter.helper

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.graphics.Bitmap
import android.net.Uri
import android.util.Patterns
import android.database.Cursor
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Build
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.vice_hotel.presenter.lib.glide.GlideUtil
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import android.view.animation.AnimationUtils
import android.view.animation.AnimationUtils.loadLayoutAnimation
import android.view.animation.LayoutAnimationController
import android.widget.TextView
import androidx.core.app.ShareCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.vice_hotel.R
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.view.BaseFragment
import java.security.MessageDigest
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.collections.ArrayList


class AppUtil {

    companion object {

        @JvmStatic
        @BindingAdapter("imageUrl")
        fun ImageView.loadImage(url: String?) {
            if (!url.isNullOrEmpty()) GlideUtil.load(context, url.trim(), this)
        }

        @JvmStatic
        @BindingAdapter("imageCircleUrl")
        fun ImageView.loadCircleImage(url: String?) {
            if (!url.isNullOrEmpty()) GlideUtil.loadCircle(context, url.trim(), this)
        }

        fun dpToPx(context: Context?, dp: Int): Float? =
            if (context != null) dp * context.resources.displayMetrics.density else null

        fun toast(context: Context?, s: String?, isShortTime: Boolean) =
            Toast.makeText(context, s, if (isShortTime) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()

        fun toast(context: Context?, resId: Int, isShortTime: Boolean) =
            toast(context, context?.getString(resId), isShortTime)

        fun hideKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun openGooglePlayWithAppID(activity: Activity?, appId: String?) {
            try {
                val link = "market://details?id=$appId"
                activity?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)))
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        fun isNotContainSpecialCharacter(s: String?): Boolean = s?.matches(Regex("[a-zA-Z0-9]*")) == true

        fun isEmailValid(email: String?): Boolean =
            !email.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()

        fun hasNavBar(context: Context?): Boolean? {
            //Emulator
            if (Build.FINGERPRINT.startsWith("generic"))
                return true;

            val id = context?.resources?.getIdentifier("config_showNavigationBar", "bool", "android")
            return id?.compareTo(0) == 1 && context.resources.getBoolean(id)
        }

        fun getAllPictureFromGallery(activity: Activity): ArrayList<Uri> {
            var cursor: Cursor? = null
            var columnIndexData = 0
            val listOfAllImages = ArrayList<Uri>()
            var absolutePathOfImage: String? = null
            val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val projection = arrayOf(MediaStore.MediaColumns.DATA)
            cursor = activity.contentResolver.query(uri, projection, null, null, null)
            columnIndexData = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(columnIndexData)
                listOfAllImages.add(Uri.fromFile(File(absolutePathOfImage!!)))
            }
            cursor.close()
            return listOfAllImages
        }

        fun bitmapToBase64(bitmap: Bitmap): String {
            val outputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            val arr = outputStream.toByteArray()
            return Base64.encodeToString(arr, Base64.DEFAULT)
        }

        fun bitmapToBase64(filePath: String): String {
            val bitmap = BitmapFactory.decodeFile(filePath)
            return Companion.bitmapToBase64(bitmap)
        }

        fun arrayToString(strArray: Array<String>): String = TextUtils.join(", ", strArray)
        fun stringToArray(string: String): List<String> = string.split(",")

        fun convertSecondToTime(second: Int): String {
            var second = second
            second %= (24 * 3600)
            val hour = second / 3600
            second %= 3600
            val minutes = second / 60
            second %= 60
            val seconds = second
            return "${hour}h : ${minutes}m : ${seconds}s"
        }

        fun convertSecondToDynamic(second: Int): String {
            var second = second
            val days = second / (24 * 3600)
            second %= (24 * 3600)
            val hour = second / 3600
            second %= 3600
            val minutes = second / 60
            second %= 60
            val seconds = second
            return "${days}d: ${hour}h : ${minutes}m : ${seconds}s"
        }

        fun getCurrentTime(): String {
            val format = SimpleDateFormat("hh'h':mm'm':ss's'")
            val date = Date()
            return format.format(date)
        }

        fun notifyDataChanged(recyclerView: RecyclerView) {
            val context = recyclerView.context
            val controller = loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
            recyclerView.layoutAnimation = controller
            recyclerView.adapter?.notifyDataSetChanged()
            recyclerView.scheduleLayoutAnimation()
        }

        fun encodeImage(context: Context?, uri: Uri?): String? {
            if (uri != null) {
                val imageStream = context?.contentResolver?.openInputStream(uri)
                var bitmap = BitmapFactory.decodeStream(imageStream)
                val bao = ByteArrayOutputStream()
                val maxHeight = 300
                val maxWidth = 300
                val scale = Math.min((maxHeight.toFloat() / bitmap.width), (maxWidth.toFloat() / bitmap.height))
                val matrix = Matrix()
                matrix.postScale(scale, scale)
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao)
                val b = bao.toByteArray()
                return Base64.encodeToString(b, Base64.DEFAULT)
            }
            return null
        }

        fun checkPlanExist(fragment: BaseFragment, isPlanExist: (Boolean) -> Unit) {
            ViewModelProviders.of(fragment).get(PlanModel::class.java).getPlanBilling(fragment.context)
                .observeOnce(fragment, Observer {
                    when (it.data?.plan != null) {
                        true -> isPlanExist.invoke(true)
                        else -> isPlanExist.invoke(false)
                    }
                })
        }

        fun changeTabsFont(context: Context, tabLayout: TabLayout) {

            val vg = tabLayout.getChildAt(0) as ViewGroup
            val tabsCount = vg.childCount
            for (j in 0 until tabsCount) {
                val vgTab = vg.getChildAt(j) as ViewGroup
                val tabChildsCount = vgTab.childCount
                for (i in 0 until tabChildsCount) {
                    val tabViewChild = vgTab.getChildAt(i)
                    if (tabViewChild is TextView) {
                        tabViewChild.typeface = FontUtil.textRegular(context)
                        tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                    }
                }
            }
        }

        fun generalQRCode(context: Context?, code: String?): Bitmap? {
            try {
                val barcodeEncoder = BarcodeEncoder()
                return barcodeEncoder.encodeBitmap(
                    code,
                    BarcodeFormat.QR_CODE,
                    dpToPx(context, 200)!!.toInt(),
                    dpToPx(context, 200)!!.toInt()
                )
            } catch (ex: Exception) {
            }
            return null
        }

        fun facebookHashKey(context: Context?) {
            try {
                val info = context?.packageManager?.getPackageInfo(
                    context.packageName,
                    PackageManager.GET_SIGNATURES
                ) as PackageInfo
                for (signature in info.signatures) {
                    val md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.e(
                        "KeyHash:", Base64.encodeToString(
                            md.digest(),
                            Base64.DEFAULT
                        )
                    );
                }
            } catch (ex: Exception) {
                ex.printStackTrace();

            }
        }

        fun convertDateFromServer(dateServer: String?): String? {
            try {
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
                val date = simpleDateFormat.parse(dateServer)
                val toFormatDate = SimpleDateFormat("dd / MM / yyy", Locale.ENGLISH)
                return toFormatDate.format(date)
            } catch (e: Exception) {
            }

            return dateServer
        }

        fun shareUrl(activity: Activity, text: String) {
            ShareCompat.IntentBuilder.from(activity)
                .setType("text/plain")
                .setChooserTitle("Share URL")
                .setText(text)
                .startChooser()
        }

    }

}