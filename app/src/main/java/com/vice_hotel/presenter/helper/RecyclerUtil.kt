
package com.vice_hotel.presenter.helper

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager



class RecyclerUtil {

    companion object {

        fun setUp(context: Context?, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
            if (context!=null && adapter!=null)
                setUp(context, recyclerView, adapter, false)
        }

        fun setUpHorizontal(context: Context?, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
            if (context!=null && adapter!=null)
                setUp(context, recyclerView, adapter, false, 1, false)
        }

        fun setUpHorizontal(context: Context?, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, fixedSize: Boolean) {
            if (context!=null && adapter!=null)
                setUp(context, recyclerView, adapter, fixedSize, 1, false)
        }

        fun setUp(context: Context, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>, fixedSize: Boolean) {
            setUp(context, recyclerView, adapter, fixedSize, 1)
        }

        fun setUp(context: Context?, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, column: Int) {
            if (adapter!=null && context!=null) setUp(context, recyclerView, adapter, false, column)
        }

        fun setUp(context: Context, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>, fixedSize: Boolean, column: Int) {
            setUp(context, recyclerView, adapter, fixedSize, column, true)
        }

        fun setUp(context: Context, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>, fixedSize: Boolean, isVertical: Boolean) {
            setUp(context, recyclerView, adapter, false, 1, isVertical)
        }

        fun setUp(context: Context, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>, fixedSize: Boolean, column: Int, isVertical: Boolean) {
            recyclerView.setHasFixedSize(fixedSize)
            val layoutManager: RecyclerView.LayoutManager
            if (column == 1) {
                layoutManager = LinearLayoutManager(context, if (isVertical) RecyclerView.VERTICAL else RecyclerView.HORIZONTAL, false)
            } else {
                layoutManager = GridLayoutManager(context, column)
            }
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
        }

        fun setUpStaggered(context: Context?, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, column: Int) {
            if (adapter!=null && context!=null) {
                val layoutManager = StaggeredGridLayoutManager(column, StaggeredGridLayoutManager.VERTICAL)
                recyclerView.layoutManager = layoutManager
                recyclerView.setHasFixedSize(true)
                recyclerView.adapter = adapter
            }
        }

        fun setUpAsInstagram(context: Context?, recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
            if (adapter!=null && context!=null) {
                val layoutManager= GridLayoutManager(context, 2)
                layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return when (position) {
                            0,4 -> 2
                            else -> 1
                        }
                    }
                }
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = adapter
            }
        }


    }

}