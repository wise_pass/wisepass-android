package com.vice_hotel.presenter.helper

private const val TYPE_VENUE = "venue"
private const val TYPE_PROMO = "promo"
enum class BarcodeType(type: String) {

    UNKNOWN(""),
    VENUE(TYPE_VENUE),
    PROMO(TYPE_PROMO);

    companion object {
        fun detect(type: String?): BarcodeType = when (type) {
            TYPE_VENUE -> VENUE
            TYPE_PROMO -> PROMO
            else -> UNKNOWN
        }
    }


}