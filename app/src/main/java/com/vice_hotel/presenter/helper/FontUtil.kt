package com.vice_hotel.presenter.helper

import android.content.Context
import android.graphics.Typeface

class FontUtil {

    companion object {
        fun textRegular(context: Context?) : Typeface {
            return Typeface.createFromAsset(context?.assets, "font/SF-Pro-Text-Regular.otf")
        }
        fun textMedium(context: Context?) : Typeface {
            return Typeface.createFromAsset(context?.assets, "font/SF-Pro-Text-Medium.otf")
        }
        fun textLight(context: Context?) : Typeface {
            return Typeface.createFromAsset(context?.assets, "font/SF-Pro-Text-Light.otf")
        }
        fun textBold(context: Context?) : Typeface {
            return Typeface.createFromAsset(context?.assets, "font/SF-Pro-Text-Bold.otf")
        }
        fun textSemibold(context: Context?) : Typeface {
            return Typeface.createFromAsset(context?.assets, "font/SF-Pro-Text-Semibold.otf")
        }
    }

}