package com.vice_hotel.presenter.helper

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.vice_hotel.R
import com.vice_hotel.databinding.BottomSheetVenueBinding
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.fragment.DashboardFragmentDirections
import kotlinx.android.synthetic.main.bottom_sheet_venue.*

class VenueBottomSheet: BottomSheetDialogFragment() {

    companion object {
        const val TAG = "VenueBottomSheet"
        fun newInstance(venueId: String?) = VenueBottomSheet().apply {
            arguments = Bundle().apply {
                putString("VENUE", venueId)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.bottom_sheet_venue, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState
        )
        val venueId = arguments?.getString("VENUE", null)
        btnView.setOnClickListener() {
            dismiss()
            val directions = DashboardFragmentDirections.actionDashboardFragmentToVenueDetailFragment(venueId)
            findNavController().navigate(directions)
        }
        loadVenueById(venueId, view)
    }

    private fun loadVenueById(id: String?, view: View) {
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenueDetailById(context, id).observeOnce(this, Observer {
            when (it.data?.venue) {
                null -> {
                    AppUtil.toast(context, it.message, false)
                }
                else -> {
                    val venue = it.data.venue
                    val binding: BottomSheetVenueBinding? = DataBindingUtil.bind(view)
                    binding?.venue = venue
                    if (venue.phoneNo.isNullOrEmpty()) {
                        btnCallVenue.visibility = View.GONE
                    }
                    if (venue.latitude==null || venue.longitude==null) {
                        btnLocation.visibility = View.GONE
                    }
                    btnCallVenue.setOnClickListener() {
                        val phoneNo = venue.phoneNo
                        var phone = phoneNo
                        if (phoneNo!=null) {
                            if (phoneNo.contains("/")) {
                                phone = phoneNo.split("/")[0].trim()
                            }
                        }
                        callVenue(phone)
                        dismiss()
                    }
                    btnLocation.setOnClickListener() {
                        openGoogleMap(venue)
                        dismiss()
                    }
                }


            }
        })
    }

    private fun callVenue(phoneNo: String?) {
        if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${phoneNo}"))
            activity?.startActivity(intent)
        } else {
            requestPermissions(arrayOf(android.Manifest.permission.CALL_PHONE), 3213)
        }
    }

    private fun openGoogleMap(venue: Venue?) {
        val gmmIntentUri = Uri.parse("google.navigation:q=${venue?.latitude},${venue?.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        activity?.startActivity(mapIntent)
    }

}