package com.vice_hotel.presenter.helper

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.reflect.KClass

const val SHARE_PREFERENCE_NAME = "wisepass_android"

class PreferenceUtils constructor(context: Context?){

    private var sharePreferences: SharedPreferences? = null

    companion object {
        const val ACCESS_KEY_API = "access_key_api"
        const val APP_LANGUAGE = "app_language"
        const val EMAIL_REGISTRATION = "remember_email_registration"
        const val DASHBOARD_ACTION = "dashboard_action"
        const val PUSH_NOTIFICATION = "push_notification"
        const val LAST_LOCATION = "last_location"
        const val NOT_FIRST_IN_APP = "not_first_in_app"
    }

    init {
        sharePreferences = context?.getSharedPreferences(SHARE_PREFERENCE_NAME, 0)
    }

    operator fun get(key: String, anonymousClass: Class<Any>?): Any? = when (anonymousClass) {
        String.javaClass -> sharePreferences?.getString(key, "")
        Boolean.javaClass -> sharePreferences?.getBoolean(key, false)
        Float.javaClass -> sharePreferences?.getFloat(key, 0f)
        Long.javaClass -> sharePreferences?.getLong(key, 0)
        Int.javaClass -> sharePreferences?.getInt(key, 0)
        else -> null
    }

    fun put(key: String, data: Any?) {
        if (data!=null) {
            val editor = sharePreferences?.edit()
            if (editor!=null) {
                when (data) {
                    is String -> editor.putString(key, data)
                    is Boolean -> editor.putBoolean(key, data)
                    is Float -> editor.putFloat(key, data)
                    is Long -> editor.putLong(key, data)
                    is Int -> editor.putInt(key, data)
                }
                editor.apply()
            }
        }
    }

    fun clear() {
        sharePreferences?.edit()?.remove(ACCESS_KEY_API)?.apply()
        sharePreferences?.edit()?.remove(DASHBOARD_ACTION)?.apply()
    }

    fun remove(key: String) {
        sharePreferences?.edit()?.remove(key)?.apply()
    }

    fun putListString(key: String, arrayString: Array<String>) {
        val s = AppUtil.arrayToString(arrayString)
        put(key, s)
    }

    fun getListString(key: String): List<String> {
        val string = get(key,String.javaClass) as String
        if (string.isEmpty()) {
           return ArrayList<String>()
        }
        return AppUtil.stringToArray(string)

    }

}