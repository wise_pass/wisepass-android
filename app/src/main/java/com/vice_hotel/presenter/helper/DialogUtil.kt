package com.vice_hotel.presenter.helper

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.vice_hotel.R
import kotlinx.android.synthetic.main.dialog_default.view.dialog_cancel
import kotlinx.android.synthetic.main.dialog_default.view.dialog_content
import kotlinx.android.synthetic.main.dialog_default.view.dialog_ok
import kotlinx.android.synthetic.main.dialog_default.view.dialog_title
import kotlinx.android.synthetic.main.dialog_default_cvv.view.*

class DialogUtil {

    companion object {
        private var dialog: Dialog? = null
        fun dismiss() {
            if (dialog!=null)
                if (dialog!!.isShowing)
                    dialog?.dismiss()
            dialog = null
        }
        fun show() {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
            dialog?.setCanceledOnTouchOutside(false);
            dialog?.show()
        }
    }

    class OpenLoadingDialog(activity: Activity?) {

        init {
            if (activity!=null) {
                dismiss()
                dialog = Dialog(activity, R.style.AppDialogTheme)
                val view = View.inflate(activity, R.layout.dialog_loading, null)
                dialog?.addContentView(view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                show()
            }
        }

    }

    class OpenCustomDialog(activity: Activity?, title: String?, content: String?, resOk: Int?, resCancel: Int?, callback: (Boolean) -> Unit) {

        init {
            if (activity!=null) {
                dismiss()
                dialog = Dialog(activity, R.style.AppDialogTheme)
                val view = View.inflate(activity, R.layout.dialog_default, null)
                if (title.isNullOrEmpty()) {
                    view.dialog_title.visibility = View.GONE
                } else {
                    view.dialog_title.text = title
                }
                view.dialog_content.text = content
                if (resOk!=null) {
                    view.dialog_ok.setText(resOk)
                    view.dialog_ok.visibility = View.VISIBLE
                }
                if (resCancel!=null) {
                    view.dialog_cancel.setText(resCancel)
                    view.dialog_cancel.visibility = View.VISIBLE
                }
                view.dialog_ok.setOnClickListener () { callback(true) }
                view.dialog_cancel.setOnClickListener () { callback(false) }
                dialog?.addContentView(view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                show()
            }
        }

    }

    class OpenCustomDialogCvv(activity: Activity?, callback: (String) -> Unit) {

        init {
            if (activity!=null) {
                dismiss()
                dialog = Dialog(activity, R.style.AppDialogTheme)
                val view = View.inflate(activity, R.layout.dialog_default_cvv, null)
                view.btnSubmit.setOnClickListener () {
                    val cvv = view.edCvv.text.toString().trim()
                    if (cvv.isNotEmpty()) {
                        callback(cvv)
                    } else {
                        AppUtil.toast(activity, "CVV is empty", false)
                    }
                }
                view.btnCancel.setOnClickListener () { callback("") }
                dialog?.addContentView(view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                show()
            }
        }

    }

    class OpenCustomDialogMultiOptions(activity: Activity?, title: String?, content: String?, resOk: Int?, resCancel: Int?, callback: (Int) -> Unit) {

        init {
            if (activity!=null) {
                dismiss()
                dialog = Dialog(activity, R.style.AppDialogTheme)
                val view = View.inflate(activity, R.layout.dialog_options, null)
                if (title.isNullOrEmpty()) {
                    view.dialog_title.visibility = View.GONE
                } else {
                    view.dialog_title.text = title
                }
                view.dialog_content.text = content
                if (resOk!=null) {
                    view.dialog_ok.setText(resOk)
                    view.dialog_ok.visibility = View.VISIBLE
                }
                if (resCancel!=null) {
                    view.dialog_cancel.setText(resCancel)
                    view.dialog_cancel.visibility = View.VISIBLE
                }
                view.dialog_ok.setOnClickListener () { callback(1) }
                view.dialog_cancel.setOnClickListener () { callback(2) }
                dialog?.addContentView(view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                show()
            }
        }

    }

    class OpenCustomDialogError(activity: Activity?, title: String?, content: String?, resOk: Int?, resCancel: Int?, callback: (Boolean) -> Unit) {

        init {
            if (activity!=null) {
                dismiss()
                dialog = Dialog(activity, R.style.AppDialogTheme)
                val view = View.inflate(activity, R.layout.dialog_default, null)
                view.dialog_title.setTextColor(Color.RED)
                if (title.isNullOrEmpty()) {
                    view.dialog_title.visibility = View.GONE
                } else {
                    view.dialog_title.text = title
                }
                view.dialog_content.text = content
                if (resOk!=null) {
                    view.dialog_ok.setText(resOk)
                    view.dialog_ok.visibility = View.VISIBLE
                }
                if (resCancel!=null) {
                    view.dialog_cancel.setText(resCancel)
                    view.dialog_cancel.visibility = View.VISIBLE
                }
                view.dialog_ok.setOnClickListener () { callback(true) }
                view.dialog_cancel.setOnClickListener () { callback(false) }
                dialog?.addContentView(view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                show()
            }
        }

    }

}