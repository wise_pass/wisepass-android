package com.vice_hotel.presenter.helper

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.vice_hotel.R
import com.vice_hotel.databinding.BottomSheetVenueBinding
import com.vice_hotel.model.data.CardInfor
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.fragment.DashboardFragmentDirections
import kotlinx.android.synthetic.main.bottom_sheet_cvv.*
import kotlinx.android.synthetic.main.bottom_sheet_venue.*

class CvvBottomSheet: BottomSheetDialogFragment() {

    companion object {
        const val TAG = "VenueBottomSheet"
        fun newInstance(cardInfor: CardInfor) = CvvBottomSheet().apply {
            arguments = Bundle().apply {
                putParcelable("cardInfor", cardInfor)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.bottom_sheet_cvv, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cardInfor = arguments?.getParcelable("cardInfor") as CardInfor
        btnSubmit.setOnClickListener() {
            val cvv = edCvv.text.toString().trim()
            cardInfor.cvv = cvv
            setDefault(cardInfor)
        }
    }

    private fun setDefault(cardInfor: CardInfor) {
        val request = ApiService.create(context)?.setDefaultCard(cardInfor)
        RetrofitUtil.requestAPI(context, request) {
            if (it.isSuccess()) {
                dismiss()
            } else {
                edCvv.text.clear()
                AppUtil.toast(context, "CVV is not valid", false)
            }
        }
    }

}