package com.vice_hotel.presenter.plan_payment

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import com.braintreepayments.api.models.PaymentMethodNonce
import com.braintreepayments.cardform.view.CardForm
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity

class BrainTreeUtil {

    companion object {
        const val REQUEST_CODE = 2341

        fun getDropInResult(requestCode: Int, resultCode: Int, data: Intent?, callBack: (Boolean) -> Unit) {
            if (requestCode == REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    callBack(true)
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // the user canceled
                    callBack(false)
                } else {
                    val msg = (data?.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception).message
                    Log.d("httpConnect", msg)
                    callBack(false)
                }
            }
        }

        fun requestPayment(activity: MainActivity, fragment: BaseFragment, callBack: (Boolean) -> Unit) {
            DialogUtil.OpenLoadingDialog(activity)
            ViewModelProviders.of(fragment).get(CheckoutModel::class.java).requestBrainTreeToken(fragment.context).observe(fragment, Observer {
                DialogUtil.dismiss()
                if (it.isSuccess()) {
                    val token = it.data?.token
                    val dropInRequest = DropInRequest()
                        .clientToken(token)
                        .cardholderNameStatus(CardForm.FIELD_REQUIRED)
                        .vaultManager(true)
                        .maskSecurityCode(true)
                    activity.startActivityForResult(dropInRequest.getIntent(activity), REQUEST_CODE)
                    activity.getPaymentResult {
                        callBack(it)
                    }
                } else {
                    callBack(false)
                }
            })
        }
    }

}