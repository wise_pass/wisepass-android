package com.vice_hotel.presenter.plan_payment

import android.content.Context
import com.vice_hotel.R

class PlanType {

    companion object {
        const val PAY_AS_YOU_GO = ""
        const val STANDARD = "Standard"
        const val PREMIUM = "Premium"
        const val DISCOVERY = "Discovery"
    }

    constructor()
    constructor(context: Context?, plan: String?) {
        if (!plan.isNullOrEmpty()) {
            this.plan = plan
        }
        this.context = context
    }

    private var plan: String = ""
    private var context: Context? = null

    fun isDiscoveryPlan(): Boolean = (plan.contentEquals(DISCOVERY))
    fun isStandardPlan(): Boolean = (plan.contentEquals(STANDARD))
    fun isPremiumPlan(): Boolean = (plan.contentEquals(PREMIUM))
    fun isPayAsYouGoPlan(): Boolean = when (plan) {
        DISCOVERY -> false
        STANDARD -> false
        PREMIUM -> false
        else -> true
    }

}