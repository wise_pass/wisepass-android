package com.vice_hotel.presenter.plan_payment

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Plan
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class PlanModel: ViewModel() {

    fun getPlans(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getPlans()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getPlanBilling(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getPlanBilling()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun updatePlan(context: Context?, plan: Plan): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.updatePlan(plan)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun upgradePlan(context: Context?, plan: Plan): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.upgradePlan(plan)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}