package com.vice_hotel.presenter.plan_payment

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class CheckoutModel: ViewModel() {

    fun getCheckOutRequest(context: Context?, planId: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val planCheckOut = PlanCheckOut(id = planId)
        val request = ApiService.create(context)?.getCheckOutRequest(planCheckOut)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getCheckOutRequest(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getCheckOutRewardRequest()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun requestBrainTreeToken(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.requestBrainTreeToken()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun subscriptionCheckout(context: Context?, payment: Payment): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.subscriptionCheckout(payment)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun checkPackageByBraintree(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.checkPackageByBraintree()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun checkOut(context: Context?, checkOut: CheckOut): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.checkOut(checkOut)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun checkOut(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.checkOut()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun removeCard(context: Context?, cardInfor: CardInfor): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.removeCard(cardInfor)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getPaymentMethodsHistories(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getPaymentMethodsHistories()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun cancelSubscription(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.cancelSubscription()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}