package com.vice_hotel.presenter.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.UserAccount
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class AccountModel: ViewModel() {

    fun getProfile(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getProfile()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun updateProfile(context: Context?, userAccount: UserAccount): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.updateProfile(userAccount)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun checkAvaiableCoins(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.checkAvailableCoins()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getUserReferral(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getUserReferral()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun sendMailRemind(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.sendEmailRemind()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}