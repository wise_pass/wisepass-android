package com.vice_hotel.presenter.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.RequestLogin
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class SignUpModel: ViewModel() {

    fun loginWithFacebook(context: Context?, requestLogin: RequestLogin): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val requestApi = ApiService.create(context)?.loginWithFacebook(requestLogin)
        RetrofitUtil.requestAPI(context, requestApi) { response ->
            data.postValue(response)
        }
        return data
    }

    fun reconnectWithFacebook(context: Context?, requestLogin: RequestLogin): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val requestApi = ApiService.create(context)?.reconnectWithFacebook(requestLogin)
        RetrofitUtil.requestAPI(context, requestApi) { response ->
            data.postValue(response)
        }
        return data
    }

    fun disconnectFacebook(context: Context?, requestLogin: RequestLogin): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val requestApi = ApiService.create(context)?.disconnectFacebook(requestLogin)
        RetrofitUtil.requestAPI(context, requestApi) { response ->
            data.postValue(response)
        }
        return data
    }

    fun validateEmail(context: Context?, email: RequestLogin): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val requestApi = ApiService.create(context)?.signUpEmail(email)
        RetrofitUtil.requestAPI(context, requestApi) { response ->
            data.postValue(response)
        }
        return data
    }

    fun signUpOtp(context: Context?, email: RequestLogin): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val requestApi = ApiService.create(context)?.signUpOtp(email)
        RetrofitUtil.requestAPI(context, requestApi) { response ->
            data.postValue(response)
        }
        return data
    }

}