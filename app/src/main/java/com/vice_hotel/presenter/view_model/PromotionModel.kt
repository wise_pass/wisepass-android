package com.vice_hotel.presenter.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Barcode
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class PromotionModel: ViewModel() {

    fun letPass(context: Context?, code: String): LiveData<ResponseBody> {
        val barcode = Barcode(promoCode = code)
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.letPass(barcode)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}