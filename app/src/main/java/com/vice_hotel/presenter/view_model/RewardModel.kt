package com.vice_hotel.presenter.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Challenge
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class RewardModel: ViewModel() {

    fun getRewards(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getRewards()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getChallenges(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getChallenges()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getChallengeDetail(context: Context?, idString: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val challenge = Challenge(id = idString)
        val request = ApiService.create(context)?.getChallengeDetail(challenge)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun claimChallenge(context: Context?, idString: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val challenge = Challenge(id = idString)
        val request = ApiService.create(context)?.claimChallenge(challenge)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}