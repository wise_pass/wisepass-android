package com.vice_hotel.presenter.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.SearchData
import com.vice_hotel.model.data.Venue
import com.vice_hotel.model.data.WifiCountry
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class VenueModel: ViewModel() {

    fun getVenuesByKeyType(context: Context?, keyType: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val venue = Venue(keyType = keyType)
        val request = ApiService.create(context)?.getVenuesByKeyType(venue)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getHomeVenues(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getHomeVenues()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun searchVenues(context: Context?, searchData: SearchData): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.searchVenues(searchData)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getVenuesByCity(context: Context?, cityCode: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val wifiCountry = WifiCountry(cityCode = cityCode)
        val request = ApiService.create(context)?.getVenuesByCity(wifiCountry)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getCities(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getCities()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getVenueDetailById(context: Context?, id: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val venue =  Venue(id = id)
        val request = ApiService.create(context)?.getVenueDetailById(venue)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

    fun getVenueReadMoreById(context: Context?, id: String?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val venue =  Venue(venueId = id)
        val request = ApiService.create(context)?.getVenueReadMoreById(venue)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}