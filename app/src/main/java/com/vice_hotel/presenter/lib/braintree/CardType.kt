package com.vice_hotel.presenter.lib.braintree

import java.util.regex.Pattern
import java.util.Collections.replaceAll

enum class CardType {
    UNKNOWN,
    VISA("^4[0-9]{12}(?:[0-9]{3}){0,2}$"),
    MASTERCARD("^(?:5[1-5]|2(?!2([01]|20)|7(2[1-9]|3))[2-7])\\d{14}$");

    constructor()
    constructor(pattern: String) {
        this.pattern = Pattern.compile(pattern)
    }

    private var pattern: Pattern? = null

    companion object {
        fun detect(cNumber: String): CardType {
            val cardNumber = cNumber.replace("-", "")
            for (cardType in CardType.values()) {
                if (null == cardType.pattern) continue
                if (cardType.pattern!!.matcher(cardNumber).matches()) return cardType
            }
            return UNKNOWN
        }
    }

}