package com.vice_hotel.presenter.lib.retrofit

import android.content.Context
import com.braintreepayments.api.models.CardNonce
import com.vice_hotel.BuildConfig
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*
import retrofit2.converter.gson.GsonConverterFactory


interface ApiService {

    companion object {
        fun create(context: Context?): ApiService? {
            if (context!=null) {
                return Retrofit.Builder()
                    .baseUrl(BuildConfig.API_URL)
                    .client(RetrofitUtil.clientBuilder(context))
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(ApiService::class.java)
            }
            return null
        }
        fun createWithoutHost(context: Context?, url: String): ApiService? {
            if (context!=null) {
                return Retrofit.Builder()
                    .baseUrl(url)
                    .client(RetrofitUtil.clientBuilder(context))
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(ApiService::class.java)
            }
            return null
        }
    }

    @GET(".")
    fun getCountryByIpInternet(): Call<WifiCountry>

    @GET(".")
    fun getDataWithoutHost(): Call<String>

    @Headers("Content-Type: application/json")
    @POST("auth/v2/getconfig")
    fun setConfigAndCheckVersion(@Body appConfig: AppConfig): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("auth/signup")
    fun signUpEmail(@Body email: RequestLogin): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("auth/v2/validateotp")
    fun loginValidateOtp(@Body email: RequestLogin): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("auth/fb/login")
    fun loginWithFacebook(@Body email: RequestLogin): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("auth/fb/reconnect")
    fun reconnectWithFacebook(@Body email: RequestLogin): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("authnode/fb/logout")
    fun disconnectFacebook(@Body email: RequestLogin): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("auth/v2/validateotp")
    fun signUpOtp(@Body email: RequestLogin): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/availablecoins")
    fun checkAvailableCoins(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/validatebarcode")
    fun scanQrCode(@Body barcode: Barcode): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/plansfrompromocode")
    fun getPromotionCode(@Body barcode: Barcode): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/letspass")
    fun letPass(@Body barcode: Barcode): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/venueitems")
    fun getVenuesByQrCode(@Body scanType: ScanType): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/rewarditems")
    fun getRewardByQrCode(@Body scanType: ScanType): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/confirm")
    fun confirmVenueAfterScan(@Body confirmVenue: ScanType): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/rewardconfirm")
    fun confirmVenueAfterScanReward(@Body confirmVenue: ScanType): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("v4/schedule/remind")
    fun sendEmailRemind(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/history")
    fun getHistory(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/plans")
    fun getPlans(): Call<ResponseBody>

//    @Headers("Content-Type: application/json")
//    @GET("user/planandbilling")
//    fun getPlanBilling(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("v4/auth/plan")
    fun getPlanBilling(): Call<ResponseBody>



    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/listcards")
    fun getPaymentMethodsHistories(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/setdefaultcard")
    fun setDefaultCard(@Body card: CardInfor): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("venue/v2/seeallvenues")
    fun searchVenues(@Body searchData: SearchData): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("venue/v3/city")
    fun getVenuesByCity(@Body wifiCountry: WifiCountry): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET("venue/v3/city")
    fun getCities(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("venue/v2/details")
    fun getVenueDetailById(@Body venue: Venue): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("venue/v2/vendor/readmore")
    fun getVenueReadMoreById(@Body venue: Venue): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/checkoutscreen")
    fun getCheckOutRequest(@Body planCheckOut: PlanCheckOut): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET("plannode/v2/paygetonereward/checkout")
    fun getCheckOutRewardRequest(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/clienttokenforbraintree")
    fun requestBrainTreeToken(): Call<ResponseBody>

//    @Headers("Content-Type: application/json")
//    @POST("v4/payment/subscription/checkout")
//    fun requestBrainTreeToken(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("v4/payment/subscription/checkout")
    fun subscriptionCheckout(@Body payment: Payment): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("v4/payment/subscription/cancel")
    fun cancelSubscription(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("v4/auth/pass/reset")
    fun checkPackageByBraintree(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/addcardwithnoncefromclient")
    fun paymentVerify(@Body nonce: BTNonce): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("venue/v2/homescreen")
    fun getHomeVenues(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("venue/v2/seeall")
    fun getVenuesByKeyType(@Body venue: Venue): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET("user/profile")
    fun getProfile(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("user/updateProfile")
    fun updateProfile(@Body userAccount: UserAccount): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("scan/v2/getrewards")
    fun getRewards(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/pay")
    fun checkOut(@Body checkOut: CheckOut): Call<ResponseBody>


    @Headers("Content-Type: application/json")
    @POST("plannode/v2/paygetonereward")
    fun checkOut(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("planpayment/v2/deletecard")
    fun removeCard(@Body card: CardInfor): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("user/updateplanautorenew")
    fun updatePlan(@Body plan: Plan): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("plannode/v2/changenextplan")
    fun upgradePlan(@Body plan: Plan): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET("challenge/v2/byuser")
    fun getChallenges(): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("challenge/v2/details")
    fun getChallengeDetail(@Body challenge: Challenge): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("challenge/v2/claim")
    fun claimChallenge(@Body challenge: Challenge): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET("user/referralcode")
    fun getUserReferral(): Call<ResponseBody>

}