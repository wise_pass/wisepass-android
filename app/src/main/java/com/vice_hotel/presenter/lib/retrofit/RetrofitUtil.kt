package com.vice_hotel.presenter.lib.retrofit

import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.vice_hotel.BuildConfig
import com.vice_hotel.R
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.WifiCountry
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.PreferenceUtils
import com.vice_hotel.view.activity.INTENT_FILTER_TOKEN_EXPIRED
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.KeyStore
import java.security.SecureRandom
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

private const val READ_TIME_OUT = 10L
private const val CONNECTION_TIME_OUT = 10L
private const val KEY_LOG = "httpConnection"
const val HTTP_OK = 200
class RetrofitUtil private constructor() {

    companion object {

        private fun enableTls12OnPreLollipop(client: OkHttpClient.Builder): OkHttpClient.Builder {
            if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                try {
                    val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
                    trustManagerFactory.init(null as KeyStore?)
                    val trustManagers = trustManagerFactory.trustManagers
                    if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
                        throw IllegalStateException("Unexpected default trust managers")
                    }

                    val trustManager = trustManagers[0] as X509TrustManager

                    val sslContext = SSLContext.getInstance(TlsSocketFactory.TLS_V12)
                    sslContext.init(null, trustManagers, SecureRandom())
                    client.sslSocketFactory(TlsSocketFactory(sslContext.socketFactory), trustManager)
                } catch (exc: Exception) {
                    Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc)
                }

            }

            return client
        }

        fun clientBuilder(context: Context): OkHttpClient {

            val httpClient = enableTls12OnPreLollipop(
                OkHttpClient.Builder()
                    .addNetworkInterceptor(
                        HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                            if (BuildConfig.DEBUG) {
                                Log.d(KEY_LOG, it)
                            }
                        }).setLevel(HttpLoggingInterceptor.Level.BODY)
                    )
            )
            httpClient.readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
            httpClient.connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
            httpClient.addInterceptor { chain ->

                val original = chain.request();

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()

                requestBuilder.addHeader("Accept", "application/json")

                val token = PreferenceUtils(context).get(PreferenceUtils.ACCESS_KEY_API, String.javaClass) as String
                val languageCode = PreferenceUtils(context).get(PreferenceUtils.APP_LANGUAGE, String.javaClass) as String
                requestBuilder.addHeader("app_private_key", "TranKyHoaThanh12345")
                if (token.isNotEmpty()) {
                    requestBuilder.addHeader("Authorization", token)
                }
                if (languageCode.isNotEmpty()) {
                    requestBuilder.addHeader("language_code", languageCode)
                } else {
                    requestBuilder.addHeader("language_code", "en")
                }

                val request = requestBuilder.build();

                chain.proceed(request);
            }

            return httpClient.build();

        }

        fun requestAPI(context: Context?, call: Call<ResponseBody>?, callback: (response: ResponseBody) -> Unit) {
            call?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>?, t: Throwable) {
                    //val encodedPath = call?.request()?.url()?.encodedPath()
                    val responseBody = ResponseBody()
                    responseBody.statusCode = 500
                    responseBody.message = context?.resources?.getString(R.string.network_offline)
                    callback(responseBody)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>?) {
                    if (response?.body()!=null) {
                        val data = response.body()
                        data?.statusCodeExactly = data?.statusCode
                        if (data?.statusCode == 701) {
                            DialogUtil.dismiss()
                            if (context!=null)
                                LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(INTENT_FILTER_TOKEN_EXPIRED))
                            return
                        } else if (data?.statusCode!! >= 200 && data.statusCode!! <=299) {
                            data.statusCode = HTTP_OK
                        }
                        callback(data)
                    } else {
                        val responseBody = ResponseBody()
                        responseBody.statusCode = 500
                        responseBody.message = context?.resources?.getString(R.string.network_offline)
                        callback(responseBody)
                    }
                }
            })
        }

        fun requestAPIForWifiCountry(context: Context?, call: Call<WifiCountry>?, callback: (response: WifiCountry) -> Unit) {

            call?.enqueue(object : Callback<WifiCountry> {
                override fun onFailure(call: Call<WifiCountry>?, t: Throwable) {
                    AppUtil.toast(context, context?.resources?.getString(R.string.network_offline), false)
                    callback(WifiCountry())
                }

                override fun onResponse(call: Call<WifiCountry>, response: Response<WifiCountry>?) {
                    if (response?.body()!=null) {
                        val data = response.body()!!
                        data.httpCode = HTTP_OK
                        callback(data)
                    } else {
                        callback(WifiCountry())
                    }
                }
            })
        }

        fun requestDataString(context: Context?, call: Call<String>?, callback: (response: String) -> Unit) {

            call?.enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>?, t: Throwable) {
                    AppUtil.toast(context, context?.resources?.getString(R.string.network_offline), false)
                    callback("")
                }

                override fun onResponse(call: Call<String>, response: Response<String>?) {
                    if (response?.body()!=null) {
                        val data = response.body()!!
                        callback(data)
                    } else {
                        callback("")
                    }
                }
            })
        }
    }

}