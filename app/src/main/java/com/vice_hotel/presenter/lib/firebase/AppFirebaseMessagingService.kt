package com.vice_hotel.presenter.lib.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.vice_hotel.R
import com.vice_hotel.model.data.AppConfig
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.PreferenceUtils
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.view.activity.MainActivity
import java.util.*

class AppFirebaseMessagingService: FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        remoteMessage?.data?.isNotEmpty()?.let {
            val pushNotification = PreferenceUtils(this).get(PreferenceUtils.PUSH_NOTIFICATION, Boolean.javaClass) as Boolean
            if (it && pushNotification) {
                sendNotification(remoteMessage.data, randomNumber())
            }
        }
    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        val appConfig = AppManager.instance.getAppConfig()
        if (appConfig!=null) {
            appConfig.push_token = p0
            val requestCheckVersion = ApiService.create(this)?.setConfigAndCheckVersion(appConfig)
            RetrofitUtil.requestAPI(this, requestCheckVersion) {}
        }
    }
    private fun sendNotification(stringMap: Map<String, String>, id: Int) {

        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse("wisepass://${getString(R.string.host_rewards)}")
        }
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, "fcm_default_channel")
            .setSmallIcon(R.drawable.intercom_push_icon)
            .setLargeIcon(BitmapFactory.decodeResource(applicationContext.resources, R.drawable.intercom_push_icon))
            .setContentTitle(stringMap["title"])
            .setContentText(stringMap["body"])
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val inboxStyle = NotificationCompat.BigTextStyle()
        inboxStyle.setBigContentTitle(stringMap["title"])
        inboxStyle.bigText(stringMap["body"])
        notificationBuilder.setStyle(inboxStyle)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("fcm_default_channel",
                "wise_pass",
                NotificationManager.IMPORTANCE_DEFAULT)
            channel.setShowBadge(false)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(id /* ID of notification */, notificationBuilder.build())
    }
    private fun randomNumber(): Int {
        val r = Random()
        return r.nextInt(80 - 30) + 30
    }
}