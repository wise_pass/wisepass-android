package com.vice_hotel.presenter.lib.glide

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.AppUtil


class GlideUtil {

    companion object {
        fun load(context: Context?, pUrl: String?, view: ImageView?) {
            if (context!=null && view!=null  && pUrl!=null)
                GlideApp.with(context).load(pUrl).into(view)
        }

        fun load(context: Context?, pUri: Uri?, view: ImageView?) {
            if (context!=null && view!=null && pUri!=null)
                GlideApp.with(context).load(pUri).into(view)
        }

        fun loadCircle(context: Context?, pUri: Uri?, view: ImageView?) {
            if (context!=null && view!=null && pUri!=null)
                GlideApp.with(context).load(pUri).apply(RequestOptions().circleCrop()).into(view)
        }

        fun loadCircle(context: Context?, pUrl: String?, view: ImageView?) {
            if (context!=null && view!=null && pUrl!=null)
                GlideApp.with(context).load(pUrl).apply(RequestOptions().circleCrop()).into(view)
        }

        @SuppressLint("CheckResult")
        fun loadCircleBitmap(context: Context?, pUrl: String?, callBack: (Bitmap)-> Unit) {
            if (context!=null && !pUrl.isNullOrEmpty()) {
                val width = AppUtil.dpToPx(context, 36)!!.toInt()
                GlideApp.with(context).asBitmap().apply(RequestOptions().circleCrop()).load(pUrl).addListener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean
                    ): Boolean {
                        val icon = BitmapFactory.decodeResource(
                            context.resources,
                            R.drawable.ic_dashboard_wisepass
                        )
                        callBack.invoke(icon)
                        return false
                    }

                    override fun onResourceReady(
                        resource: Bitmap?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        if (resource!=null) {
                            callBack.invoke(resource)
                        } else {
                            val icon = BitmapFactory.decodeResource(
                                context.resources,
                                R.drawable.ic_dashboard_wisepass
                            )
                            callBack.invoke(icon)
                        }

                        return false
                    }

                }).submit(width,width)
            }
        }
    }

}