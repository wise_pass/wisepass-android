package com.vice_hotel.presenter.scan

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Barcode
import com.vice_hotel.model.data.ValidateBarcode
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class ScanQrCodeModel: ViewModel() {

    fun scanQrCode(context: Context?, barcodeString: String): LiveData<ResponseBody> {
        val barcode = Barcode(barcode = barcodeString)
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.scanQrCode(barcode)
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}