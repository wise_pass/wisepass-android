package com.vice_hotel.presenter.scan

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.ScanType
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import retrofit2.Call

class QrCodeDetectedModel: ViewModel() {

    fun getVenuesByBarcode(context: Context?, scanType: ScanType?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        if (scanType!=null) {
            var request: Call<ResponseBody>? = null
            when (scanType.type) {
                ScanType.REWARD -> {
                    request = ApiService.create(context)?.getRewardByQrCode(scanType)
                }
                else -> {
                    request = ApiService.create(context)?.getVenuesByQrCode(scanType)
                }
            }
            if (request!=null) {
                RetrofitUtil.requestAPI(context, request) {
                    data.postValue(it)
                }
            }
        }

        return data
    }

    fun confirmVenueAfterScan(context: Context?, scanType: ScanType?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        if (scanType!=null) {
            var request: Call<ResponseBody>? = null
            when (scanType.type) {
                ScanType.REWARD -> {
                    request = ApiService.create(context)?.confirmVenueAfterScanReward(scanType)
                }
                else -> {
                    request = ApiService.create(context)?.confirmVenueAfterScan(scanType)
                }
            }
            if (request!=null) {
                RetrofitUtil.requestAPI(context, request) {
                    data.postValue(it)
                }
            }
        }
        return data
    }

}