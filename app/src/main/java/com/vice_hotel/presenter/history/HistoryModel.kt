package com.vice_hotel.presenter.history

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.ResponseData
import com.vice_hotel.model.data.HistoryItem
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil

class HistoryModel: ViewModel() {

    fun getHistory(context: Context?): LiveData<ResponseBody> {
        val data = MutableLiveData<ResponseBody>()
        val request = ApiService.create(context)?.getHistory()
        RetrofitUtil.requestAPI(context, request) {
            data.postValue(it)
        }
        return data
    }

}