package com.vice_hotel.view.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.vice_hotel.R
import com.vice_hotel.view.BaseActivity
import kotlinx.android.synthetic.main.activity_web.*
import android.widget.MediaController


class WebActivity: BaseActivity() {

    companion object {
        fun startActivity(activity: BaseActivity, url: String?) {
            val intent = Intent(activity, WebActivity::class.java).apply {
                putExtra("url", url)
            }
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        actionBack.setOnClickListener() {
            finish()
        }
        webView.settings.javaScriptEnabled = true
        webView.settings.defaultTextEncodingName = "utf-8"
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.webViewClient = CustomWebViewClient()
        webView.webChromeClient = WebChromeClient()

        webView.loadUrl(intent.getStringExtra("url"))
//        videoView.setVideoURI(Uri.parse(intent.getStringExtra("url")))
//        val controller = MediaController(this)
//        videoView.setMediaController(controller)
//        videoView.start()
    }

    inner class CustomWebViewClient: WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
        }
    }

}