package com.vice_hotel.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.R
import kotlinx.android.synthetic.main.activity_profile_picture.*
import android.os.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.camera.GalleryModel
import com.vice_hotel.view.adapter.GalleryPickerAdapter
import com.vice_hotel.view.fragment.camera.Camera2BasicFragment

class ProfilePictureActivity: BaseActivity() {

    var galleryPickerAdapter: GalleryPickerAdapter? = null

    companion object {
        fun startActivity(activity: BaseActivity) {
            val intent = Intent(activity, ProfilePictureActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_picture)
        requestCameraAndGallery()
    }

    private fun requestCameraAndGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                onCompletePermission()
            } else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode== Constant.REQUEST_CODE_CAMERA) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    onCompletePermission()
                } else {
                    finish()
                }
            }
        }
    }

    private fun onCompletePermission() {
        supportFragmentManager.beginTransaction().replace(R.id.textureView, Camera2BasicFragment.newInstance()).commit()
        getImagesFromGallery()
    }

    fun onDidCapturePicture(urlLocal: String) {
        AppUtil.toast(this, urlLocal, false)
    }

    private fun getImagesFromGallery() {
        val rclHeight = resources.displayMetrics.heightPixels - resources.getDimensionPixelSize(R.dimen.action_bar) - resources.displayMetrics.widthPixels
        rclGallery.layoutParams.height = rclHeight
        rclGallery.requestLayout()
        galleryPickerAdapter = GalleryPickerAdapter()
        galleryPickerAdapter?.setOnPhotoSelected {
            AppUtil.toast(this, it.path, false)
        }
        RecyclerUtil.setUp(this, rclGallery, galleryPickerAdapter, 3)
        val viewModel = ViewModelProviders.of(this).get(GalleryModel::class.java)
        viewModel.getImages()?.observe(this, Observer {
            galleryPickerAdapter?.addPhotos(it)
        })
        viewModel.queryImagesFromGallery(this)
    }
}