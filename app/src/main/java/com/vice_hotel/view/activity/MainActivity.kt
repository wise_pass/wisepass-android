package com.vice_hotel.view.activity

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.braintreepayments.api.dropin.DropInResult
import com.braintreepayments.api.models.PaymentMethodNonce
import com.facebook.AccessToken
import com.google.android.gms.location.*
import com.google.firebase.iid.FirebaseInstanceId
import com.vice_hotel.BuildConfig
import com.vice_hotel.R
import com.vice_hotel.model.data.AppConfig
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.presenter.plan_payment.BrainTreeUtil
import com.vice_hotel.presenter.view_model.PromotionModel
import com.vice_hotel.view.BaseActivity

const val INTENT_FILTER_TOKEN_EXPIRED = "wisepass.token_expired"
class MainActivity : BaseActivity() {

    lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var mLocationRequest: LocationRequest
    private val mLocationCallback: LocationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            mFusedLocationClient.removeLocationUpdates(this)
            val location = locationResult?.lastLocation
            PreferenceUtils(this@MainActivity).put(PreferenceUtils.LAST_LOCATION, "${location?.latitude}@${location?.longitude}")
            locationListener?.invoke(location?.latitude, location?.longitude)
        }

        override fun onLocationAvailability(p0: LocationAvailability?) {
            super.onLocationAvailability(p0)
            if (p0!=null && p0.isLocationAvailable) {
            } else {
                locationListener?.invoke(null, null)
            }
        }
    }

    private var paymentCallBack: ((Boolean)->Unit)? = null
    private var locationListener: ((Double?, Double?)->Unit)? = null

    companion object {
        fun startActivity(activity: BaseActivity) {
            val intent = Intent(activity, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            activity.startActivity(intent)
        }
    }

    inner class TokenExpireBroadcastsReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            AppManager.logOut(this@MainActivity)
        }
    }

    private val tokenExpireBroadcastsReceiver = TokenExpireBroadcastsReceiver()
    var venueID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val filter = IntentFilter(INTENT_FILTER_TOKEN_EXPIRED)
        setLocationRequest()
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenExpireBroadcastsReceiver, filter)
        if (intent?.data!=null) {
            val action: Uri? = intent?.data
            when (action?.host) {
                getString(R.string.host_rewards) -> {
                    val code = action?.lastPathSegment
                    if (!code.isNullOrEmpty()) {
                        leftPassFromPromotionCode(code)
                    }
                }
                getString(R.string.host_plans) -> {
                    val code = action?.lastPathSegment
                    if (!code.isNullOrEmpty()) {
                        leftPassFromPromotionCode(code)
                    }
                }
                getString(R.string.host_venue) -> {
                    val code = action?.lastPathSegment
                    venueID = code
                }
                else -> {
                    val list = action?.pathSegments
                    if (list!=null) {
                        if (list.size > 1) {
                            val path = list[list.size-2]
                            when (path) {
                                getString(R.string.host_rewards) -> {
                                    val code = action.lastPathSegment
                                    if (!code.isNullOrEmpty()) {
                                        leftPassFromPromotionCode(code)
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    private fun setLocationRequest() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

    }

    fun requestLocation(locationListener: ((Double?, Double?)->Unit)) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED){
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null)
            this.locationListener = locationListener
        }
    }

    var goToPage = ""

    private fun leftPassFromPromotionCode(code: String) {
        ViewModelProviders.of(this).get(PromotionModel::class.java)
            .letPass(this, code).observe(this, androidx.lifecycle.Observer { letpass ->
                if (letpass.isSuccess()){
                    val type = letpass.data?.letPass?.type
                    if (type!=null) {
                        goToPage = type
                    }
                } else {
                    AppUtil.toast(applicationContext, letpass.message, false)
                }
            })
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tokenExpireBroadcastsReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        BrainTreeUtil.getDropInResult(requestCode, resultCode, data) {
            paymentCallBack?.invoke(it)
        }
    }

    fun getPaymentResult(paymentCallBack: ((Boolean)->Unit)?) {
        this.paymentCallBack = paymentCallBack
    }

}
