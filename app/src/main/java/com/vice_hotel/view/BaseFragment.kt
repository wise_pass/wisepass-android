package com.vice_hotel.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.vice_hotel.presenter.helper.DialogUtil

abstract class BaseFragment: Fragment() {

    var fragmentView: View? = null
    private var isCreatedFragment = false

    abstract fun getLayoutRes(): Int

    abstract fun onViewCreated(savedInstanceState: Bundle?)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(getLayoutRes(), container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DialogUtil.dismiss()
        if (!isCreatedFragment) {
            onFragmentCreateFirstTime()
            isCreatedFragment = true
        }
        onViewCreated(savedInstanceState)
    }

    open fun onFragmentCreateFirstTime() {

    }



}