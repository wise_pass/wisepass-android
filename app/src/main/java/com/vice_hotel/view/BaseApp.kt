package com.vice_hotel.view

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.multidex.MultiDexApplication
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.vice_hotel.BuildConfig
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.presenter.helper.AppUtil
import com.appsflyer.AppsFlyerLibCore.LOG_TAG
import com.appsflyer.AppsFlyerLibCore.LOG_TAG
import com.appsflyer.AppsFlyerTrackingRequestListener
import com.facebook.drawee.backends.pipeline.Fresco


class BaseApp: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        val appsFlyerKey = BuildConfig.APPSFLYER_API_KEY
        if (appsFlyerKey.isNotEmpty()) {
            AppsFlyerLib.getInstance().init(appsFlyerKey, conversionDataListener, applicationContext)
            AppsFlyerLib.getInstance().startTracking(this, appsFlyerKey, object : AppsFlyerTrackingRequestListener {
                override fun onTrackingRequestSuccess() {
                    Log.d(LOG_TAG, "onTrackingRequestSuccess")
                }
                override fun onTrackingRequestFailure(p0: String?) {
                    Log.d(LOG_TAG, "onTrackingRequestFailure")
                }
            })
            Fresco.initialize(this)
        }
    }
    private val conversionDataListener = object : AppsFlyerConversionListener {

        override fun onAppOpenAttribution(conversionData: MutableMap<String, String>?) {
            if (conversionData!=null) {
                for (attrName in conversionData.keys) {
                    Log.d(
                        LOG_TAG, "onAppOpen_attribute: " + attrName + " = " +
                                conversionData.get(attrName)
                    )
                }
            }
        }

        override fun onAttributionFailure(errorMessage: String?) {
            Log.d(LOG_TAG, "error onAttributionFailure : " + errorMessage);
        }

        override fun onInstallConversionDataLoaded(conversionData: MutableMap<String, String>?) {
            if (conversionData!=null) {
                for (attrName in conversionData.keys) {
                    Log.d(
                        LOG_TAG, "conversion_attribute: " + attrName + " = " +
                                conversionData.get(attrName)
                    )
                }
            }

        }

        override fun onInstallConversionFailure(errorMessage: String?) {
            Log.d(LOG_TAG, "error onAttributionFailure : " + errorMessage);
        }

    }
}