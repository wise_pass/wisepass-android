package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterFilterBrandsBinding
import com.vice_hotel.model.data.Brand

class FilterBrandsAdapter: RecyclerView.Adapter<FilterBrandsAdapter.ViewHolder>() {

    private var items = ArrayList<Brand?>()
    private var callBack: (()-> Unit)? = null

    fun onItemSelectListener(callBack: (()-> Unit)) {
        this.callBack = callBack
    }

    fun addAll(brans: ArrayList<Brand>?) {
        if (brans!=null) {
            items.clear()
            items.add(null)
            items.addAll(brans)
        }
    }

    fun getAllBrandsActive(): ArrayList<Brand> {
        val itemsActive = ArrayList<Brand>()
        if (items.size > 0) {
            for (brand in items) {
                when (brand?.checked) {
                    true -> itemsActive.add(brand)
                }
            }
        }
        return itemsActive
    }

    override fun getItemViewType(position: Int): Int =
        when (items[position]) {
            null -> R.layout.adapter_view_margin
            else -> R.layout.adapter_filter_brands
        }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener() {
                val isCheck = items[adapterPosition]?.checked
                if (isCheck!=null) {
                    items[adapterPosition]?.checked = !isCheck
                }
                notifyItemChanged(adapterPosition)
                callBack?.invoke()
            }
        }

        fun bind() {
            val binding: AdapterFilterBrandsBinding? = DataBindingUtil.bind(itemView)
            binding?.data = items[adapterPosition]
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterBrandsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FilterBrandsAdapter.ViewHolder, position: Int) {
        if (items[position]!=null) {
            holder.bind()
        }
    }
}