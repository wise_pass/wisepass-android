package com.vice_hotel.view.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.presenter.lib.glide.GlideApp
import com.vice_hotel.presenter.lib.glide.GlideUtil
import kotlinx.android.synthetic.main.adapter_gallery_picker.view.*

class GalleryPickerAdapter: RecyclerView.Adapter<GalleryPickerAdapter.PhotoHolder>() {

    private val items = ArrayList<Uri>()
    private var callBack: ((Uri) -> Unit)? = null

    fun addPhotos(uris: ArrayList<Uri>) {
        items.clear()
        items.addAll(uris)
        notifyDataSetChanged()
    }

    fun setOnPhotoSelected(callBack: ((Uri) -> Unit)?) {
        this.callBack = callBack
    }

    inner class PhotoHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(uri: Uri) {
            GlideUtil.load(itemView.context, uri, itemView.img)
        }
        init {
            itemView.setOnClickListener () { callBack?.invoke(items[adapterPosition]) }
        }
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_gallery_picker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return PhotoHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: GalleryPickerAdapter.PhotoHolder, position: Int) {
        holder.bind(items[position])
    }
}