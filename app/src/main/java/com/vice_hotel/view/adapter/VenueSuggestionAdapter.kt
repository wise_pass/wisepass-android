package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterVenueSuggestionBinding
import com.vice_hotel.model.data.Venue

class VenueSuggestionAdapter: RecyclerView.Adapter<VenueSuggestionAdapter.ViewHolder>() {

    private var venues = ArrayList<Venue?>()
    private var listener: ((Venue) -> Unit)? = null

    fun setOnSelectedVenueListener(listener: ((Venue) -> Unit)) {
        this.listener = listener
    }

    fun addAll(arr: ArrayList<Venue>?) {
        if (arr!=null) {
            venues.clear()
            val newArr = ArrayList<Venue?>()
            newArr.add(null)
            newArr.addAll(arr)
            venues.addAll(newArr)
            notifyDataSetChanged()
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (venues[position]) {
            null -> R.layout.adapter_view_margin
            else -> R.layout.adapter_venue_suggestion
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = venues.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(venues[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private var binding: AdapterVenueSuggestionBinding? = null

        init {
            itemView.setOnClickListener() {
                val venue = venues[adapterPosition]
                if (venue!=null) {
                    listener?.invoke(venue)
                }

            }
        }

        fun bind(venue: Venue?) {
            if (venue!=null) {
                binding = DataBindingUtil.bind(itemView)
                binding?.venue = venue
            }
        }
    }
}