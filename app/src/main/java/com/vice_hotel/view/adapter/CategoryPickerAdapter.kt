package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterCategoryPickerBinding
import com.vice_hotel.model.data.Category

class CategoryPickerAdapter: RecyclerView.Adapter<CategoryPickerAdapter.ViewHolder>() {

    val list = ArrayList<Category>()

    fun addAll(categories: ArrayList<Category>?) {
        if (categories!=null) {
            list.clear()
            list.addAll(categories)
            notifyDataSetChanged()
        }
    }

    fun getAllCategoriesActive(): ArrayList<Category> {
        val categories = ArrayList<Category>()
        for (category in list) {
            if (category.checked!! && category.section.isNullOrEmpty()) {
                categories.add(category)
            }
        }
        return categories
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_category_picker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view)  {

        var binding: AdapterCategoryPickerBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() {
                list[adapterPosition].checked = !list[adapterPosition].checked!!
                notifyDataSetChanged()
            }
        }

        fun bind(category: Category) {
            binding?.category = category
        }

    }
}