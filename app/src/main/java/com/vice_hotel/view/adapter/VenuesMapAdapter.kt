package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterVenuesMapBinding
import com.vice_hotel.model.data.Venue

class VenuesMapAdapter: RecyclerView.Adapter<VenuesMapAdapter.ViewHolder>() {

    private var venues = ArrayList<Venue>()
    private var listener: ((Venue) -> Unit)? = null

    fun setOnSelectedVenueListener(listener: ((Venue) -> Unit)) {
        this.listener = listener
    }

    fun addAll(arr: ArrayList<Venue>?) {
        if (arr!=null) {
            venues.clear()
            venues.addAll(arr)
            notifyDataSetChanged()
        }
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_venues_map

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = venues.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(venues[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private var binding: AdapterVenuesMapBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() {
                listener?.invoke(venues[adapterPosition])
            }
        }

        fun bind(venue: Venue) {
            binding?.venue = venue
        }
    }
}