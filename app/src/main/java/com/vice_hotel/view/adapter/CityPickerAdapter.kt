package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterCategoryPickerBinding
import com.vice_hotel.databinding.AdapterCityPickerBinding
import com.vice_hotel.model.data.Category
import com.vice_hotel.model.data.WifiCountry

class CityPickerAdapter: RecyclerView.Adapter<CityPickerAdapter.ViewHolder>() {

    private val list = ArrayList<WifiCountry>()
    private var callback: ((WifiCountry) -> Unit)? = null

    fun addAll(countries: ArrayList<WifiCountry>?) {
        if (countries!=null) {
            list.clear()
            list.addAll(countries)
            notifyDataSetChanged()
        }
    }

    fun setOnSelectItem(callback: ((WifiCountry) -> Unit)) {
        this.callback = callback
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_city_picker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view)  {

        var binding: AdapterCityPickerBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() {
                val country = list[adapterPosition]
                callback?.invoke(country)
                notifyDataSetChanged()

            }
        }

        fun bind(country: WifiCountry) {
            binding?.country = country
        }

    }
}