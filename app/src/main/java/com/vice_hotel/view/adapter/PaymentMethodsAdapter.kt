package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterPaymentMethodsBinding
import com.vice_hotel.model.data.CardInfor

class PaymentMethodsAdapter: RecyclerView.Adapter<PaymentMethodsAdapter.ViewHolder>() {

    private val cards = ArrayList<CardInfor>()
    private var callBack: ((CardInfor) -> Unit)? = null

    fun addAll(items: ArrayList<CardInfor>?) {
        if (items!=null) {
            cards.clear()
            cards.addAll(items)
            notifyDataSetChanged()
        }
    }

    fun setOnItemSelected(callBack: ((CardInfor) -> Unit)?) {
        this.callBack = callBack
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_payment_methods

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentMethodsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = cards.size

    override fun onBindViewHolder(holder: PaymentMethodsAdapter.ViewHolder, position: Int) {
        holder.bindItem(cards[position])
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var bind: AdapterPaymentMethodsBinding? = null

        init {
            bind = DataBindingUtil.bind(itemView)
        }

        fun bindItem(card: CardInfor) {
            bind?.card = card
            itemView.setOnClickListener() {
                callBack?.invoke(card)
            }

        }
    }
}