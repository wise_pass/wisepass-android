package com.vice_hotel.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterHistoryBinding
import com.vice_hotel.model.data.HistoryVenue
import com.vice_hotel.model.data.VenueItem
import com.vice_hotel.presenter.lib.glide.GlideUtil
import kotlinx.android.synthetic.main.adapter_history.view.*

class HistoryAdapter: RecyclerView.Adapter<HistoryAdapter.VenueItemHolder>() {

    private val items = ArrayList<HistoryVenue>()

    fun addAll(venues: ArrayList<HistoryVenue>?) {
        if (venues!=null) {
            items.clear()
            items.addAll(venues)
            notifyDataSetChanged()
        }
    }

    inner class VenueItemHolder(view: View): RecyclerView.ViewHolder(view) {

        var bind: AdapterHistoryBinding? = null

        init {
            bind = DataBindingUtil.bind(itemView)
        }

        fun bindVenueItem(venue: HistoryVenue) {
            bind?.data = venue
        }
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_history

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return VenueItemHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: VenueItemHolder, position: Int) {
        holder.bindVenueItem(items[position])
    }
}