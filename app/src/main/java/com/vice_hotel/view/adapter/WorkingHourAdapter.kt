package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterWorkingHourBinding
import com.vice_hotel.model.data.WorkingHour

class WorkingHourAdapter: RecyclerView.Adapter<WorkingHourAdapter.ViewHolder>() {

    private val workingHours = ArrayList<WorkingHour>()

    fun addAll(hours: ArrayList<WorkingHour>?) {
        if (hours!=null) {
            workingHours.clear()
            workingHours.addAll(hours)
            notifyDataSetChanged()
        }
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_working_hour

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = workingHours.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(workingHours[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var binding: AdapterWorkingHourBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }

        fun bind(workingHour: WorkingHour) {
            binding?.workingHour = workingHour
        }

    }
}