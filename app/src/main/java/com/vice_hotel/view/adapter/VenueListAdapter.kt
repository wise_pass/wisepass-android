package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterVenueListBinding
import com.vice_hotel.model.data.Venue
import java.util.*
import kotlin.collections.ArrayList

class VenueListAdapter: RecyclerView.Adapter<VenueListAdapter.ViewHolder>() {

    val venues = ArrayList<Venue>()
    val venuesTemp = ArrayList<Venue>()
    var callBack: ((Venue)->Unit)? = null

    fun keepDataTemp(data: ArrayList<Venue>?) {
        if (data!=null) {
            venuesTemp.clear()
            venuesTemp.addAll(data)
        }
    }

    fun addAll(data: ArrayList<Venue>?) {
        if (data!=null) {
            venues.clear()
            val list = data
            Collections.sort(list, kotlin.Comparator { o1, o2 ->
                o1.name!!.trim().compareTo(o2.name!!.trim())
            })
            venues.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun searchByKeyword(keyword: String) {
        val listSearch = ArrayList<Venue>()
        for (venue in venuesTemp) {
            if (venue.name!!.toLowerCase().contains(keyword.toLowerCase())) {
                listSearch.add(venue)
            }
        }
        addAll(listSearch)
    }

    fun setOnSelectedVenue(callBack: ((Venue)->Unit)?) {
        this.callBack = callBack
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_venue_list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = venues.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(venues[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var binding: AdapterVenueListBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() {
                callBack?.invoke(venues[adapterPosition])
            }
        }

        fun bind(venue: Venue) {
            binding?.venue = venue
        }
    }

}
