package com.vice_hotel.view.adapter

import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterChallengeBinding
import com.vice_hotel.databinding.AdapterRewardBinding
import com.vice_hotel.model.data.Challenge
import com.vice_hotel.presenter.helper.AppUtil
import kotlinx.android.synthetic.main.adapter_challenge.view.*

class ChallengesAdapter: RecyclerView.Adapter<ChallengesAdapter.ViewHolder>() {

    private val challenges = ArrayList<Challenge>()
    private var listener: ((String?) -> Unit)? = null


    fun addAll(items: ArrayList<Challenge>?) {
        if (items!=null) {
            this.challenges.clear()
            this.challenges.addAll(items)
            notifyDataSetChanged()
        }
    }

    fun setOnItemSelectedListener(listener: ((String?) -> Unit)?) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_challenge

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = challenges.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(challenges[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var binding: AdapterChallengeBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.btnLearnMore.setOnClickListener() {
                listener?.invoke(challenges[adapterPosition].memberChallengeId)
            }
        }

        fun bind(challenge: Challenge) {
            binding?.data = challenge

        }
    }
}