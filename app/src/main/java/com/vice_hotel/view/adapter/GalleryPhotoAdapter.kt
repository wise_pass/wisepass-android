package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.presenter.lib.glide.GlideUtil
import kotlinx.android.synthetic.main.adapter_gallery_horizontal.view.*

class GalleryPhotoAdapter: RecyclerView.Adapter<GalleryPhotoAdapter.ImageHolder>() {

    private val items = ArrayList<String>()

    private var callback: ((Int) -> Unit)? = null

    fun addPhotos(urls: ArrayList<String>?) {
        if (!urls.isNullOrEmpty()) {
            items.clear()
            items.addAll(urls)
            notifyDataSetChanged()
        }
    }

    fun setOnClickItem(callback: ((Int) -> Unit)) {
        this.callback = callback
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_gallery_photo

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ImageHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ImageHolder(view: View): RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener() {
                callback?.invoke(adapterPosition)
            }
        }

        fun bind(url: String) {
            GlideUtil.load(itemView.context, url.trim(), itemView.img)
        }
    }
}