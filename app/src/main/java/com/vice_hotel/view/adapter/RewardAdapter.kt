package com.vice_hotel.view.adapter

import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterRewardBinding
import com.vice_hotel.model.data.Reward
import com.vice_hotel.presenter.helper.AppUtil
import kotlinx.android.synthetic.main.adapter_reward.view.*

class RewardAdapter: RecyclerView.Adapter<RewardAdapter.ViewHolder>() {

    private val rewards = ArrayList<Reward>()
    private var listener: ((String?) -> Unit)? = null


    fun addAll(rewards: ArrayList<Reward>?) {
        if (rewards!=null) {
            this.rewards.clear()
            this.rewards.addAll(rewards)
            notifyDataSetChanged()
        }
    }

    fun setOnItemSelectedListener(listener: ((String?) -> Unit)?) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_reward

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = rewards.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(rewards[position])
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private var timer: CountDownTimer? = null
        var binding: AdapterRewardBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.btnScanPass.setOnClickListener() {
                listener?.invoke(rewards[adapterPosition].id)
            }
        }

        fun bind(reward: Reward) {

            binding?.reward = reward
            if (timer != null) {
                timer?.cancel();
            }
            timer = object : CountDownTimer(reward.seconds!!*1000L, 1000) {
                override fun onFinish() {}

                override fun onTick(millisUntilFinished: Long) {
                    val second = millisUntilFinished / 1000L
                    try {
                        if (rewards.isNotEmpty()) {
                            rewards[adapterPosition].seconds = second
                        }
                    } catch (ex: Exception) {}
                    itemView.rewardAvaiableDate?.text = " ${AppUtil.convertSecondToDynamic(second.toInt())}"
                }

            }.start()

        }
    }
}