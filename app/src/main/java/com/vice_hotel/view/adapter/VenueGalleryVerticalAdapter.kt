package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterVenueGalleryVerticalBinding
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.view.fragment.VenueGalleryVerticalFragmentDirections
import kotlinx.android.synthetic.main.adapter_venue_gallery_vertical.view.*

class VenueGalleryVerticalAdapter: RecyclerView.Adapter<VenueGalleryVerticalAdapter.ViewHolder>() {

    val urls = ArrayList<String>()
    private var listener: ((Int) -> Unit)? = null

    fun setOnItemClickListener(listener: ((Int) -> Unit)) {
        this.listener = listener
    }

    fun addAll(items: ArrayList<String>?) {
        if (items!=null) {
            urls.clear()
            for (index in 0 until items.size) {
                urls.add(items[index].trim())
            }
            notifyDataSetChanged()
        }
    }

    fun addAllMore(items: ArrayList<String>?) {
        if (items!=null) {
            val position = urls.size
            for (index in 0 until items.size) {
                urls.add(items[index].trim())
            }
            notifyItemRangeInserted(position, urls.size)
        }
    }

    fun add(url: String) {
        urls.add(url)
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var binding: AdapterVenueGalleryVerticalBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() {
                listener?.invoke(adapterPosition)
            }
        }

        fun bind() {
            binding?.url = urls[adapterPosition]
        }
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_venue_gallery_vertical


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueGalleryVerticalAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = urls.size

    override fun onBindViewHolder(holder: VenueGalleryVerticalAdapter.ViewHolder, position: Int) {
        holder.bind()
    }

}
