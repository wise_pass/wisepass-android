package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterHomeVenueBinding
import com.vice_hotel.databinding.AdapterSearchVenueBinding
import com.vice_hotel.model.data.Venue

class HomeVenueAdapter: RecyclerView.Adapter<HomeVenueAdapter.VenueHolder>() {

    var venues = ArrayList<Venue?>()
    var listener: ((Venue) -> Unit)? = null

    fun addAll(items: ArrayList<Venue>?) {
        if (items!=null) {
            venues.clear()
            val newArr = ArrayList<Venue?>()
            newArr.add(null)
            newArr.addAll(items)
            venues.addAll(newArr)
            notifyDataSetChanged()
        }
    }

    fun setOnSelectedVenueListener(listener: ((Venue) -> Unit)) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int =
        when (venues[position]) {
            null -> R.layout.adapter_view_margin
            else -> R.layout.adapter_home_venue
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return VenueHolder(view)
    }

    override fun getItemCount(): Int = venues.size

    override fun onBindViewHolder(holder: VenueHolder, position: Int) {
        holder.bind(venues[position])
    }

    inner class VenueHolder(view: View): RecyclerView.ViewHolder(view) {

        private var binding: AdapterHomeVenueBinding? = null

        init {

            itemView.setOnClickListener() {
                val venue = venues[adapterPosition]
                if (venue!=null) {
                    listener?.invoke(venue)
                }

            }
        }

        fun bind(venue: Venue?) {
            if (venue!=null) {
                binding = DataBindingUtil.bind(itemView)
                binding?.venue = venue
            }
        }

    }
}