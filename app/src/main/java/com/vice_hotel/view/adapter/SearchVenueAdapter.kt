package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterSearchVenueBinding
import com.vice_hotel.model.data.Venue

class SearchVenueAdapter: RecyclerView.Adapter<SearchVenueAdapter.VenueHolder>() {

    var venues = ArrayList<Venue>()
    var listener: ((Venue) -> Unit)? = null

    fun addAll(items: ArrayList<Venue>?) {
        if (items!=null) {
            venues.clear()
            venues.addAll(items)
        }
    }

    fun setOnSelectedVenueListener(listener: ((Venue) -> Unit)) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_search_venue

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return VenueHolder(view)
    }

    override fun getItemCount(): Int = venues.size

    override fun onBindViewHolder(holder: VenueHolder, position: Int) {
        holder.bind()
    }

    inner class VenueHolder(view: View): RecyclerView.ViewHolder(view) {

        private var bind: AdapterSearchVenueBinding? = null

        init {
            bind = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() { listener?.invoke(venues[adapterPosition]) }
        }

        fun bind() {
            bind?.venue = venues[adapterPosition]
        }

    }
}