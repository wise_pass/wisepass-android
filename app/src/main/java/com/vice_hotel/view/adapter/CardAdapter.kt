package com.vice_hotel.view.adapter

import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.vice_hotel.R
import com.vice_hotel.view.BaseFragment

class CardAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments: ArrayList<BaseFragment> = ArrayList()

    fun clearAll() = fragments.clear()

    fun addFragment(baseFragment: BaseFragment) = fragments.add(baseFragment)

    fun getCardViewAt(position: Int?): CardView? {
        if (position!=null)
            return fragments[position].fragmentView?.findViewById<CardView>(R.id.cardView)
        return null
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size
}