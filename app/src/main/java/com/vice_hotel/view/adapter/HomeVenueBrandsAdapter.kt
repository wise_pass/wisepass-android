package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterHomeVenueBrandsBinding
import com.vice_hotel.databinding.AdapterHomeVenueCategoryBinding
import com.vice_hotel.model.data.Brand
import com.vice_hotel.model.data.Category

class HomeVenueBrandsAdapter: RecyclerView.Adapter<HomeVenueBrandsAdapter.VenueHolder>() {

    var brands = ArrayList<Brand>()
    var listener: ((Brand) -> Unit)? = null
    var scrollPosition: ((Int) -> Unit)? = null

    fun addAll(items: ArrayList<Brand>?) {
        if (items!=null) {
            brands.clear()
            brands.addAll(items)
            notifyDataSetChanged()
        }
    }

    fun setOnSelectedVenueListener(listener: ((Brand) -> Unit)) {
        this.listener = listener
    }

    fun getCurrentPosition(listener: ((Int) -> Unit)) {
        this.scrollPosition = listener
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_home_venue_brands

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return VenueHolder(view)
    }

    override fun getItemCount(): Int = Integer.MAX_VALUE

    override fun onBindViewHolder(holder: VenueHolder, position: Int) {
        holder.bind()
    }

    inner class VenueHolder(view: View): RecyclerView.ViewHolder(view) {

        private var bind: AdapterHomeVenueBrandsBinding? = null

        init {
            bind = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() {
                val brand = brands.get(adapterPosition % brands.size)
                listener?.invoke(brand)
            }
        }

        fun bind() {
            val brand = brands.get(adapterPosition % brands.size)
            bind?.brand = brand
            scrollPosition?.invoke(adapterPosition)
        }

    }
}