package com.vice_hotel.view.adapter.support

import android.content.Context
import android.view.View
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.ViewPager
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.view.adapter.CardAdapter

class ShadowTransformer: ViewPager.OnPageChangeListener, ViewPager.PageTransformer {


    private var viewPager: ViewPager? = null
    private var cardAdapter: CardAdapter? = null
    private var lastOffset: Float = 0f
    private var cardSelectedShadow = 0f
    private var cardDefaultShadow = 0f
    private var context: Context? = null

    constructor(context: Context?, viewPager: ViewPager, cardAdapter: CardAdapter) {
        this.context = context
        this.viewPager = viewPager
        viewPager.addOnPageChangeListener(this)
        this.cardAdapter = cardAdapter
        cardSelectedShadow = AppUtil.dpToPx(context, 1)!!
        cardDefaultShadow = AppUtil.dpToPx(context, 5)!!
    }

    override fun transformPage(page: View, position: Float) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        var realCurrentPosition: Int? = null
        var nextPosition: Int? = null
        val goingLeft = (lastOffset.compareTo(positionOffset) < 0)
        when (goingLeft) {
            true -> {
                realCurrentPosition = position + 1
                nextPosition = position
            }
            false -> {
                nextPosition = position + 1
                realCurrentPosition = position
            }
        }
        // Avoid crash on overscroll
        if (nextPosition > cardAdapter!!.count - 1 || realCurrentPosition > cardAdapter!!.count - 1) return

        val currentCard = cardAdapter?.getCardViewAt(realCurrentPosition)

        if (currentCard != null) {
            currentCard.cardElevation = cardDefaultShadow
        }

        val nextCard = cardAdapter?.getCardViewAt(nextPosition);

        if (nextCard != null) {
            nextCard.cardElevation = cardSelectedShadow
        }

        lastOffset = positionOffset;
    }
    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageSelected(position: Int) {}

}