package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.model.data.FacebookVideo
import com.vice_hotel.presenter.lib.glide.GlideUtil
import kotlinx.android.synthetic.main.adapter_facebook_video.view.*

class FacebookVideoAdapter: RecyclerView.Adapter<FacebookVideoAdapter.ViewHolder>() {


    private var callback: ((String?) -> Unit)? = null
    private val items = ArrayList<FacebookVideo>()

    fun addAll(facebookVideos: ArrayList<FacebookVideo>?) {
        if (facebookVideos!=null) {
            items.clear()
            items.addAll(facebookVideos)
            notifyDataSetChanged()
        }
    }

    fun setOnClickItem(callback: ((String?) -> Unit)) {
        this.callback = callback
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_facebook_video

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener() {
                callback?.invoke(items[adapterPosition].source)
            }
        }

        fun bind(url: String?) {
            GlideUtil.load(itemView.context, url?.trim(), itemView.img)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FacebookVideoAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FacebookVideoAdapter.ViewHolder, position: Int) {
        holder.bind(items[position].urlImage)
    }
}