package com.vice_hotel.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.vice_hotel.view.BaseFragment
import java.util.*

class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments: ArrayList<BaseFragment> = ArrayList()

    fun clearAll() = fragments.clear()

    fun addFragment(baseFragment: BaseFragment) = fragments.add(baseFragment)

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

}