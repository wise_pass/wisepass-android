package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterChallengeConditionBinding
import com.vice_hotel.model.data.Condition

class ChallengeConditionAdapter: RecyclerView.Adapter<ChallengeConditionAdapter.ViewHolder>() {

    private val conditions = ArrayList<Condition>()

    fun addAll(items: ArrayList<Condition>?) {
        if (items!=null) {
            conditions.clear()
            conditions.addAll(items)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int = conditions.size

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        var binding: AdapterChallengeConditionBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }

        fun bind() {
            val condition = conditions[adapterPosition]
            binding?.data = condition
        }

    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_challenge_condition

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChallengeConditionAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChallengeConditionAdapter.ViewHolder, position: Int) = holder.bind()
}