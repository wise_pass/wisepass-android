package com.vice_hotel.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.AdapterHomeVenueCategoryBinding
import com.vice_hotel.model.data.Category

class HomeVenueCategoryAdapter: RecyclerView.Adapter<HomeVenueCategoryAdapter.VenueHolder>() {

    var categories = ArrayList<Category>()
    var listener: ((Category) -> Unit)? = null

    fun addAll(items: ArrayList<Category>?) {
        if (items!=null) {
            categories.clear()
            categories.addAll(items)
            notifyDataSetChanged()
        }
    }

    fun setOnSelectedVenueListener(listener: ((Category) -> Unit)) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int = R.layout.adapter_home_venue_category

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return VenueHolder(view)
    }

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: VenueHolder, position: Int) {
        holder.bind()
    }

    inner class VenueHolder(view: View): RecyclerView.ViewHolder(view) {

        private var bind: AdapterHomeVenueCategoryBinding? = null

        init {
            bind = DataBindingUtil.bind(itemView)
            itemView.setOnClickListener() { listener?.invoke(categories[adapterPosition]) }
        }

        fun bind() {
            bind?.category = categories[adapterPosition]
        }

    }
}