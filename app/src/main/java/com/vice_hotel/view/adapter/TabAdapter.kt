package com.vice_hotel.view.adapter

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.vice_hotel.view.BaseFragment
import java.util.ArrayList

class TabAdapter(fragment: FragmentManager): FragmentPagerAdapter(fragment) {

    private val mFragmentList = ArrayList<BaseFragment>()
    private val mFragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): BaseFragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: BaseFragment, title: String?) {
        mFragmentList.add(fragment)
        if (title.isNullOrEmpty()) {
            mFragmentTitleList.add("")
        } else {
            mFragmentTitleList.add(title)
        }

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }
}