package com.vice_hotel.view.fragment

import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentReferralBinding
import com.vice_hotel.model.data.UserReferral
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.AccountModel
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_referral.*

class ReferralFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_referral

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() {
            activity?.onBackPressed()
        }
        loadData()
    }

    private fun loadData() {
        DialogUtil.OpenLoadingDialog(activity!!)
        ViewModelProviders.of(this).get(AccountModel::class.java).getUserReferral(context)
            .observeOnce(this, Observer {
                DialogUtil.dismiss()
            if (it.isSuccess()) {
                if (it.data?.userReferral!=null) {
                    loadUI(it.data.userReferral)
                }
            } else {
                DialogUtil.OpenCustomDialog(activity, getString(R.string.error), it.message, R.string.close, null) {
                    DialogUtil.dismiss()
                    activity?.onBackPressed()
                }
            }
        })
    }

    private fun loadUI(userReferral: UserReferral) {
        val bind: FragmentReferralBinding? = DataBindingUtil.bind(fragmentView!!)
        bind?.referral = userReferral
        val bitmap = AppUtil.generalQRCode(context, userReferral.code)
        if (bitmap!=null) {
            imgQrCode.setImageBitmap(bitmap)
        }
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = WebViewClient()
        webView.webChromeClient = WebChromeClient()
        webView.loadUrl(userReferral.termConditionLink)
    }

}