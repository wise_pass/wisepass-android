package com.vice_hotel.view.fragment.promotion

import android.os.Bundle
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.plan_payment.CardDiscoverySubscribeFragment
import kotlinx.android.synthetic.main.fragment_promotion_confirm.*

class PromotionConfirmFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_promotion_confirm

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
    }
}