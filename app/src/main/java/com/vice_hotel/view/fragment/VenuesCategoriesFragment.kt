package com.vice_hotel.view.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.vice_hotel.R
import com.vice_hotel.model.data.Brand
import com.vice_hotel.model.data.Category
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.PagerAdapter
import com.vice_hotel.view.adapter.TabAdapter
import kotlinx.android.synthetic.main.fragment_venues_categories.*

class VenuesCategoriesFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_venues_categories

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        loadVenues()
    }

    private fun loadVenues() {
        val viewModel = ViewModelProviders.of(this).get(VenueModel::class.java).getHomeVenues(context)
        viewModel.observeOnce(this, Observer {
            if (it.isSuccess()) {
                dataReady(it.data?.brands)
            }
        })
    }

    private fun dataReady(brands: ArrayList<Brand>?) {
        if (brands!=null) {
            val adapter = TabAdapter(childFragmentManager)
            adapter.addFragment(VenuesByCategoriesFragment.newInstance("nearby"), getString(R.string.all))
            for (brand in brands) {
                adapter.addFragment(VenuesByCategoriesFragment.newInstance(brand.keyword), brand.display)
            }
            viewPager.adapter = adapter
            viewPager.addOnPageChangeListener(onPageChangedListener())
            tabLayout.setupWithViewPager(viewPager)
            AppUtil.changeTabsFont(context!!, tabLayout)
        }
    }
    private fun onPageChangedListener(): ViewPager.OnPageChangeListener {
        return object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
            }

        }
    }
}