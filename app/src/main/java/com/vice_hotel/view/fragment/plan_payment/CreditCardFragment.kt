package com.vice_hotel.view.fragment.plan_payment

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.braintreepayments.api.Card
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentCreditCardBinding
import com.vice_hotel.model.data.CardInfor
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.view.BaseFragment

private const val KEY = "card_credit"
class CreditCardFragment: BaseFragment() {

    companion object {
        fun newInstance(cardInfor: CardInfor?) = CreditCardFragment().apply {
            arguments = Bundle().apply {
                if (cardInfor!=null) putParcelable(KEY, cardInfor)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_credit_card

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val card = arguments?.getParcelable<CardInfor>(KEY)
        val binding: FragmentCreditCardBinding? = DataBindingUtil.bind(fragmentView!!)
        binding?.card = card
    }
}