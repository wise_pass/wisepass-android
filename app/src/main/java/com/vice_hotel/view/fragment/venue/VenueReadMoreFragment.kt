package com.vice_hotel.view.fragment.venue

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.google.gson.JsonArray
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentVenueReadMoreBinding
import com.vice_hotel.model.data.FacebookVideo
import com.vice_hotel.model.data.WorkingHour
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.WebActivity
import com.vice_hotel.view.adapter.FacebookVideoAdapter
import com.vice_hotel.view.adapter.GalleryHorizontalAdapter
import com.vice_hotel.view.adapter.WorkingHourAdapter
import kotlinx.android.synthetic.main.fragment_venue_read_more.*
import org.json.JSONArray
import org.json.JSONObject

class VenueReadMoreFragment : BaseFragment() {

    private val args: VenueReadMoreFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_venue_read_more

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        Handler().postDelayed({
            getVenueReadMore()
        }, 300)
    }

    private fun getVenueReadMore() {
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenueReadMoreById(context, args.venueId)
            .observeOnce(this, Observer {
                if (it.isSuccess()) {
                    val bind: FragmentVenueReadMoreBinding? = DataBindingUtil.bind(fragmentView!!)
                    bind?.venue = it.data?.venue
                    setGallery(it.data?.venue?.gallery)
                    setWorkingHours(it.data?.venue?.workingHour)
                    if (AccessToken.getCurrentAccessToken()!=null && AccessToken.isCurrentAccessTokenActive()) {
                        getVideosFacebook(it?.data?.venue?.facebook)
                    }
                    btnSeeAllGalleries.setOnClickListener() { view ->
                        val directions = VenueReadMoreFragmentDirections.actionVenueReadMoreFragmentToVenueGalleryVerticalFragment(venueId = bind?.venue?.id)
                        findNavController().navigate(directions)
                    }
                }
            })
    }

    private fun setGallery(photos: ArrayList<String>?) {
        val galleryAdapter = GalleryHorizontalAdapter()
        RecyclerUtil.setUpHorizontal(context, rclGallery, galleryAdapter)
        galleryAdapter.addPhotos(photos)
    }

    private fun setVideos(data: ArrayList<FacebookVideo>?) {
        val adapter = FacebookVideoAdapter()
        adapter.setOnClickItem {
            WebActivity.startActivity(activity as BaseActivity, it)
        }
        RecyclerUtil.setUpHorizontal(context, rclVideos, adapter)
        adapter.addAll(data)
    }

    private fun setWorkingHours(data: ArrayList<WorkingHour>?) {
        val adapter = WorkingHourAdapter()
        RecyclerUtil.setUp(context, rclTime, adapter)
        adapter.addAll(data)
    }

    private fun getVideosFacebook(facebookId: String?) {
        if (AccessToken.getCurrentAccessToken()!=null) {
            val arrVideo = ArrayList<FacebookVideo>()
            GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(),"/${facebookId}/videos") { response ->
                if (response!=null && response.jsonObject!=null) {
                    fbVideos.visibility = View.VISIBLE
                    val js = response.jsonObject.get("data")
                    if (js is JSONArray) {
                        val jsons = js as JSONArray
                        for (index in 0 until jsons.length()) {
                            val json = jsons.getJSONObject(index)
                            if (json!=null) {
                                val id = json.getString("id")
                                val facebookVideo = FacebookVideo(
                                    urlImage = "https://graph.facebook.com/$id/picture?type=large",
//                                    source = "https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook%2Fvideos%2F${id}%2F"
                                    source = "https://www.facebook.com/video/embed?video_id=${id}"
                                )
                                arrVideo.add(facebookVideo)
                            }

                        }
                        setVideos(arrVideo)
                    }

                }


            }.executeAsync()
        }

    }
}