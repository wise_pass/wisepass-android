package com.vice_hotel.view.fragment.dashboard

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.history.HistoryModel
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.HistoryAdapter
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment: BaseFragment() {

    var adapter: HistoryAdapter? = null

    override fun getLayoutRes(): Int = R.layout.fragment_history

    override fun onViewCreated(savedInstanceState: Bundle?) {
        adapter = HistoryAdapter()
        RecyclerUtil.setUp(context, rcl, adapter)
        loadHistory()
    }

    private fun loadHistory() {
        ViewModelProviders.of(this).get(HistoryModel::class.java).getHistory(context).observeOnce(this, Observer {
            progressBar.visibility = View.GONE
            if (it.isSuccess()) {
                adapter?.addAll(it.data?.history_items?.listHistory)
            }
        })
    }
}