package com.vice_hotel.view.fragment.registration

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.provider.Settings
import android.security.keystore.KeyProperties
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentSignUpInputEmailBinding
import com.vice_hotel.model.data.RequestLogin
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.view_model.SignUpModel
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up_input_email.actionBack
import kotlinx.android.synthetic.main.fragment_sign_up_input_email.btnClearEmail
import kotlinx.android.synthetic.main.fragment_sign_up_input_email.btnNext
import kotlinx.android.synthetic.main.fragment_sign_up_input_email.inputEmail
import kotlinx.android.synthetic.main.fragment_sign_up_input_email.textNoted
import kotlinx.android.synthetic.main.fragment_sign_up_input_email.txtErrorWrongEmail
import javax.crypto.Cipher

class SignUpInputEmailFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_sign_up_input_email

    var contextLocale: Context? = null

    override fun onViewCreated(savedInstanceState: Bundle?) {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        loadContentOnUI()
        listener()
        //requestFingerprint()
    }

    private fun loadContentOnUI() {
        val binding: FragmentSignUpInputEmailBinding? = DataBindingUtil.bind(fragmentView!!)
        val lang = LocaleManager.getLanguage(context)
        LocaleManager.changeLocateContext(context, lang) {
            contextLocale = it
            binding?.tellUsYourEmail = it?.getString(R.string.tell_us_your_email)
            binding?.hintTypeYourEmailHere = it?.getString(R.string.type_your_email_here)
            binding?.noted = it?.getString(R.string.we_will_send_an_email_with_verification_code)
            binding?.alreadyAnUser = it?.resources?.getString(R.string.already_an_user)
            binding?.login = it?.resources?.getString(R.string.log_in)
        }
    }

    private fun listener() {
        inputEmail.doAfterTextChanged {
            val text = it.toString()
            btnClearEmail.visibility = when (text.isEmpty()) {
                true -> View.GONE
                false -> View.VISIBLE
            }
            btnNext.isEnabled = AppUtil.isEmailValid(text)
            if (!AppUtil.isNotContainSpecialCharacter(text)) {
                if (!AppUtil.isEmailValid(text)) {
                    labelStatusForValidEmail(true, contextLocale?.resources?.getString(R.string.wrong_email_format))
                    return@doAfterTextChanged
                }
            }
            labelStatusForValidEmail(false, contextLocale?.resources?.getString(R.string.tell_us_your_email))
        }
        btnClearEmail.setOnClickListener() { inputEmail.setText(null, TextView.BufferType.EDITABLE) }
        btnNext.setOnClickListener() {
            AppUtil.hideKeyboard(activity as BaseActivity)
            DialogUtil.OpenLoadingDialog(activity)
            val requestLogin = AppManager.instance.getRequestLogin().apply {
                email = inputEmail.text.toString().trim()
            }
            ViewModelProviders.of(this).get(SignUpModel::class.java).validateEmail(context, requestLogin).observe(this, Observer {
                DialogUtil.dismiss()
                if (it.statusCode== HTTP_OK) {
                    val nav =
                        SignUpInputEmailFragmentDirections.actionSignUpInputEmailFragmentToSignUpInputOTPFragment(
                            inputEmail.text.toString().trim()
                        )
                    findNavController().navigate(nav)
                } else {
                    btnClearEmail.performClick()
                    labelStatusForValidEmail(true, it.message)
                }
            })

        }
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        textNoted.setOnClickListener () { findNavController().navigate(R.id.action_global_termsConditionsFragment) }
        val email = PreferenceUtils(context).get(PreferenceUtils.EMAIL_REGISTRATION, String.javaClass) as String
        if (email.isNotEmpty()) {
            inputEmail.setText(email)
        }
    }
    private fun labelStatusForValidEmail(isError: Boolean, text: String?) {
        if (isError) {
            txtErrorWrongEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_circle_error_16dp, 0, 0, 0)
            txtErrorWrongEmail.setTextColor(Color.parseColor("#e64c54"))
        } else {
            txtErrorWrongEmail.setCompoundDrawables(null, null, null, null)
            txtErrorWrongEmail.setTextColor(Color.parseColor("#80FFFFFF"))
        }
        txtErrorWrongEmail.text = text
    }

    private fun requestFingerprint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.USE_BIOMETRIC)
                == PackageManager.PERMISSION_GRANTED){
                fingerAuthor()
            } else {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1021)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==1021) {
            if (permissions[0].contentEquals(Manifest.permission.USE_BIOMETRIC) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fingerAuthor()
            }
        }
    }

    private fun fingerAuthor() {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.USE_BIOMETRIC)
            == PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val executor = activity?.mainExecutor
                val cancelListener = DialogInterface.OnClickListener { _, _ ->

                }
                val biometricPrompt = BiometricPrompt.Builder(context)
                    .setTitle("Title")
                    .setSubtitle("Subtitle")
                    .setDescription("Description")
                    .setNegativeButton("Cancel", executor!!, cancelListener)
                    .build()
                val cipherString = "${KeyProperties.KEY_ALGORITHM_AES}/${KeyProperties.BLOCK_MODE_CBC}/${KeyProperties.ENCRYPTION_PADDING_PKCS7}"
                val cipher: Cipher = Cipher.getInstance(cipherString)
                val cryptObject = BiometricPrompt.CryptoObject(cipher)
                val cancelSignal = CancellationSignal()
                cancelSignal.setOnCancelListener {  }
                biometricPrompt.authenticate(cancelSignal, executor,object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                        super.onAuthenticationSucceeded(result)

                    }

                    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                        super.onAuthenticationError(errorCode, errString)
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                    }
                } )
            }

        }
    }

}