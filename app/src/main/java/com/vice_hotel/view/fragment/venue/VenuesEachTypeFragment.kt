package com.vice_hotel.view.fragment.venue

import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentVenuesBinding
import com.vice_hotel.databinding.FragmentVenuesEachTypeBinding
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.HomeVenueAdapter
import com.vice_hotel.view.adapter.PagerAdapter
import com.vice_hotel.view.adapter.SearchVenueAdapter
import kotlinx.android.synthetic.main.fragment_venues_each_type.*

class VenuesEachTypeFragment: BaseFragment() {

    private val args: VenuesEachTypeFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_venues_each_type

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() {activity?.onBackPressed()}
        loadVenues()
    }

    private fun loadVenues() {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenuesByKeyType(context, args.keyType).observeOnce(this, Observer {
            DialogUtil.dismiss()
            Handler().postDelayed({fragmentReady(it)}, 200)
        })
    }

    private fun fragmentReady(it: ResponseBody) {
        if (it.isSuccess()) {
            val binding: FragmentVenuesEachTypeBinding? = DataBindingUtil.bind(fragmentView!!)
            initSlide(it.data?.slideMenu)
            initListVenues(it.data?.listItems)
            binding?.titleName = args.displayName
        } else {
            DialogUtil.OpenCustomDialogError(activity, getString(R.string.sorry), getString(R.string.network_offline), R.string.try_again, null) {
                DialogUtil.dismiss()
                if (it) {
                    loadVenues()
                }
            }
        }
    }

    private fun initSlide(venues: ArrayList<Venue>?) {
        if (venues!=null) {
            val adapter = PagerAdapter(childFragmentManager)
            for (venue in venues) {
                adapter.addFragment(SlidePagerVenueTypeFragment.newInstance(venue))
            }
            viewPagerHomeSlide.adapter = adapter
            dots_indicator.setViewPager(viewPagerHomeSlide)
        }
    }

    private fun initListVenues(venues: ArrayList<Venue>?) {
        if (venues!=null) {
            val adapter = SearchVenueAdapter()
            adapter.setOnSelectedVenueListener {
                val directions = VenuesEachTypeFragmentDirections.actionVenuesEachTypeFragmentToVenueDetailFragment(it.id)
                findNavController().navigate(directions)
            }
            RecyclerUtil.setUp(context, rcl, adapter, 2)
            adapter.addAll(venues)
        }
    }

}