package com.vice_hotel.view.fragment.dashboard

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.facebook.AccessToken
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentMenuBinding
import com.vice_hotel.presenter.view_model.AccountModel
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.DashboardFragmentDirections
import kotlinx.android.synthetic.main.fragment_menu.*

class MenuFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_menu

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionLogout.setOnClickListener() { AppManager.logOut(activity as BaseActivity) }
        getProfile()
        lnProfile.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_profileFragment)
        }
        lnAboutWisePass.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_aboutWisePassFragment)
        }
        lnRewards.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_challengesRewardsFragment)
        }
        lnPlanBilling.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_planBillingFragment)
        }
        lnReferral.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_referralFragment)
        }
        lnConnection.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_connectionsFragment)
        }
        lnCity.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_cityPickerFragment)
        }
        lnBuyPass.setOnClickListener() {
            val direction = DashboardFragmentDirections.actionDashboardFragmentToCheckOutFragment2(null, Constant.REWARD)
            findNavController().navigate(direction)
        }
        lnBuyPass.visibility = View.GONE
    }

    private fun getProfile() {
        ViewModelProviders.of(this).get(AccountModel::class.java).getProfile(context).observeOnce(this, Observer {
            if (it.isSuccess()) {
                val binding: FragmentMenuBinding? = DataBindingUtil.bind(fragmentView!!)
                val userProfile = it.data?.user_profile
                if (userProfile!=null) {
                    binding?.userAccount = userProfile
                }

            }
        })
    }
}