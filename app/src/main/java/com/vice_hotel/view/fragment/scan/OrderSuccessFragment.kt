package com.vice_hotel.view.fragment.scan

import android.graphics.Color
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.view.setPadding
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentOrderSuccessBinding
import com.vice_hotel.model.data.ScanConfirm
import com.vice_hotel.model.data.VenueItem
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.MemberType
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.presenter.view_model.AccountModel
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_order_success.*
import java.util.*

private const val TYPE_STARBUCKS = "STARBUCKS"
private const val LUNCH = "lunch"
private const val DINNER = "dinner"
private const val SINGHA = "singha"
class OrderSuccessFragment: BaseFragment() {

    private var timer: CountDownTimer? = null

    private val args: OrderSuccessFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_order_success

    override fun onViewCreated(savedInstanceState: Bundle?) {
        btnClose.setOnClickListener() {
            activity?.onBackPressed()
        }
        addVenueToUI(args.venueItem, args.scanConfirm)
    }

    private fun addVenueToUI(venueItem: VenueItem?, scanConfirm: ScanConfirm?) {
        if (venueItem!=null) {
            val binding: FragmentOrderSuccessBinding? = DataBindingUtil.bind(fragmentView!!)
            binding?.venueItem = venueItem
            //
            if (scanConfirm?.code.isNullOrEmpty()) {
                when (scanConfirm?.productConfirmType) {
                    TYPE_STARBUCKS -> {
                        imgBadge.setPadding(AppUtil.dpToPx(context, 16)!!.toInt())
                        imgBadge.scaleType = ImageView.ScaleType.FIT_CENTER
                        GlideUtil.load(context, "${scanConfirm.imageUrl}", imgBadge)
                    }
                    else -> {
                        val name = scanConfirm?.productName
                        var uri: Uri? = null
                        if (!name.isNullOrEmpty()) {
                            when {
                                name.toLowerCase().trim().contains(LUNCH) -> uri = Uri.parse("android.resource://" + context?.packageName + "/" + R.raw.lunch)
                                name.toLowerCase().trim().contains(SINGHA) -> uri = Uri.parse("android.resource://" + context?.packageName + "/" + R.raw.singha)
                                name.toLowerCase().trim().contains(DINNER) -> uri = Uri.parse("android.resource://" + context?.packageName + "/" + R.raw.dinner)
                            }
                        }
                        if (uri!=null) {
                            if (name!!.toLowerCase().contains("singha")) {
                                val layoutParamsCompat =  RelativeLayout.LayoutParams(AppUtil.dpToPx(context, 250)!!.toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)
                                layoutParamsCompat.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
                                videoView.layoutParams = layoutParamsCompat
                                videoView.requestLayout()
                                //send mail remind when user don't package before.
                                ViewModelProviders.of(this).get(AccountModel::class.java)
                                    .sendMailRemind(context).observeOnce(this, androidx.lifecycle.Observer {})
                            }
                            videoView.setVideoURI(uri)
                            videoView.setMediaController(null)
                            videoView.setOnPreparedListener { it.isLooping = true }
                            videoView.start()
                            videoView.visibility = View.VISIBLE
                        } else {
                            GlideUtil.load(context, "${venueItem.imageUrl}", imgBadge)
                        }

                    }
                }

            } else {
                txtCode.text = scanConfirm?.code
            }
            //
            var countTime = 0
            if (scanConfirm?.countDownSeconds!=null) {
                countTime = scanConfirm.countDownSeconds!!
            }
            if (countTime==0) {
                startUpDownTime()
            } else {
                startCountDown(countTime)
            }
            //
            setTheme(MemberType.detect(venueItem.userType))

        }
    }

    private fun startCountDown(countTime: Int) {
        txtCountTime.text = AppUtil.convertSecondToTime(countTime)
        timer = object : CountDownTimer(countTime*1000L, 1000) {
            override fun onFinish() {
                activity?.onBackPressed()
            }

            override fun onTick(millisUntilFinished: Long) {
                txtCountTime.text = AppUtil.convertSecondToTime((millisUntilFinished/1000).toInt())
            }

        }.start()
    }

    private fun startUpDownTime() {
        val date = Calendar.getInstance()
        var count = 0
        count = date.get(Calendar.HOUR_OF_DAY) * 60 * 60
        count += date.get(Calendar.MINUTE) * 60
        count += date.get(Calendar.SECOND)
        txtCountTime.text = AppUtil.convertSecondToTime(count)
        timer = object : CountDownTimer(1000000, 1000) {
            override fun onFinish() {
                activity?.onBackPressed()
            }

            override fun onTick(millisUntilFinished: Long) {
                val newTime = count + ((1000000-millisUntilFinished)/1000).toInt()
                txtCountTime.text = AppUtil.convertSecondToTime(newTime)
            }

        }.start()
    }

    private fun setTheme(memberType: MemberType) {
        when (memberType) {
            MemberType.PREMIUM -> {
                cardView.setCardBackgroundColor(resources.getColor(R.color.premium))
                txtMemberType.setTextColor(resources.getColor(R.color.premium))
                txtCode.setTextColor(Color.parseColor("#ffffff"))
                txtTitle.setTextColor(Color.parseColor("#ffffff"))
                txtTime.setTextColor(Color.parseColor("#80ffffff"))
                txtDate.setTextColor(Color.parseColor("#80ffffff"))
                lineView.setBackgroundColor(Color.parseColor("#30ffffff"))
                txtTime.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_clock_white_16dp, 0, 0 ,0)
                txtDate.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_calendar_white_16dp, 0, 0 ,0)
            }
            else -> {
                cardView.setCardBackgroundColor(resources.getColor(R.color.standard))
                txtMemberType.setTextColor(resources.getColor(R.color.standard))
                txtCode.setTextColor(Color.parseColor("#000000"))
                txtTitle.setTextColor(Color.parseColor("#000000"))
                txtTime.setTextColor(Color.parseColor("#80000000"))
                txtDate.setTextColor(Color.parseColor("#80000000"))
                lineView.setBackgroundColor(Color.parseColor("#30000000"))
                txtTime.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_clock_black_16dp, 0, 0 ,0)
                txtDate.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_calendar_black_16dp, 0, 0 ,0)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer?.cancel()
        if (videoView.isPlaying) {
            videoView.stopPlayback()
        }
    }

}