package com.vice_hotel.view.fragment.scan

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.SeekBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.model.data.Category
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.scan.QrCodeDetectedModel
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_qr_code_detected.*

class QrCodeDetectedFragment: BaseFragment() {

    private lateinit var qrCodeDetectedModel: QrCodeDetectedModel

    private val args: QrCodeDetectedFragmentArgs by navArgs()

    private var mFragmentTag: String? = null

    override fun getLayoutRes(): Int = R.layout.fragment_qr_code_detected

    override fun onViewCreated(savedInstanceState: Bundle?) {
        qrCodeDetectedModel = ViewModelProviders.of(this).get(QrCodeDetectedModel::class.java)
        listener()
        listenerForSlideButton()
        Handler().postDelayed({ loadData() }, 300)
    }

    private fun loadData() {
        qrCodeDetectedModel.getVenuesByBarcode(context, args.scanType).observeOnce(this, Observer {
            when (it.statusCode) {
                HTTP_OK -> {
                    loadCategories(it.data?.scan_venue_items?.categories)
                }
                else -> {
                    DialogUtil.OpenCustomDialogError(activity, getString(R.string.error), it.message, R.string.cancel, null) {
                        DialogUtil.dismiss()
                        activity?.onBackPressed()
                    }
                }
            }
        })
    }

    @SuppressLint("ResourceType")
    private fun loadCategories(categories: ArrayList<Category>?) {
        if (categories!=null) {
            val size = categories.size
            if (size > 0) {
                radioGroupMenu.weightSum = size.toFloat()
                for (index in 0 until size) {
                    val item = View.inflate(context, R.layout.item_qr_code_detected_menu, null) as RadioButton

                    val param = RadioGroup.LayoutParams(
                        0,
                        RadioGroup.LayoutParams.WRAP_CONTENT,
                        1.0f
                    )
                    item.layoutParams = param
                    item.id = index+1
                    item.text = categories[index].displayName
                    radioGroupMenu.addView(item)
                }
            }
            radioGroupMenu.setOnCheckedChangeListener { group, checkedId ->
                addFragmentVenues(categories[checkedId-1])
            }
            radioGroupMenu.check(1)
        }
    }

    private fun listener() {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
    }

    private fun addFragmentVenues(category: Category?) {
        mFragmentTag = category?.keyword
        fragmentManager?.beginTransaction()?.replace(R.id.qrCodeVenueContainer, QrCodeVenueFragment.newInstance(category), category?.keyword)?.commit()
    }

    private var isFlag = false

    private fun listenerForSlideButton() {
        uiStartState()
        btnSlide.setPadding(0,0,0,0)
        btnSlide.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (progress == 100) {
                    if (isFlag) {
                        return
                    }
                    isFlag = true
                    txtFinish.visibility = View.VISIBLE
                    btnSlide.isEnabled = false
                    if (fragmentManager?.fragments!=null) {
                        val qrCodeFragment = fragmentManager?.findFragmentByTag(mFragmentTag) as QrCodeVenueFragment
                        if (!qrCodeFragment.venueItem?.id.isNullOrEmpty()) {
                            DialogUtil.OpenLoadingDialog(activity)
                            val scanType = args.scanType
                            scanType?.productInBusinessVenueId = qrCodeFragment.venueItem?.id
                            qrCodeDetectedModel.confirmVenueAfterScan(context, args.scanType).observeOnce(this@QrCodeDetectedFragment, Observer {
                                    DialogUtil.dismiss()
                                    if (it.isSuccess()) {
                                        val nav = QrCodeDetectedFragmentDirections.actionQrCodeDetectedFragmentToOrderSuccessFragment(qrCodeFragment.venueItem, it.data?.scan_confirm)
                                        findNavController().navigate(nav)
                                    } else {
                                        DialogUtil.OpenCustomDialogError(activity, getString(R.string.error), it.message, R.string.cancel, null) {
                                            DialogUtil.dismiss()
                                            txtFinish.visibility = View.GONE
                                            btnSlide.isEnabled = true
                                            isFlag = false
                                            uiStartState()
                                        }
                                    }
                                })
                        } else {
                            AppUtil.toast(context, R.string.error, false)
                            activity?.onBackPressed()
                        }
                    }

                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                imgArrow.animation = null
                imgArrow.visibility = View.INVISIBLE
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                if (seekBar!!.progress < 80) {
                    uiStartState()
                } else {
                    seekBar.progress = 100
                }
            }

        })
    }

    private fun uiStartState() {
        btnSlide.progress = 10
        val animation = AnimationUtils.loadAnimation(context, R.anim.move_infinite)
        imgArrow.startAnimation(animation)
        imgArrow.visibility = View.VISIBLE
    }

}