package com.vice_hotel.view.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.data.SearchData
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.SearchVenueAdapter
import kotlinx.android.synthetic.main.fragment_venues_by_categories.*

class VenuesByCategoriesFragment: BaseFragment() {

    companion object {
        fun newInstance(type: String?) = VenuesByCategoriesFragment().apply {
            arguments = Bundle().apply {
                putString("TYPE", type)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_venues_by_categories

    override fun onViewCreated(savedInstanceState: Bundle?) {
        arguments.let {
            val type = it?.getString("TYPE")
            loadVenuesByCategory(type)
        }

    }

    private fun loadVenuesByCategory(category: String?) {
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenuesByKeyType(context, category).observe(this, Observer {
            if (it.statusCode == HTTP_OK) {
                val list = it.data?.listItems
                val adapter = SearchVenueAdapter()
                adapter.setOnSelectedVenueListener {
                    val directions = VenuesCategoriesFragmentDirections.actionVenuesCategoriesFragmentToVenueDetailFragment(it.id)
                    findNavController().navigate(directions)
                }
                adapter.addAll(list)
                RecyclerUtil.setUp(context, rclVenues, adapter, 2)
            }
        })
    }
}