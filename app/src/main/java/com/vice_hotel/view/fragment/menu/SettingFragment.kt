package com.vice_hotel.view.fragment.menu

import android.os.Bundle
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.PreferenceUtils
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_setting.*

class SettingFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_setting

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        val pushNotification = PreferenceUtils(activity).get(PreferenceUtils.PUSH_NOTIFICATION, Boolean.javaClass) as Boolean
        swPushNotification.isChecked = pushNotification
        swPushNotification.setOnCheckedChangeListener { _, isChecked ->
            PreferenceUtils(activity).put(PreferenceUtils.PUSH_NOTIFICATION, isChecked)
        }
    }
}