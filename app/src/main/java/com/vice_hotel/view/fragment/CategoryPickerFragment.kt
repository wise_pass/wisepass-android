package com.vice_hotel.view.fragment

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vice_hotel.R
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Category
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.CategoryPickerAdapter
import kotlinx.android.synthetic.main.fragment_category_picker.*
import java.util.*

class CategoryPickerFragment: BaseFragment() {

    private var adapter: CategoryPickerAdapter? = null

    override fun getLayoutRes(): Int = R.layout.fragment_category_picker

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        adapter = CategoryPickerAdapter()
        RecyclerUtil.setUp(context, rcl, adapter)
        loadCategories()
    }

    private fun loadCategories() {
        progressBar.visibility = View.VISIBLE
        ViewModelProviders.of(this).get(VenueModel::class.java).getHomeVenues(context).observe(this, Observer {
            progressBar.visibility = View.GONE
            Handler().postDelayed({fragmentReady(it)}, 200)
        })
    }

    private fun fragmentReady(it: ResponseBody) {
        if (it.isSuccess()) {
            val categories = it.data?.categories

            if (categories!=null) {
                Collections.sort(categories, kotlin.Comparator { o1, o2 -> o1.name!!.compareTo(o2.name!!) })
                var charTemp = ""
                val list = ArrayList<Category>()
                for (category in categories) {
                    val section = category.name!!.trim().substring(0,1)
                    if (!charTemp.contentEquals(section)) {
                        charTemp = section
                        list.add(Category(section = section, checked = false))
                    }
                    category.checked = false
                    list.add(category)
                }
                adapter?.addAll(list)

            }

        }
    }
}