package com.vice_hotel.view.fragment.plan_payment

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentCardPremiumBinding
import com.vice_hotel.model.data.Plan
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.plan_payment.BrainTreeUtil
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_card_discovery.*
import kotlinx.android.synthetic.main.fragment_card_premium.*
import kotlinx.android.synthetic.main.fragment_card_premium.btnShowFieldPromoCode
import kotlinx.android.synthetic.main.fragment_card_premium.btnSubmit

private const val KEY = "plan"
class CardPremiumFragment: BaseFragment() {

    companion object {
        fun newInstance(plan: Plan?) = CardPremiumFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY, plan)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_card_premium

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val plan = arguments?.getParcelable(KEY) as Plan?
        val bind : FragmentCardPremiumBinding? = DataBindingUtil.bind(fragmentView!!)
        bind?.plan = plan
        bind?.promotionCode = getString(R.string.promotion_n_code)
        listener(plan)
    }
    private fun listener(plan: Plan?) {
        btnShowFieldPromoCode.setOnClickListener() {}
        btnSubmit.setOnClickListener() {
            when (plan?.isUpgrade) {
                true -> {
                    registerAutoRenewNextMonth(plan)
                }
                else -> {
                    val direction = PlanPickerFragmentDirections.actionPlanPickerFragmentToCheckOutFragment(plan?.id, plan?.type)
                    findNavController().navigate(direction)
                }
            }

        }
    }

    private fun registerAutoRenewNextMonth(plan: Plan) {
        ViewModelProviders.of(this).get(PlanModel::class.java)
            .upgradePlan(context,plan).observe(this, Observer {
                if (it.isSuccess()) {
                    activity?.onBackPressed()
                } else {
                    AppUtil.toast(context, it.message, false)
                }
            })
    }

}