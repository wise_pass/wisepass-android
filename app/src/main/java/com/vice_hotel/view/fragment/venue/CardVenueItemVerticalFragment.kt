package com.vice_hotel.view.fragment.venue

import android.graphics.Color
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentCardVenueItemVerticalBinding
import com.vice_hotel.model.data.VenueItem
import com.vice_hotel.presenter.helper.MemberType
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_card_venue_item_vertical.*

private const val VENUE = "key_venue"
class CardVenueItemVerticalFragment: BaseFragment() {

    companion object {
        fun newInstance(venue: VenueItem?) = CardVenueItemVerticalFragment().apply {
            arguments = Bundle().apply {
                putParcelable(VENUE, venue)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_card_venue_item_vertical

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val venueItem = arguments?.getParcelable(VENUE) as VenueItem
        addVenueToUI(venueItem)
    }

    private fun addVenueToUI(venueItem: VenueItem?) {
        if (venueItem!=null) {
            val bind : FragmentCardVenueItemVerticalBinding? = DataBindingUtil.bind(fragmentView!!)
            bind?.venueItem = venueItem
            setTheme(MemberType.detect(venueItem.userType))
        }
    }

    private fun setTheme(memberType: MemberType) {
        when (memberType) {
            MemberType.PREMIUM -> {
                cardView.setCardBackgroundColor(resources.getColor(R.color.premium))
                txtMemberType.setTextColor(resources.getColor(R.color.premium))
                txtTitle.setTextColor(Color.parseColor("#ffffff"))
                txtTime.setTextColor(Color.parseColor("#80ffffff"))
                txtDate.setTextColor(Color.parseColor("#80ffffff"))
                lineView.setBackgroundColor(Color.parseColor("#30ffffff"))
                txtTime.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_clock_white_16dp, 0, 0 ,0)
                txtDate.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_calendar_white_16dp, 0, 0 ,0)
            }
            else -> {
                cardView.setCardBackgroundColor(resources.getColor(R.color.standard))
                txtMemberType.setTextColor(resources.getColor(R.color.standard))
                txtTitle.setTextColor(Color.parseColor("#000000"))
                txtTime.setTextColor(Color.parseColor("#80000000"))
                txtDate.setTextColor(Color.parseColor("#80000000"))
                lineView.setBackgroundColor(Color.parseColor("#30000000"))
                txtTime.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_clock_black_16dp, 0, 0 ,0)
                txtDate.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_calendar_black_16dp, 0, 0 ,0)
            }
        }
    }

}