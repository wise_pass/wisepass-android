package com.vice_hotel.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.PreferenceUtils
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.presenter.view_model.SignUpModel
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.WebActivity
import kotlinx.android.synthetic.main.fragment_connections.*

class ConnectionsFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_connections

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private lateinit var callbackManager: CallbackManager

    override fun onViewCreated(savedInstanceState: Bundle?) {
        callbackManager = CallbackManager.Factory.create()
        val loginManager = LoginManager.getInstance()
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        if (AccessToken.getCurrentAccessToken()!=null) {
            loadFacebookConnection(AccessToken.getCurrentAccessToken())
            facebook_login.setText(getString(R.string.disconnect))
            facebook_login.setOnClickListener() {

                val requestLogin = AppManager.instance.getRequestLogin().apply {
                    facebookId = AccessToken.getCurrentAccessToken().userId
                    facebookAccessToken = AccessToken.getCurrentAccessToken().token
                }
                DialogUtil.OpenLoadingDialog(activity)
                ViewModelProviders.of(this@ConnectionsFragment).get(SignUpModel::class.java)
                    .disconnectFacebook(context, requestLogin).observe(this@ConnectionsFragment, Observer { logOut ->
                        DialogUtil.dismiss()
                        if (logOut.isSuccess()) {
                            loginManager.logOut()
                            activity?.onBackPressed()
                        }
                    })

            }
        } else {
            facebook_login.visibility = View.VISIBLE
            loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    result?.accessToken?.userId
                    if (result?.accessToken!=null) {
                        continueWithFacebook(result.accessToken)
                    }
                }

                override fun onCancel() {

                }

                override fun onError(error: FacebookException?) {
                    error?.localizedMessage
                }

            })
            facebook_login.setOnClickListener() {
                loginManager.logInWithReadPermissions(this, arrayListOf("public_profile"))
            }
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        LoginManager.getInstance().unregisterCallback(callbackManager)
    }

    private fun loadFacebookConnection(accessToken: AccessToken) {
        GraphRequest.newMeRequest(accessToken) { _, response ->
            lnFacebookAuthor.visibility = View.VISIBLE
            facebook_login.setText(getString(R.string.disconnect))
            val id = response.jsonObject.getString("id")
            val name = response.jsonObject.getString("name")
            txtFacebook.text = " ${name}"
            GlideUtil.loadCircle(context, "https://graph.facebook.com/$id/picture?type=large", imgPhoto)
        }.executeAsync()
    }

    private fun continueWithFacebook(accessToken: AccessToken) {
        val requestLogin = AppManager.instance.getRequestLogin().apply {
            facebookId = accessToken.userId
            facebookAccessToken = accessToken.token
        }
        AppManager.instance.setRequestLogin(requestLogin)
        ViewModelProviders.of(this)
            .get(SignUpModel::class.java).reconnectWithFacebook(context, requestLogin)
            .observe(this, androidx.lifecycle.Observer {
                DialogUtil.dismiss()
                if (it.isSuccess()) {
//                    if (it.data?.account!=null) {
//                        val account = it.data.account
//                        AppManager.instance.loginSuccess(context, account) {
//                            loadFacebookConnection(accessToken)
//                        }
//                    } else {
//                        getEmailFromFacebook(accessToken)
//                    }
                    loadFacebookConnection(accessToken)
                } else {
                    DialogUtil.OpenCustomDialogError(activity, getString(R.string.error), it.message, R.string.close, null) {
                        LoginManager.getInstance().logOut()
                        DialogUtil.dismiss()
                    }
                }
            })
    }

    private fun getEmailFromFacebook(accessToken: AccessToken) {
        val request = GraphRequest.newMeRequest(accessToken) { _, response ->
            val emailFacebook = response.jsonObject.getString("email")
            if (!emailFacebook.isNullOrEmpty()) {
                PreferenceUtils(context).put(PreferenceUtils.EMAIL_REGISTRATION, emailFacebook)
            }
            findNavController().navigate(R.id.action_connectionsFragment_to_nav_sign_up)
        }
        val permissions = Bundle().apply {
            putString("fields","id,name,email")
        }
        request.parameters = permissions
        request.executeAsync()
    }



}