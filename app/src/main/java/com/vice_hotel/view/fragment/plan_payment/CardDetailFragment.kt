package com.vice_hotel.view.fragment.plan_payment

import android.os.Bundle
import android.os.Handler
import androidx.core.widget.doAfterTextChanged
import com.vice_hotel.R
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_card_detail.*
import android.text.Editable
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.braintreepayments.api.Card
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.models.CardBuilder
import com.braintreepayments.api.models.CardNonce
import com.braintreepayments.cardform.view.CardForm
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.lib.braintree.CardType
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.google.zxing.integration.android.IntentIntegrator.REQUEST_CODE
import android.content.Intent.getIntent
import com.braintreepayments.api.dropin.DropInRequest


private const val BraintreeRequestCode = 233
class CardDetailFragment: BaseFragment() {

    private val CARD_DATE_TOTAL_SYMBOLS = 5 // size of pattern MM/YY
    private val CARD_DATE_TOTAL_DIGITS = 4 // max numbers of digits in pattern: MM + YY
    private val CARD_DATE_DIVIDER_MODULO = 3 // means divider position is every 3rd symbol beginning with 1
    private val CARD_DATE_DIVIDER_POSITION = CARD_DATE_DIVIDER_MODULO - 1 // means divider position is every 2nd symbol beginning with 0
    private val CARD_DATE_DIVIDER = '/'

    //
    private val CARD_NUMBER_TOTAL_SYMBOLS = 19 // size of pattern 0000-0000-0000-0000
    private val CARD_NUMBER_TOTAL_DIGITS = 16 // max numbers of digits in pattern: 0000 x 4
    private val CARD_NUMBER_DIVIDER_MODULO = 5 // means divider position is every 5th symbol beginning with 1
    private val CARD_NUMBER_DIVIDER_POSITION =
        CARD_NUMBER_DIVIDER_MODULO - 1 // means divider position is every 4th symbol beginning with 0
    private val CARD_NUMBER_DIVIDER = '-'


    override fun getLayoutRes(): Int = R.layout.fragment_card_detail

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() {
            activity?.onBackPressed()
        }
        btnSubmit.setOnClickListener() { onCheckOut() }
        edCardExpiredDate.doAfterTextChanged { s ->
            if (!isInputCorrect(s!!, CARD_DATE_TOTAL_SYMBOLS, CARD_DATE_DIVIDER_MODULO, CARD_DATE_DIVIDER)) {
                s.replace(0, s.length, concatString(getDigitArray(s, CARD_DATE_TOTAL_DIGITS), CARD_DATE_DIVIDER_POSITION, CARD_DATE_DIVIDER));
            }
            cardExpireDate.text = s.toString()
        }
        edCardNumber.setOnCompleteDetectCardType {
            when (it) {
                CardType.VISA -> {
                    creditCard.visibility = View.VISIBLE

                    imgCard.setImageResource(R.drawable.bg_creditcard_visa)
                    imgTypeCard.setImageResource(R.drawable.ic_logo_card_visa)
                    iconWisePass.setImageResource(R.drawable.ic_pass_purple_24dp)
                }
                CardType.MASTERCARD -> {
                    creditCard.visibility = View.VISIBLE

                    imgCard.setImageResource(R.drawable.bg_creditcard_master)
                    imgTypeCard.setImageResource(R.drawable.ic_logo_master_card)
                    iconWisePass.setImageResource(R.drawable.ic_pass_white_24dp)

                }
                CardType.UNKNOWN -> {
                    imgCard.setImageResource(R.drawable.bg_master_card)
                    imgTypeCard.setImageResource(0)
                    iconWisePass.setImageResource(0)
                }
            }
            Handler().post { edCardNumber.requestFocus() }
        }

        edCardNumber.doAfterTextChanged { s ->
            if (!isInputCorrect(s!!, CARD_NUMBER_TOTAL_SYMBOLS, CARD_NUMBER_DIVIDER_MODULO, CARD_NUMBER_DIVIDER)) {
                s.replace(0, s.length, concatString(getDigitArray(s, CARD_NUMBER_TOTAL_DIGITS), CARD_NUMBER_DIVIDER_POSITION, CARD_NUMBER_DIVIDER));
            }
            val stringArr = s.toString().split("-")
            for (index in 0 until stringArr.size) {
                when (index) {
                    0 -> cardFirstFourNumber.text = fillCardNumberOnCard(stringArr[index])
                    1 -> cardSecondFourNumber.text = fillCardNumberOnCard(stringArr[index])
                    2 -> cardThirdFourNumber.text = fillCardNumberOnCard(stringArr[index])
                    3 -> cardLastFourNumber.text = fillCardNumberOnCard(stringArr[index])
                }
            }
        }
        edCardHolderName.doAfterTextChanged {
            cardHolderName.text = it.toString()
        }
        edCardCvv.doAfterTextChanged {
            cardCvv.text = it.toString()
        }
    }

    private fun fillCardNumberOnCard(string: String): String = when (string.length) {
            1 -> "${string}000"
            2 -> "${string}00"
            3 -> "${string}0"
            4 -> string
            else -> "0000"

    }

    private fun onCheckOut() {
        btnSubmit.isEnabled = false
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).requestBrainTreeToken(context).observe(this, Observer {
            DialogUtil.dismiss()
            btnSubmit.isEnabled = true
            if (it.isSuccess()) {
                val token = it.data?.token
                val dropInRequest = DropInRequest()
                    .clientToken(token)
                startActivityForResult(dropInRequest.getIntent(context), BraintreeRequestCode)
            }
        })
    }

    private fun isInputCorrect(s: Editable, size: Int, dividerPosition: Int, divider: Char): Boolean {
        var isCorrect = s.length <= size
        for (i in 0 until s.length) {
            if (i > 0 && (i + 1) % dividerPosition == 0) {
                isCorrect = isCorrect and (divider == s[i])
            } else {
                isCorrect = isCorrect and Character.isDigit(s[i])
            }
        }
        return isCorrect
    }

    private fun concatString(digits: CharArray, dividerPosition: Int, divider: Char): String {
        val formatted = StringBuilder()

        for (i in digits.indices) {
            if (digits[i].toInt() != 0) {
                formatted.append(digits[i])
                if (i > 0 && i < digits.size - 1 && (i + 1) % dividerPosition == 0) {
                    formatted.append(divider)
                }
            }
        }

        return formatted.toString()
    }

    private fun getDigitArray(s: Editable, size: Int): CharArray {
        val digits = CharArray(size)
        var index = 0
        var i = 0
        while (i < s.length && index < size) {
            val current = s[i]
            if (Character.isDigit(current)) {
                digits[index] = current
                index++
            }
            i++
        }
        return digits
    }



}