package com.vice_hotel.view.fragment.menu

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_about_wise_pass.*

class AboutWisePassFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_about_wise_pass

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        lnSetting.setOnClickListener(listener)
        lnTerm.setOnClickListener(listener)
        lnTutorials.setOnClickListener(listener)
        lnFAQs.setOnClickListener(listener)
    }

    private val listener = View.OnClickListener {
        when (it.id) {
            lnSetting.id -> {
                findNavController().navigate(R.id.action_aboutWisePassFragment_to_settingFragment)
            }
            lnTerm.id -> {
                findNavController().navigate(R.id.action_aboutWisePassFragment_to_termsConditionsFragment)
            }
            lnTutorials.id -> {
                val direction = AboutWisePassFragmentDirections.actionAboutWisePassFragmentToTermsConditionsFragment(Constant.WEB_HowWisePassWorkApp)
                findNavController().navigate(direction)
            }
            lnFAQs.id -> {
                val direction = AboutWisePassFragmentDirections.actionAboutWisePassFragmentToTermsConditionsFragment(Constant.WEB_FAQApp)
                findNavController().navigate(direction)
            }
        }
    }
}