package com.vice_hotel.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import com.vice_hotel.view.BaseFragment
import android.view.WindowManager
import android.os.Build
import android.os.Handler
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.format.Formatter
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.vice_hotel.BuildConfig
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentSplashBinding
import com.vice_hotel.model.data.AppConfig
import com.vice_hotel.model.data.WifiCountry
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.activity.MainActivity
import com.vice_hotel.view.activity.ProfilePictureActivity
import java.security.Permission
import java.util.*
import kotlin.math.ln

private const val REQUEST_LOCATION = 111
private const val GOOGLE_PLAY_SERVICES_ID = "com.google.android.gms&hl=en"
private const val TAG = "SplashFragment"

class SplashFragment : BaseFragment() {

    private var wifiCountry: WifiCountry? = null

    override fun getLayoutRes(): Int = R.layout.fragment_splash

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val notFirstInApp = PreferenceUtils(activity as BaseActivity).get(
            PreferenceUtils.NOT_FIRST_IN_APP,
            Boolean.javaClass
        ) as Boolean
        if (!notFirstInApp) {
            PreferenceUtils(activity).put(PreferenceUtils.NOT_FIRST_IN_APP, true)
            PreferenceUtils(activity).put(PreferenceUtils.PUSH_NOTIFICATION, true)
        }
        AppManager.instance.getLocationByWifi(context) {
            if (it == null || it.countryCode.isNullOrEmpty()) {
                val telephonyManager = activity?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                val countryIso = telephonyManager.simCountryIso.toUpperCase()
                wifiCountry = WifiCountry(countryCode = countryIso)
            } else {
                wifiCountry = WifiCountry()
                wifiCountry = it.copy()
            }
            prepareApp()
        }
    }

    private fun prepareApp() {
        val status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
        if (status == ConnectionResult.SUCCESS) {
            getLocationByGPS()
        } else {
            AppUtil.openGooglePlayWithAppID(activity, GOOGLE_PLAY_SERVICES_ID)
            activity?.finish()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION) {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                (activity as MainActivity).requestLocation() { lat, lng ->
                    if (lat != null && lng != null && wifiCountry != null) {
                        wifiCountry!!.lat = lat
                        wifiCountry!!.lon = lng
                    }
                    postConfig()
                }
            } else {
                postConfig()
            }
        }

    }

    private fun getLocationByGPS() {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            (activity as MainActivity).requestLocation() { lat, lng ->
                if (lat != null && lng != null && wifiCountry != null) {
                    wifiCountry!!.lat = lat
                    wifiCountry!!.lon = lng
                }
                postConfig()
            }
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION)
        }
    }

    private fun postConfig() {
        val deviceId = Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID) as String?
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            val token = it.result?.token
            val appConfig = AppConfig(
                push_token = token,
                device_id = deviceId,
                app_version = BuildConfig.VERSION_NAME,
                app_version_code = BuildConfig.VERSION_CODE,
                os_name = "${Build.MODEL}-${Build.VERSION.SDK_INT}",
                latitude = wifiCountry?.lat,
                longitude = wifiCountry?.lon,
                country_code = wifiCountry?.countryCode
            )
            AppManager.instance.setAppConfig(appConfig)
            val requestCheckVersion = ApiService.create(context)?.setConfigAndCheckVersion(appConfig)
            RetrofitUtil.requestAPI(context, requestCheckVersion) {
                if (it.isSuccess()) {
                    when (it.data?.update_code) {
                        0 -> onAppReady()
                        1 -> DialogUtil.OpenCustomDialog(
                            activity,
                            getString(R.string.please_update),
                            null,
                            R.string.update,
                            null
                        ) {
                            AppUtil.openGooglePlayWithAppID(activity, activity?.packageName)
                        }
                    }
                } else {
                    DialogUtil.OpenCustomDialogError(activity, null, it.message, R.string._continue, null) {
                        DialogUtil.dismiss()
                        onAppReady()
                    }
                }
            }
        }
    }

    private fun onAppReady() {
        val token = PreferenceUtils(context).get(PreferenceUtils.ACCESS_KEY_API, String.javaClass) as String?
        if (token.isNullOrEmpty()) {
            findNavController().navigate(R.id.action_splashFragment_to_selectLanguageFragment)
        } else {
            findNavController().navigate(R.id.action_splashFragment_to_dashboardFragment)
        }
    }

}