package com.vice_hotel.view.fragment.dashboard

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vice_hotel.R
import com.vice_hotel.model.data.Brand
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.FilterBrandsAdapter
import kotlinx.android.synthetic.main.fragment_filter.*

class FilterFragment: BaseFragment() {

    private lateinit var adapter: FilterBrandsAdapter

    override fun getLayoutRes(): Int = R.layout.fragment_filter

    override fun onViewCreated(savedInstanceState: Bundle?) {
        adapter = FilterBrandsAdapter()
        adapter.onItemSelectListener {
            val itemsActive = adapter.getAllBrandsActive()
            AppManager.instance.setSearchBrands(itemsActive)
        }
        actionBack.setOnClickListener() {
            activity?.onBackPressed()
        }
        setSpinnerStatus()
        loadVenues()
    }

    private fun loadVenues() {
        val viewModel = ViewModelProviders.of(this).get(VenueModel::class.java).getHomeVenues(context)
        viewModel.observeOnce(this, Observer {
            when(it.data?.brands!=null) {
                true -> {
                    initBrands(it.data?.brands)
                }
                else -> {
                    AppUtil.toast(context, it.message, false)
                }
            }
        })
    }

    private fun initBrands(brands: ArrayList<Brand>?) {
        RecyclerUtil.setUpHorizontal(context, rclCategories, adapter)
        val brandsActive = AppManager.instance.getSearchData().brands
        for (brand in brands!!) {
            brand.checked = false
            if (brandsActive!=null) {
                for (brandActive in brandsActive) {
                    if (brand.keyword!=null && brandActive.keyword!=null) {
                        if (brand.keyword!!.trim().toLowerCase().contentEquals(brandActive.keyword!!.trim().toLowerCase())) {
                            brand.checked = true
                        }
                    }
                }
            }
        }
        adapter.addAll(brands)
    }

    private fun setSpinnerStatus() {
        val array = resources.getStringArray(R.array.venues_status)
        val spinerAdapter = ArrayAdapter<String>(context, R.layout.view_spinner, array)
        spinner.adapter = spinerAdapter
        if (AppManager.instance.getSearchData().venue_status!=null) {
            val status = AppManager.instance.getSearchData().venue_status
            when (status) {
                "open" -> spinner.setSelection(1)
                "close" -> spinner.setSelection(2)
            }

        }
        spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                AppManager.instance.setSearchVenueByStatus(when (position) {
                    0 -> "all"
                    1 -> "open"
                    else -> "close"
                })
            }

        })
    }
}