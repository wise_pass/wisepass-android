package com.vice_hotel.view.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.vice_hotel.R
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_map_of_venue.*

class MapOfVenueFragment: BaseFragment(), OnMapReadyCallback {

    private var map: GoogleMap? = null
    private val args: MapOfVenueFragmentArgs by navArgs()

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        try {
            map?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_json))
        } catch (e: Exception) {}
        map?.setMinZoomPreference(12f)
        map?.setMaxZoomPreference(18f)
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED){
            map?.isMyLocationEnabled = true
        }
        setUpVenueOnMap(args.venue)
    }

    override fun getLayoutRes(): Int = R.layout.fragment_map_of_venue

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        val childMap = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        childMap.getMapAsync(this)
    }

    private fun setUpVenueOnMap(venue: Venue) {
        val currentLocation = LatLng(venue.latitude!!, venue.longitude!!)
        map?.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
        //
        GlideUtil.loadCircleBitmap(context, venue.logo) {
            val marker = MarkerOptions().position(LatLng(venue.latitude!!, venue.longitude!!)).title(venue.name).icon(
                BitmapDescriptorFactory.fromBitmap(it)
            )
            val mar = map?.addMarker(marker)
            mar?.tag = venue.id
        }
    }
}