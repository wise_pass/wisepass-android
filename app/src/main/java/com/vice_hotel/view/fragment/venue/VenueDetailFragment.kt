package com.vice_hotel.view.fragment.venue

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentVenueDetailBinding
import com.vice_hotel.model.data.Category
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.widget.AppTextView
import kotlinx.android.synthetic.main.fragment_venue_detail.*

private const val REQUEST_CALLING = 3213
class VenueDetailFragment: BaseFragment(), OnMapReadyCallback {

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        try {
            map?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_json))
        } catch (e: Exception) {}
        map?.setMinZoomPreference(16f)
        map?.setMaxZoomPreference(18f)
        loadVenueById(args.venueId)
    }

    private val args: VenueDetailFragmentArgs by navArgs()
    private var binding: FragmentVenueDetailBinding? = null
    private var map: GoogleMap? = null

    override fun getLayoutRes(): Int = R.layout.fragment_venue_detail

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val childMap = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        childMap.getMapAsync(this)
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        actionShare.setOnClickListener() {
            AppUtil.shareUrl(activity!!, "https://mobile.wisepass.co/venue?venueid=${args.venueId}")
        }
        btnReadMore.setOnClickListener() {
            val direction = VenueDetailFragmentDirections.actionVenueDetailFragmentToVenueReadMoreFragment(args.venueId)
            findNavController().navigate(direction)
        }
    }

    private fun loadVenueById(id: String?) {
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenueDetailById(context, id).observeOnce(this, Observer {
            if (it.isSuccess()) {
                val venue = it.data?.venue
                //
                val currentLocation = LatLng(venue?.latitude!!, venue.longitude!!)
                map?.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
                map?.setOnMapClickListener {
                    val directions =  VenueDetailFragmentDirections.actionVenueDetailFragmentToMapOfVenueFragment(venue)
                    findNavController().navigate(directions)
                }
                addMarker(venue)
                //
                binding = DataBindingUtil.bind(fragmentView!!)
                binding?.venue = venue
                loadCategories(venue.categories)
                if (binding?.venue?.phoneNo.isNullOrEmpty()) {
                    btnCallVenue.visibility = View.GONE
                }
                if (binding?.venue?.latitude==null || binding?.venue?.longitude==null) {
                    btnLocation.visibility = View.GONE
                }
                btnCallVenue.setOnClickListener() { callVenue() }
                btnLocation.setOnClickListener() { openGoogleMap() }
            }
        })
    }

    private fun addMarker(venue: Venue) {
        GlideUtil.loadCircleBitmap(context, venue.logo) {
            val marker = MarkerOptions().position(LatLng(venue.latitude!!, venue.longitude!!)).icon(
                BitmapDescriptorFactory.fromBitmap(it)
            )
            map?.addMarker(marker)
        }
    }

    @SuppressLint("ResourceType")
    private fun loadCategories(categories: ArrayList<Category>?) {
        if (categories!=null) {
            val size = categories.size
            if (size > 0) {
                radioGroupMenu.weightSum = size.toFloat()
                for (index in 0 until size) {

                    val item = View.inflate(context, R.layout.item_qr_code_detected_menu, null) as RadioButton

                    val param = RadioGroup.LayoutParams(
                        0,
                        RadioGroup.LayoutParams.WRAP_CONTENT,
                        1.0f
                    )
                    item.layoutParams = param
                    item.id = index+1
                    item.text = categories[index].displayName
                    radioGroupMenu.addView(item)
                    //
                    val tagView = LayoutInflater.from(context).inflate(R.layout.view_category_tag, null, false) as View
                    tagView.findViewById<AppTextView>(R.id.txtView).text = categories[index].displayName
                    containerTagCategories.addView(tagView)
                }
                radioGroupMenu.requestLayout()
                radioGroupMenu.setOnCheckedChangeListener { group, checkedId ->
                    addFragmentVenueItems(categories[checkedId-1])
                }
                radioGroupMenu.check(1)
            }
        }
    }

    private fun addFragmentVenueItems(category: Category?) {
        fragmentManager?.beginTransaction()?.replace(R.id.containerVenueItems, VenueItemsPagerFragment.newInstance(category), category?.keyword)?.commit()
    }

    private fun callVenue() {
        if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            val phoneNo = binding?.venue?.phoneNo
            var phone = phoneNo
            if (phoneNo!=null) {
                if (phoneNo.contains("/")) {
                    phone = phoneNo.split("/")[0].trim()
                }
            }
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${phone}"))
            activity?.startActivity(intent)
        } else {
            requestPermissions(arrayOf(android.Manifest.permission.CALL_PHONE), REQUEST_CALLING)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode==REQUEST_CALLING) {
            if ( grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${binding?.venue?.phoneNo}"))
                activity?.startActivity(intent)
            }
        }
    }

    private fun openGoogleMap() {
        val gmmIntentUri = Uri.parse("google.navigation:q=${binding?.venue?.latitude},${binding?.venue?.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        activity?.startActivity(mapIntent)
    }

}