package com.vice_hotel.view.fragment.venue

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.SearchData
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.VenueListAdapter
import kotlinx.android.synthetic.main.fragment_list_venue.*

class VenueListFragment: BaseFragment() {

    var adapter: VenueListAdapter? = null

    override fun getLayoutRes(): Int = R.layout.fragment_list_venue

    override fun onFragmentCreateFirstTime() {
        super.onFragmentCreateFirstTime()
        adapter = VenueListAdapter()
        adapter?.setOnSelectedVenue {
            val direction = VenueListFragmentDirections.actionVenueListFragmentToVenueDetailFragment(it.id)
            findNavController().navigate(direction)
        }
        loadVenues()
    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        if (adapter?.itemCount!=0) {
            progressBar.visibility = View.GONE
        }
        RecyclerUtil.setUp(context, rcl, adapter)
        edSearchField.doAfterTextChanged {
            adapter?.searchByKeyword(it.toString().trim())
        }
    }

    private fun loadVenues() {
        ViewModelProviders.of(this).get(VenueModel::class.java).searchVenues(context, SearchData()).observe(this, Observer {
            progressBar.visibility = View.GONE
            Handler().postDelayed({fragmentReady(it)}, 200)
        })
    }

    private fun fragmentReady(it: ResponseBody) {
        if (it.isSuccess()) {
            adapter?.keepDataTemp(it.data?.venues)
            adapter?.addAll(it.data?.venues)
        }
    }



}