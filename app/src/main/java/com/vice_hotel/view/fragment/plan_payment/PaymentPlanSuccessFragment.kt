package com.vice_hotel.view.fragment.plan_payment

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentPaymentPlanSuccessBinding
import com.vice_hotel.presenter.plan_payment.PlanType
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_payment_plan_success.*

class PaymentPlanSuccessFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_payment_plan_success

    override fun onViewCreated(savedInstanceState: Bundle?) {


        btnExplore.setOnClickListener() {
            MainActivity.startActivity(activity as BaseActivity)
        }

    }
}