package com.vice_hotel.view.fragment.scan

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.google.zxing.client.android.BeepManager
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoderFactory
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.vice_hotel.R
import com.vice_hotel.model.data.LetPass
import com.vice_hotel.model.data.ScanType
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.view_model.AccountModel
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.presenter.scan.ScanQrCodeModel
import com.vice_hotel.presenter.view_model.PromotionModel
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_scan_qr_code.*
import java.util.*

class ScanQrCodeFragment: BaseFragment() {

    private var isReward: Boolean = false

    private val args: ScanQrCodeFragmentArgs by navArgs()

    private var beepManager: BeepManager? = null

    override fun getLayoutRes(): Int = R.layout.fragment_scan_qr_code

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        when(args.scanType?.type) {
            ScanType.REWARD -> {
                isReward = true
                setUpScanBarcode()
                listener()
            }
            else -> {
                DialogUtil.OpenLoadingDialog(activity)
                ViewModelProviders.of(this).get(CheckoutModel::class.java)
                    .checkPackageByBraintree(context).observeOnce(this@ScanQrCodeFragment, androidx.lifecycle.Observer {
                        checkAvaiableCoins()
                    })

            }
        }

    }

    private fun listener() {

        cbFlash.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (context?.packageManager?.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)!!) {
                    barcodeView.setTorchOn()
                }
            } else {
                if (context?.packageManager?.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)!!) {
                    barcodeView.setTorchOff()
                }
            }
        }
    }

    private fun checkAvaiableCoins() {
        ViewModelProviders.of(this).get(AccountModel::class.java).checkAvaiableCoins(context).observeOnce(this, androidx.lifecycle.Observer {
            DialogUtil.dismiss()
            when (it.data?.available_coins?.coins) {
                null, 0 ->{
                    AppUtil.checkPlanExist(this) { isPlanExist ->
                        DialogUtil.dismiss()
                        if (!isPlanExist) {
                            DialogUtil.OpenCustomDialog(activity, null, getString(R.string.subscribe_or_skip_promotion), R.string.next, R.string.skip) { popup ->
                                DialogUtil.dismiss()
                                if (popup) {
                                    findNavController().navigate(R.id.action_scanQrCodeFragment_to_plan_payment)
                                } else {
                                    setUpScanBarcode()
                                    listener()
                                }
                            }
                        } else {
                            setUpScanBarcode()
                            listener()
                        }
                    }
                }
                else ->{
                    status_user.text = getString(R.string.you_have_avaiable_pass, it.data.available_coins.coins)
                    setUpScanBarcode()
                    listener()
                }
            }
        })
    }



    private fun setUpScanBarcode() {
        val formats = Arrays.asList(BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39)
        barcodeView.barcodeView.decoderFactory = DefaultDecoderFactory(formats) as DecoderFactory?
        barcodeView.initializeFromIntent(activity?.intent)
        barcodeView.decodeContinuous(callback)
        barcodeView.statusView.text = ""
        beepManager = BeepManager(activity)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                barcodeView.resume()
            } else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    private val callback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult) {
            if (result.text == null) {
                // Prevent duplicate scans
                return
            }
            barcodeView.pause()
            //beepManager?.playBeepSoundAndVibrate()

            checkBarCode(result.text) {
                var error = getString(R.string.qr_code_not_found)
                if (!it.isNullOrEmpty()) {
                    error = it
                }
                DialogUtil.OpenCustomDialogError(activity, getString(R.string.error), error, R.string.scan_again, R.string.cancel) {
                    DialogUtil.dismiss()
                    if (it) {
                        barcodeView.resume()
                    } else {
                        actionBack.performClick()
                    }
                }
            }
        }

        override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    }

    private fun checkBarCode(barcode: String, callBackError :(String?) -> Unit) {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(ScanQrCodeModel::class.java).scanQrCode(context, barcode)
            .observeOnce(this, androidx.lifecycle.Observer {
                DialogUtil.dismiss()
                if (it.isSuccess() && it.data!=null) {
                    val scanType = it.data.validate_barcode
                    when (scanType?.type) {
                        ScanType.PROMO -> leftPassFromPromotionCode(barcode)
                        ScanType.PLAN -> findNavController().navigate(R.id.action_scanQrCodeFragment_to_plan_payment)
                        ScanType.USER -> DialogUtil.OpenCustomDialog(activity, getString(R.string.thank_you), it.message, R.string.next, R.string.close) { popup ->
                            DialogUtil.dismiss()
                            if (popup) {
                               findNavController().navigate(R.id.action_scanQrCodeFragment_to_plan_payment)
                            } else {
                                activity?.onBackPressed()
                            }
                        }
                        else -> scanTypeVenues(scanType)
                    }

                } else {
                    callBackError(it.message)
                }
        })
    }

    private fun scanTypeVenues(scanType: ScanType?) {
        val id = scanType?.id?.trim()
        if (isReward) {
            scanType?.type = ScanType.REWARD
            scanType?.venueId = id
            scanType?.promoId = args.scanType?.promoId
        } else {
            scanType?.venueId = id
        }
        val directions = ScanQrCodeFragmentDirections.actionScanQrCodeFragmentToQrCodeDetectedFragment(scanType)
        findNavController().navigate(directions)
    }

    private fun leftPassFromPromotionCode(code: String) {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(PromotionModel::class.java)
            .letPass(context, code).observe(this, androidx.lifecycle.Observer { letpass ->
                DialogUtil.dismiss()
                if (letpass.isSuccess()) {
                    if (letpass.data?.letPass!=null) {
                        val type = letpass.data.letPass.type
                        when (type) {
                            LetPass.PLAN -> findNavController().navigate(R.id.action_scanQrCodeFragment_to_planBillingFragment)
                            else -> findNavController().navigate(R.id.action_scanQrCodeFragment_to_challengesRewardsFragment)
                        }
                    }
                } else {
                    showError(letpass.message)
                }
            })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                barcodeView.resume()
            } else {
                activity?.onBackPressed()
            }
        }
    }

    private fun showError(msg: String?) {
        DialogUtil.OpenCustomDialogError(activity, null, msg, R.string.cancel, null) {
            DialogUtil.dismiss()
            activity?.onBackPressed()
        }
    }
}