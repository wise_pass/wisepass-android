package com.vice_hotel.view.fragment

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_terms_condittions.*

class TermsConditionsFragment: BaseFragment() {

    private val args: TermsConditionsFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_terms_condittions

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        webView.settings.javaScriptEnabled = true
        webView.settings.defaultTextEncodingName = "utf-8"
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.webViewClient = CustomWebViewClient()
        webView.webChromeClient = WebChromeClient()
        title.text = when(args.pUrl) {
            Constant.WEB_HowWisePassWorkApp -> getString(R.string.tutorials)
            Constant.WEB_FAQApp -> getString(R.string.faqs)
            else -> getString(R.string.terms_conditions)
        }
        webView.loadUrl("http://wisepass.co/home/${args.pUrl}")
    }

    inner class CustomWebViewClient: WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
        }
    }
}