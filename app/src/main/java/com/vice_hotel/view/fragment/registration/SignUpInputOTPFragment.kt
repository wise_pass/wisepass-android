package com.vice_hotel.view.fragment.registration

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentSignUpInputOtpBinding
import com.vice_hotel.model.data.RequestLogin
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.view_model.SignUpModel
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up_input_otp.*
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AFInAppEventType
import com.appsflyer.AppsFlyerLib
import com.appsflyer.AppsFlyerTrackingRequestListener
import com.facebook.AccessToken
import com.vice_hotel.BuildConfig
import com.vice_hotel.model.data.AppConfig
import com.vice_hotel.model.data.WifiCountry
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil


private const val TAG = "SignUpInputOTPFragment"
class SignUpInputOTPFragment: BaseFragment() {

    private val args: SignUpInputOTPFragmentArgs by navArgs()

    var contextLocale: Context? = null

    override fun getLayoutRes(): Int = R.layout.fragment_sign_up_input_otp

    override fun onViewCreated(savedInstanceState: Bundle?) {
        inputEmail.setText(args.email, TextView.BufferType.NORMAL)
        loadContentOnUI()
        listener()
    }

    private fun loadContentOnUI() {
        val binding: FragmentSignUpInputOtpBinding? = DataBindingUtil.bind(fragmentView!!)
        val lang = LocaleManager.getLanguage(context)
        LocaleManager.changeLocateContext(context, lang) {
            contextLocale = it
            binding?.pleaseTypeOtp = it?.getString(R.string.please_type_otp)
            binding?.didNotGetEmail = it?.resources?.getString(R.string.did_not_get_email)
            binding?.resend = it?.resources?.getString(R.string.resend)
        }
    }

    private fun listener() {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        inputEmail.doAfterTextChanged {
            val s = it.toString()
            if (!AppUtil.isNotContainSpecialCharacter(s)) {
                if (!AppUtil.isEmailValid(s)) {
                    labelStatusForValidEmail(true, contextLocale?.resources?.getString(R.string.wrong_email_format))
                    return@doAfterTextChanged
                }
            }
            labelStatusForValidEmail(false, contextLocale?.resources?.getString(R.string.tell_us_your_email))
        }
        otpView.setOtpCompletionListener { otpString ->
            DialogUtil.OpenLoadingDialog(activity)
            val requestLogin = AppManager.instance.getRequestLogin().apply {
                email = inputEmail.text.toString().trim()
                otp = otpString
                udid = Settings.Secure.getString(activity?.contentResolver, Settings.Secure.ANDROID_ID)
            }
            ViewModelProviders.of(this).get(SignUpModel::class.java).signUpOtp(context, requestLogin).observe(this, Observer { it ->
                DialogUtil.dismiss()
                if (it.isSuccess()) {
                    if (it.data?.account!=null) {
                        val account = it.data.account
                        AppManager.instance.loginSuccess(context, account) {
                            PreferenceUtils(activity).put(PreferenceUtils.EMAIL_REGISTRATION, inputEmail.text.toString().trim())
                            directApp(account.status)
                        }
                    }
                } else {
                    otpView.setText("")
                    otpError.text = it.message
                }
            })
        }
        btnResendEmail.setOnClickListener() {
            DialogUtil.OpenLoadingDialog(activity)
            val requestLogin = RequestLogin(email = inputEmail.text.toString().trim())
            ViewModelProviders.of(this).get(SignUpModel::class.java).validateEmail(context, requestLogin).observe(this, Observer {
                DialogUtil.dismiss()
                if (it.statusCode!= HTTP_OK) {
                    AppUtil.toast(context, it.message, false)
                }
            })
        }
    }

    private fun labelStatusForValidEmail(isError: Boolean, text: String?) {
        if (isError) {
            txtErrorWrongEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_circle_error_16dp, 0, 0, 0)
            txtErrorWrongEmail.setTextColor(Color.parseColor("#e64c54"))
        } else {
            txtErrorWrongEmail.setCompoundDrawables(null, null, null, null)
            txtErrorWrongEmail.setTextColor(Color.parseColor("#80FFFFFF"))
        }
        txtErrorWrongEmail.text = text
    }

    private fun getLocationByWifi(complete: (WifiCountry?) -> Unit) {
        val requestCountry = ApiService.createWithoutHost(context,"http://ip-api.com/json/")?.getCountryByIpInternet()
        RetrofitUtil.requestAPIForWifiCountry(context, requestCountry) {
            complete(it)
        }
    }

    private fun directApp(status: String?) {
        when (status) {
            Constant.USER_STATUS_NEW -> {
                if (BuildConfig.APPSFLYER_API_KEY.isNotEmpty()) {
                    val eventValue = HashMap<String, Any>()
                    if (AccessToken.isCurrentAccessTokenActive()) {
                        eventValue[AFInAppEventParameterName.REGSITRATION_METHOD] = "Facebook"
                    } else {
                        eventValue[AFInAppEventParameterName.REGSITRATION_METHOD] = "OTP"
                    }
                    AppsFlyerLib.getInstance().trackEvent(activity!!.application, AFInAppEventType.COMPLETE_REGISTRATION, eventValue)
                }
                findNavController().navigate(R.id.action_global_signUpCongratulationsFragment)
            }
            Constant.USER_STATUS_HAS_PLAN -> {
                findNavController().navigate(R.id.action_sign_up_dashboardFragment)
            }
            Constant.USER_STATUS_NO_PLAN -> {
                PreferenceUtils(context).put(PreferenceUtils.DASHBOARD_ACTION, Constant.ACTION_DASHBOARD_TO_PLAN_PICKER)
                findNavController().navigate(R.id.action_sign_up_dashboardFragment)
            }
            else -> {
                PreferenceUtils(context).put(PreferenceUtils.DASHBOARD_ACTION, Constant.ACTION_DASHBOARD_TO_PLAN_PICKER)
                findNavController().navigate(R.id.action_sign_up_dashboardFragment)
            }
        }
    }

}