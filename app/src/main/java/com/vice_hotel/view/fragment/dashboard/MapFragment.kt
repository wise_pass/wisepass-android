package com.vice_hotel.view.fragment.dashboard

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.vice_hotel.R
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_map.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.glide.GlideApp
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.adapter.VenueSuggestionAdapter
import com.vice_hotel.view.adapter.VenuesMapAdapter

class MapFragment: BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    private var map: GoogleMap? = null
    private var venues: ArrayList<Venue>? = null

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        try {
            map?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_json))
        } catch (e: Exception) {}
        map?.setMinZoomPreference(12f)
        map?.setMaxZoomPreference(18f)
        map?.setOnMarkerClickListener(this)
        val appConfig = AppManager.instance.getAppConfig()
        val currentLocation = LatLng(appConfig?.latitude!!, appConfig.longitude!!)
        map?.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED){
            map?.isMyLocationEnabled = true
        }

        loadVenues()
    }

    override fun getLayoutRes(): Int = R.layout.fragment_map

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val childMap = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        childMap.getMapAsync(this)
    }

    private fun loadVenues() {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenuesByKeyType(context, "nearby").observeOnce(this, Observer {
            DialogUtil.dismiss()
            when (it.data?.listItems) {
                null -> DialogUtil.OpenCustomDialogError(activity, null, it.message, R.string.try_again, null) {
                    DialogUtil.dismiss()
                    loadVenues()
                }
                else -> {
                    venues = it.data.listItems
                    fragmentReady()
                }
            }
        })
    }

    private fun fragmentReady() {
        if (venues!=null) {
            initVenues(venues)
            for (venue in venues!!) {
                if (venue.latitude!=null && venue.longitude!=null) {
                    addMarker(venue)
                }
            }
        }
    }

    private fun addMarker(venue: Venue) {
        GlideUtil.loadCircleBitmap(context, venue.logo) {
            val marker = MarkerOptions().position(LatLng(venue.latitude!!, venue.longitude!!)).title(venue.name).icon(
                BitmapDescriptorFactory.fromBitmap(it)
            )
            val mar = map?.addMarker(marker)
            mar?.tag = venue.id
        }
    }

    private fun initVenues(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = VenuesMapAdapter()
            adapter.setOnSelectedVenueListener { venue ->
                val currentLocation = LatLng(venue.latitude!!, venue.longitude!!)
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16f))
                VenueBottomSheet.newInstance(venue.id).show(childFragmentManager, VenueBottomSheet.TAG)
            }
            RecyclerUtil.setUpHorizontal(context, rclMap, adapter)
            adapter.addAll(venues)
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        val venueId = p0?.tag
        if (venues!=null) {
            for (venue in venues!!) {
                if (venue.id == venueId) {
                    val currentLocation = LatLng(venue.latitude!!, venue.longitude!!)
                    map?.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16f))
                    VenueBottomSheet.newInstance(venue.id).show(childFragmentManager, VenueBottomSheet.TAG)
                    return true
                }
            }
        }
        return false
    }


}