package com.vice_hotel.view.fragment

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.MotionEvent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.koushikdutta.ion.Ion
import com.stfalcon.frescoimageviewer.ImageViewer
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.VenueGalleryVerticalAdapter
import kotlinx.android.synthetic.main.fragment_venue_gallery_vertical.*

class VenueGalleryVerticalFragment: BaseFragment() {

    private lateinit var adapter: VenueGalleryVerticalAdapter

    private var urlGetNextPictures: String = ""
    private var isLoading = false
    private var timer: CountDownTimer? = null
    private var linnearLayout: LinearLayoutManager? = null

    private val args: VenueGalleryVerticalFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_venue_gallery_vertical

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        adapter = VenueGalleryVerticalAdapter()
        adapter.setOnItemClickListener { position ->
            ImageViewer.Builder(context, adapter.urls)
                .setStartPosition(position)
                .setBackgroundColorRes(R.color.colorPrimaryDark)
                .show()
        }
        RecyclerUtil.setUp(context, rclGallery, adapter, 2)
        getVenueGalleries()
        linnearLayout = rclGallery.layoutManager as LinearLayoutManager
        rclGallery.addOnScrollListener(listener)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rclGallery.removeOnScrollListener(listener)
        timer?.cancel()
        timer = null
    }

    private val listener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (linnearLayout!=null) {
                if (linnearLayout!!.findLastCompletelyVisibleItemPosition() > ((rclGallery.childCount / 2) -2)
                    && AccessToken.getCurrentAccessToken()!=null) {
                    loadMorePictures()
                }
            }
        }
    }

    private fun runAutoScroll() {
        timer = object: CountDownTimer(999999, 100) {
            override fun onTick(millisUntilFinished: Long) {
                rclGallery?.post {
                    rclGallery?.smoothScrollBy(0, 10)
                }
            }

            override fun onFinish() {}
        }
        timer?.start()
        rclGallery.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    timer?.cancel()
                }
                MotionEvent.ACTION_UP -> {
                    Handler().postDelayed({
                        timer?.start()
                    },3000)
                }
            }
            false
        }
    }

    private fun getVenueGalleries() {
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenueReadMoreById(context, args.venueId)
            .observeOnce(this, Observer {
                getPhotoFacebook(it.data?.venue?.gallery, it.data?.venue?.facebook)
                Handler().postDelayed({runAutoScroll()}, 2000)
            })
    }

    private fun getPhotoFacebook(pictures: ArrayList<String>?, facebookId: String?) {
        var photos = pictures
        if (AccessToken.getCurrentAccessToken()!=null) {
            GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(),"/${facebookId}/photos/uploaded") { response ->
                if (response!= null && response.jsonObject!=null) {
                    val json = response.jsonObject
                    val jsonNextPage = json.getJSONObject("paging").getString("next")
                    val jsonData = json.getJSONArray("data")
                    for (index in 0 until jsonData.length()) {
                        val jsonPhoto = jsonData.getJSONObject(index)
                        val id = jsonPhoto.getString("id")
                        if (photos==null) {
                            photos = ArrayList<String>()
                        }
                        photos?.add("https://graph.facebook.com/${id}/picture")
                    }
                    urlGetNextPictures = jsonNextPage
                    adapter.addAll(photos)
                } else {
                    if (photos!=null) {
                        adapter.addAll(photos)
                    }
                }
            }.executeAsync()
        } else {
            if (photos!=null) {
                adapter.addAll(photos)
            }

        }

    }

    private fun loadMorePictures() {
        if (isLoading) {
            return
        }
        isLoading = true
        val photos = ArrayList<String>()
        Ion.with(context).load(urlGetNextPictures).asJsonObject().setCallback { _, result ->
            if (result!=null) {
                val string = result.toString()
                val json = JsonParser().parse(string).asJsonObject
                val jsonPage = json.get("paging")
                if (jsonPage!=null && jsonPage.isJsonObject) {
                    val jsonObjectPage = jsonPage.asJsonObject
                    val jsonNext = jsonObjectPage.get("next")
                    if (jsonNext!=null) {
                        val urlNext = jsonNext.asString
                        urlGetNextPictures = urlNext
                        val jsonData = json.getAsJsonArray("data")
                        if (jsonData!=null && jsonData.size() > 0) {
                            for (index in 0 until jsonData.size()) {
                                val jsonPhoto = jsonData.get(index)
                                if (jsonPhoto!=null && jsonPhoto.isJsonObject) {
                                    val id = jsonPhoto.asJsonObject.get("id")
                                    if (id!=null) {
                                        photos.add("https://graph.facebook.com/${id.asString}/picture")
                                    }

                                }

                            }
                            if (photos.size>0) {
                                isLoading = false
                            }
                            adapter.addAllMore(photos)
                        }

                    }
                }

            }
        }
    }

}