package com.vice_hotel.view.fragment

import android.os.Bundle
import android.widget.LinearLayout.HORIZONTAL
import android.widget.LinearLayout.VERTICAL
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentChallengesDetailBinding
import com.vice_hotel.model.data.Challenge
import com.vice_hotel.model.data.Condition
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.RewardModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.ChallengeConditionAdapter
import com.vice_hotel.view.adapter.ChallengesAdapter
import com.vice_hotel.view.adapter.support.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_challenges_detail.*

class ChallengeDetailFragment: BaseFragment() {

    private val args: ChallengeDetailFragmentArgs by navArgs()

    private var adapterCondition: ChallengeConditionAdapter? = null

    override fun getLayoutRes(): Int = R.layout.fragment_challenges_detail

    override fun onViewCreated(savedInstanceState: Bundle?) {
        adapterCondition = ChallengeConditionAdapter()
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        rclCondition.addItemDecoration(SimpleDividerItemDecoration(context!!))
        RecyclerUtil.setUp(context, rclCondition, adapterCondition)
        loadChallenge()

    }

    private fun loadChallenge() {
        ViewModelProviders.of(this).get(RewardModel::class.java).getChallengeDetail(context,args.challengeId).observeOnce(this, Observer {
            if (it.isSuccess()) {
                val bind = DataBindingUtil.bind<FragmentChallengesDetailBinding>(fragmentView!!)
                val challenge = it.data?.challenge
                if (challenge!=null) {

                    btnSubmit.isEnabled = when (challenge.claimAvailable) {
                        true -> true
                        false -> false
                        null -> false
                    }
                    btnSubmit.setOnClickListener() {claimChallenge()}

                    bind?.data = challenge
                    val conditions = challenge.conditions
                    if (conditions!=null && conditions.isNotEmpty()) {
                        adapterCondition?.addAll(conditions)
                        return@Observer
                    }
                }
            } else {
                AppUtil.toast(context, it.message, false)
            }
        })
    }

    private fun claimChallenge() {
        ViewModelProviders.of(this).get(RewardModel::class.java).claimChallenge(context,args.challengeId)
            .observeOnce(this, Observer { claim ->
            if (claim.isSuccess()) {
                DialogUtil.OpenCustomDialog(activity, getString(R.string.congratulations),
                    getString(R.string.you_have_win_our_challenges), R.string.ok, null) {
                    activity?.onBackPressed()
                }
            } else {
                AppUtil.toast(context, claim.message, false)
            }
        })
    }
}