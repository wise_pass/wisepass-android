package com.vice_hotel.view.fragment.plan_payment

import android.content.Context
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AppsFlyerLib
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentCheckOutBinding
import com.vice_hotel.model.data.CardInfor
import com.vice_hotel.model.data.CheckOut
import com.vice_hotel.model.data.Payment
import com.vice_hotel.model.data.PlanCheckOut
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.plan_payment.BrainTreeUtil
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_check_out.*

class CheckOutFragment: BaseFragment() {

    private val args: CheckOutFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_check_out

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        btnUpdatePaymentMethod.setOnClickListener() {
            when (args.planType?.contentEquals(Constant.REWARD)) {
                true -> {
                    findNavController().navigate(R.id.action_checkOutFragment2_to_addCardsFragment3)
                }
                else -> {
                    findNavController().navigate(R.id.action_checkOutFragment_to_addCardsFragment2)
                }
            }
        }
        when (args.planType?.contentEquals(Constant.REWARD)) {
            true -> {
                getCardBilling(null)
                lnInfor.visibility = View.GONE
                getCheckOutReward()
            }
            else -> {
                getCheckOutData(args.planId)
            }
        }
    }

    private fun getCheckOutReward() {
        ViewModelProviders.of(this).get(CheckoutModel::class.java)
            .getCheckOutRequest(context).observe(this, Observer {
                when (it.data?.checkout?.total) {
                    null -> {
                        AppUtil.toast(context, it.message, false)
                    }
                    else -> {
                        totalPrice.text = it.data.checkout.total
                    }
                }
            })
    }

    private fun getCheckOutData(planId: String?) {
        DialogUtil.OpenLoadingDialog(activity as BaseActivity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).getCheckOutRequest(context, planId).observeOnce(this, Observer {
            DialogUtil.dismiss()
            if (it.isSuccess()) {
                val binding: FragmentCheckOutBinding? = DataBindingUtil.bind(fragmentView!!)
                binding?.checkOut = it.data?.checkout
                getCardBilling(it.data?.checkout)
            } else {
                DialogUtil.OpenCustomDialogError(activity, null, it.message, R.string.cancel, null) {
                    DialogUtil.dismiss()
                    activity?.onBackPressed()
                }
            }
        })
    }

    private fun getCardBilling(planCheckOut: PlanCheckOut?) {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).getPaymentMethodsHistories(context).observeOnce(this, Observer {
            DialogUtil.dismiss()
            if (it.isSuccess()) {
                val cards = it.data?.cards
                if (!cards.isNullOrEmpty()) {
                    for (card in cards) {
                        if (card.isCardDefault) {
                            btnUpdatePaymentMethod.setText(R.string.change_card)
                            btnSubmit.isEnabled = true
                            childFragmentManager.beginTransaction().replace(R.id.container, CreditCardFragment.newInstance(card)).commit()
                            btnSubmit.setOnClickListener() { view ->
                                btnSubmit.isEnabled = false
                                if (planCheckOut!=null) {
                                    val planType = planCheckOut.name!!
                                    val priceNumber = planCheckOut.priceNumber!!.toInt()
                                    val currencyText = planCheckOut.currencyText!!
                                    val payment = Payment(args.planId, card.token)
                                    submitPayment(payment, priceNumber, currencyText, planType)
                                } else {
                                    checkOutReward()
                                }
                            }
                        }

                    }
                }

            }
        })
    }

    private fun submitPayment(payment: Payment, priceNumber: Int, currencyText: String, planType: String) {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).subscriptionCheckout(context, payment)
            .observeOnce(this, Observer { result ->
                DialogUtil.dismiss()
                if (result.isSuccess()) {
                    val telephonyManager = activity?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                    val countryIso = telephonyManager.simCountryIso.toUpperCase()
                    val eventValue = HashMap<String, Any>()
                    eventValue[AFInAppEventParameterName.REVENUE] = priceNumber
                    eventValue[AFInAppEventParameterName.CURRENCY] = currencyText
                    if (countryIso.isNotEmpty()) {
                        eventValue[AFInAppEventParameterName.COUNTRY] = countryIso
                    }
                    AppsFlyerLib.getInstance().trackEvent(activity!!.application, planType, eventValue)
                    findNavController().navigate(R.id.action_checkOutFragment_to_paymentPlanSuccessFragment)

                } else {
                    DialogUtil.OpenCustomDialogError(activity, null, result.message, R.string.cancel, null) {
                        DialogUtil.dismiss()
                        activity?.onBackPressed()
                    }
                }
            })
    }

    private fun checkOutReward() {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).checkOut(context)
            .observeOnce(this, Observer {
                DialogUtil.dismiss()
                if (it.isSuccess()) {
                    findNavController().navigate(R.id.action_checkOutFragment2_to_challengesRewardsFragment)
                } else {
                    btnSubmit.isEnabled = true
                }
            })
    }

}