package com.vice_hotel.view.fragment.plan_payment

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentCardPremiumSubscribeBinding
import com.vice_hotel.model.data.Plan
import com.vice_hotel.model.data.Subscription
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.menu.PlanBillingFragmentDirections
import kotlinx.android.synthetic.main.fragment_card_premium_subscribe.*

private const val KEY = "plan"
class CardPremiumSubscribeFragment: BaseFragment() {

    var bind : FragmentCardPremiumSubscribeBinding? = null

    companion object {
        fun newInstance(plan: Plan?) = CardPremiumSubscribeFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY, plan)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_card_premium_subscribe

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val plan = arguments?.getParcelable(KEY) as Plan?
        //listener(plan)
        bind = DataBindingUtil.bind(fragmentView!!)
        bind?.plan = plan
    }
    private fun listener(plan: Plan?) {
        if (plan==null) {
            return
        }
        btnChangePlan.setOnClickListener() {
            DialogUtil.OpenLoadingDialog(activity)
            ViewModelProviders.of(this).get(CheckoutModel::class.java)
                .cancelSubscription(context).observeOnce(this, Observer { respond ->
                    DialogUtil.dismiss()
                    when (respond.isSuccess()) {
                        true -> {
                            activity?.onBackPressed()
                        }
                        else -> {
                            Toast.makeText(context, "Cancel fail, Please contact us to support", Toast.LENGTH_LONG).show()
                        }
                    }
                })
        }
    }
}