package com.vice_hotel.view.fragment

import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.navArgs
import com.stfalcon.frescoimageviewer.ImageViewer
import com.vice_hotel.R
import com.vice_hotel.presenter.lib.glide.GlideApp
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_image_viewer.*

class ImageViewerFragment: BaseFragment() {

    private val args: ImageViewerFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_image_viewer

    override fun onViewCreated(savedInstanceState: Bundle?) {
        btnClose.setOnClickListener() {activity?.onBackPressed()}


    }
}