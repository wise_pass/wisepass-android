package com.vice_hotel.view.fragment.menu

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.data.Plan
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.braintree.CardType
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.presenter.plan_payment.PlanType
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.plan_payment.*
import kotlinx.android.synthetic.main.fragment_plan_billing.*

class PlanBillingFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_plan_billing

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() {activity?.onBackPressed() }
        getPlanBilling()
    }

    private fun getPlanBilling() {
        DialogUtil.OpenLoadingDialog(activity)
        radioGroupPlanBilling.visibility = View.GONE
        ViewModelProviders.of(this).get(PlanModel::class.java).getPlanBilling(context).observeOnce(this, Observer {
            DialogUtil.dismiss()
            if (it.isSuccess()) {
                val plan = it.data?.plan
                //val card = it.data?.card
                radioGroupPlanBilling.clearCheck()
                radioGroupPlanBilling.setOnCheckedChangeListener { group, checkedId ->
                    when (checkedId) {
                        R.id.rbPlan -> {
                            if (plan!=null) {
                                when (plan.type) {
                                    PlanType.STANDARD -> { childFragmentManager.beginTransaction().replace(R.id.container, CardStandardSubscribeFragment.newInstance(plan)).commit() }
                                    PlanType.PREMIUM -> { childFragmentManager.beginTransaction().replace(R.id.container, CardPremiumSubscribeFragment.newInstance(plan)).commit() }
                                    else -> { childFragmentManager.beginTransaction().replace(R.id.container, CardDiscoverySubscribeFragment.newInstance(plan)).commit() }
                                }
                            }
                        }
                        R.id.rbPayment -> {
                            childFragmentManager.beginTransaction().replace(R.id.container, CreditCardActiveFragment.newInstance()).commit()
                        }
                    }
                }
                if (plan!=null) {
                    radioGroupPlanBilling.visibility = View.VISIBLE
                    rbPlan.isChecked = true
                } else {
                    findNavController().navigate(R.id.action_planBillingFragment_to_plan_payment)
                }
            }
        })
    }
}