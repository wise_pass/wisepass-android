package com.vice_hotel.view.fragment.scan

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.vice_hotel.R
import com.vice_hotel.model.data.Category
import com.vice_hotel.model.data.VenueItem
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.CardAdapter
import com.vice_hotel.view.adapter.support.ShadowTransformer
import com.vice_hotel.view.fragment.venue.CardVenueItemVerticalFragment
import kotlinx.android.synthetic.main.fragment_qr_code_venue.*

private const val KEY_CATEGORY = "key_category"
class QrCodeVenueFragment: BaseFragment() {

    var venueItem: VenueItem? = null

    companion object {
        fun newInstance(category: Category?) = QrCodeVenueFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_CATEGORY, category)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_qr_code_venue

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val category = arguments?.getParcelable(KEY_CATEGORY) as Category?
        if (category!=null) {
            val cardAdapter = CardAdapter(childFragmentManager)
            if (category.items!=null) {
                venueItem = category.items?.get(0)
                for (venue in category.items!!) {
                    cardAdapter.addFragment(CardVenueItemVerticalFragment.newInstance(venue))
                }
            }
            val transformerShadow = ShadowTransformer(context, viewPager, cardAdapter)
            viewPager.setPageTransformer(false, transformerShadow)
            viewPager.offscreenPageLimit = 3
            viewPager.adapter = cardAdapter
            dots_indicator.setViewPager(viewPager)
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {
                    venueItem = category.items?.get(position)
                }

            })
        }

    }
}