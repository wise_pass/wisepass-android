package com.vice_hotel.view.fragment.dashboard

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentVenuesBinding
import com.vice_hotel.model.ResponseBody
import com.vice_hotel.model.data.Brand
import com.vice_hotel.model.data.Category
import com.vice_hotel.model.data.Venue
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import com.vice_hotel.view.adapter.*
import com.vice_hotel.view.fragment.DashboardFragmentDirections
import com.vice_hotel.view.fragment.venue.SlidePagerVenueFragment
import kotlinx.android.synthetic.main.fragment_venues.*
import android.R.attr.factor
import android.util.DisplayMetrics
import android.graphics.PointF
import android.util.Log
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearSmoothScroller
import com.braintreepayments.cardform.view.CountryCodeEditText
import com.vice_hotel.model.data.LetPass
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import java.util.*
import kotlin.collections.ArrayList


class VenuesFragment: BaseFragment() {

    companion object {
        fun newInstance(isCallPlan: Boolean) = VenuesFragment().apply {
            arguments = Bundle().apply {
                putBoolean("isCallPlan", isCallPlan)
            }
        }
    }

    private var isCallPlan = false

    override fun getLayoutRes(): Int = R.layout.fragment_venues

    override fun onViewCreated(savedInstanceState: Bundle?) {
        isCallPlan = arguments!!.getBoolean("isCallPlan", false)
        actionSearch.setOnClickListener() { findNavController().navigate(R.id.action_dashboardFragment_to_searchVenuesFragment) }
        loadVenues()
        btnSeeAllVenues.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_venueListFragment)
        }
    }

    private fun loadVenues() {
        containerListData.visibility = View.GONE
        val viewModel = ViewModelProviders.of(this).get(VenueModel::class.java).getHomeVenues(context)
        viewModel.observeOnce(this, Observer {
            fragmentReady(it)
        })
    }



    private fun fragmentReady(it: ResponseBody) {
        if (it.isSuccess()) {
            val binding: FragmentVenuesBinding? = DataBindingUtil.bind(fragmentView!!)
            containerListData.visibility = View.VISIBLE
            if (it.data?.slideMenu!=null) {
                initSlide(it.data.slideMenu)
            }
            if (it.data?.suggestions!=null) {
                binding?.isSuggestionNotEmpty = true
                initSuggestion(it.data.suggestions)
            }

            initCategories(null)

            if (it.data?.brands!=null) {
                binding?.isBrandNotEmpty = true
                initBrands(it.data.brands)
            }
            if (it.data?.nearby!=null) {
                binding?.isNearbyNotEmpty = true
                initNearby(it.data.nearby)
            }
            if (it.data?.trending!=null) {
                binding?.isTrendingNotEmpty = true
                initTrending(it.data.trending)
            }
            if (it.data?.favorites!=null) {
                binding?.isFavoritesNotEmpty = true
                initFavorites(it.data.favorites)
            }
            if (it.data?.recentlyAdded!=null) {
                binding?.isRecentlyAdded = true
                initRecentlyAdded(it.data.recentlyAdded)
            }
            if (it.data?.bestSpots!=null) {
                binding?.isBestSpotsRecentlyAddedNotEmpty = true
                initBestSpots(it.data.bestSpots)
            }
            if (it.data?.discovery!=null) {
                binding?.isDiscoveryNotEmpty = true
                initDiscovery(it.data.discovery)
            }

            binding?.isCategoryNotEmpty = false


            if (activity is MainActivity) {
                when ((activity as MainActivity).goToPage) {
                    LetPass.REWARD -> {
                        (activity as MainActivity).goToPage = ""
                        findNavController().navigate(R.id.action_dashboardFragment_to_challengesRewardsFragment)
                    }
                    LetPass.PLAN -> {
                        (activity as MainActivity).goToPage = ""
                        findNavController().navigate(R.id.action_dashboardFragment_to_planBillingFragment)
                    }
                    else -> {
                        if (isCallPlan) {
                            isCallPlan = false
                            findNavController().navigate(R.id.action_dashboardFragment_to_plan_payment)
                        } else if (!(activity as MainActivity).venueID.isNullOrEmpty()) {
                            val directions = DashboardFragmentDirections.actionDashboardFragmentToVenueDetailFragment((activity as MainActivity).venueID)
                            findNavController().navigate(directions)
                            (activity as MainActivity).venueID = null
                        } else if (it.data?.slideMenu!=null && it.data.slideMenu.size == 0) {
                            val appConfig = AppManager.instance.getAppConfig()
                            val countryCode = appConfig?.country_code
                            when (countryCode) {
                                "VN" -> {
                                    DialogUtil.OpenCustomDialogMultiOptions(activity, getString(R.string.apologize), getString(R.string.error_no_venues), R.string.ho_chi_minh_city, R.string.ha_noi_city) {
                                        DialogUtil.dismiss()

                                        if (it==1) {
                                            appConfig.latitude = 10.782622
                                            appConfig.longitude = 106.691555
                                        } else {
                                            appConfig.latitude = 21.029013
                                            appConfig.longitude = 105.834952
                                        }
                                        val requestCheckVersion = ApiService.create(context)?.setConfigAndCheckVersion(appConfig)
                                        RetrofitUtil.requestAPI(context, requestCheckVersion) {
                                            if (it.isSuccess()) {
                                                loadVenues()
                                            }
                                        }
                                    }
                                }
                                "TH" -> {
                                    appConfig.latitude = 13.755401
                                    appConfig.longitude = 100.501532
                                    val requestCheckVersion = ApiService.create(context)?.setConfigAndCheckVersion(appConfig)
                                    RetrofitUtil.requestAPI(context, requestCheckVersion) {
                                        if (it.isSuccess()) {
                                            loadVenues()
                                        }
                                    }
                                }
                                "PH" -> {
                                    appConfig.latitude = 14.599941
                                    appConfig.longitude = 120.982186
                                    val requestCheckVersion = ApiService.create(context)?.setConfigAndCheckVersion(appConfig)
                                    RetrofitUtil.requestAPI(context, requestCheckVersion) {
                                        if (it.isSuccess()) {
                                            loadVenues()
                                        }
                                    }
                                }
                                else -> {
                                    DialogUtil.OpenCustomDialog(activity, getString(R.string.apologize), getString(R.string.error_no_venues_default_hcm), R.string.ho_chi_minh_city, null) {
                                        DialogUtil.dismiss()
                                        if (appConfig!=null) {
                                            appConfig.latitude = 10.782622
                                            appConfig.longitude = 106.691555
                                            val requestCheckVersion = ApiService.create(context)?.setConfigAndCheckVersion(appConfig)
                                            RetrofitUtil.requestAPI(context, requestCheckVersion) {
                                                if (it.isSuccess()) {
                                                    loadVenues()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //findNavController().navigate(R.id.action_dashboardFragment_to_cityPickerFragment)
                        }
                    }
                }
            }
        } else {
            if (it.statusCode!=701) {
                DialogUtil.OpenCustomDialogError(activity, null, getString(R.string.network_offline), R.string.try_again, null) {
                    DialogUtil.dismiss()
                    if (it) {
                        loadVenues()
                    }
                }
            }
        }
    }

    private fun initSlide(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = PagerAdapter(childFragmentManager)
            for (venue in venues) {
                adapter.addFragment(SlidePagerVenueFragment.newInstance(venue))
            }
            viewPagerHomeSlide.adapter = adapter
            viewPagerHomeSlide.setOnClickListener() {
                val position = viewPagerHomeSlide.currentItem
                val venue = venues[position]
                val directions = DashboardFragmentDirections.actionDashboardFragmentToVenueDetailFragment(venue.id)
                findNavController().navigate(directions)
            }
            dots_indicator.setViewPager(viewPagerHomeSlide)
        }
    }

    private fun initSuggestion(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = VenueSuggestionAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclSuggestion, adapter)
            adapter.addAll(venues)
        }
    }

    private fun initCategories(categories: ArrayList<Category>?) {
        val adapter = HomeVenueCategoryAdapter()
        RecyclerUtil.setUpHorizontal(context, rclCategories, adapter)
        adapter.addAll(categories)
    }

    var timer: CountDownTimer? = null

    private fun initBrands(brands: ArrayList<Brand>?) {
        val adapter = HomeVenueBrandsAdapter()
        adapter.setOnSelectedVenueListener { brand ->
            when (brand.keyword?.toLowerCase()?.trim()) {
                Constant.BRAND_GRAB -> {
                    val direction = DashboardFragmentDirections.actionDashboardFragmentToPromotionCodeFragment(Constant.BRAND_GRAB)
                    findNavController().navigate(direction)
                }
                Constant.PROMOTION -> {
                    val direction = DashboardFragmentDirections.actionDashboardFragmentToPromotionCodeFragment(Constant.PROMOTION)
                    findNavController().navigate(direction)
                }
                Constant.BRAND_SHOPEE -> {
                    val direction = DashboardFragmentDirections.actionDashboardFragmentToPromotionCodeFragment(Constant.BRAND_SHOPEE)
                    findNavController().navigate(direction)
                }
                else -> {
                    val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment(brand.keyword, brand.display)
                    findNavController().navigate(direction)
                }
            }

        }
        RecyclerUtil.setUpHorizontal(context, rclBrands, adapter)
        adapter.addAll(brands)
        timer = object: CountDownTimer(999999, 100) {
            override fun onTick(millisUntilFinished: Long) {
                rclBrands?.post {
                    rclBrands?.smoothScrollBy(10, 0)
                }
            }

            override fun onFinish() {}
        }
        timer?.start()
        rclBrands.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    timer?.cancel()
                }
                MotionEvent.ACTION_UP -> {
                    Handler().postDelayed({
                        timer?.start()
                    },3000)
                    //
                }
            }
            false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer?.cancel()
        timer = null
    }

    private fun initNearby(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = HomeVenueAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclNearby, adapter, true)
            adapter.addAll(venues)
            btnNearBySeeAll.visibility = View.VISIBLE
            btnNearBySeeAll.setOnClickListener() {
                val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment("nearby", getString(R.string.nearby))
                findNavController().navigate(direction)
            }
        }
    }

    private fun initTrending(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = HomeVenueAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclTrending, adapter)
            adapter.addAll(venues)
            btnTrendingSeeAll.visibility = View.VISIBLE
            btnTrendingSeeAll.setOnClickListener() {
                val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment("trending", getString(R.string.trending))
                findNavController().navigate(direction)
            }
        }
    }

    private fun initFavorites(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = HomeVenueAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclFavorites, adapter)
            adapter.addAll(venues)
            btnFavoriteSeeAll.visibility = View.VISIBLE
            btnFavoriteSeeAll.setOnClickListener() {
                val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment("favorites", getString(R.string.favorites))
                findNavController().navigate(direction)
            }
        }
    }

    private fun initRecentlyAdded(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = HomeVenueAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclRecentlyAdded, adapter)
            adapter.addAll(venues)
            btnRecentlyAddedSeeAll.visibility = View.VISIBLE
            btnRecentlyAddedSeeAll.setOnClickListener() {
                val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment("recentlyAdded", getString(R.string.recently_added))
                findNavController().navigate(direction)
            }
        }
    }

    private fun initBestSpots(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = HomeVenueAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclBestSpots, adapter)
            adapter.addAll(venues)
            btnBestSpotsSeeAll.visibility = View.VISIBLE
            btnBestSpotsSeeAll.setOnClickListener() {
                val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment("bestSpots", getString(R.string.bestspots))
                findNavController().navigate(direction)
            }
        }
    }

    private fun initDiscovery(venues: ArrayList<Venue>?) {
        if (venues!=null && venues.size > 0) {
            val adapter = HomeVenueAdapter()
            adapter.setOnSelectedVenueListener { selectedVenue(it) }
            RecyclerUtil.setUpHorizontal(context, rclDiscovery, adapter)
            adapter.addAll(venues)
            btnDiscoverySeeAll.visibility = View.VISIBLE
            btnDiscoverySeeAll.setOnClickListener() {
                val direction = DashboardFragmentDirections.actionDashboardFragmentToVenuesEachTypeFragment("discovery", getString(R.string.discovery))
                findNavController().navigate(direction)
            }
        }
    }

    private fun selectedVenue(venue: Venue) {
        val directions = DashboardFragmentDirections.actionDashboardFragmentToVenueDetailFragment(venueId = venue.id)
        findNavController().navigate(directions)
    }

}