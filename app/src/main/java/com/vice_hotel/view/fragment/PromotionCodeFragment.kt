package com.vice_hotel.view.fragment

import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.view_model.PromotionModel
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_promotion_code.*

class PromotionCodeFragment: BaseFragment() {

    private val args: PromotionCodeFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_promotion_code

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        edPromotionCode.addTextChangedListener {
            val text = it.toString().trim()
            btnSubmit.isEnabled = text.isNotEmpty()
        }
        btnSubmit.setOnClickListener() {
            val code = edPromotionCode.text.toString().trim()
            leftPassFromPromotionCode(code)
        }
    }

    private fun leftPassFromPromotionCode(code: String) {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(PromotionModel::class.java)
            .letPass(context, code).observe(this, androidx.lifecycle.Observer { letpass ->
                DialogUtil.dismiss()
                if (letpass.isSuccess()) {
                    findNavController().navigate(R.id.action_promotionCodeFragment_to_challengesRewardsFragment)
                } else {
                    DialogUtil.OpenCustomDialogError(activity, getString(R.string.error), letpass.message, R.string.close, null) {
                        DialogUtil.dismiss()
                        edPromotionCode.text = null
                    }
                }
            })
    }
}