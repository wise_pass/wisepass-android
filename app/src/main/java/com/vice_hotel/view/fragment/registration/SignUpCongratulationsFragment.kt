package com.vice_hotel.view.fragment.registration

import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentSignUpCongratulationsBinding
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.LocaleManager
import com.vice_hotel.presenter.helper.PreferenceUtils
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up_congratulations.*

class SignUpCongratulationsFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_sign_up_congratulations

    override fun onViewCreated(savedInstanceState: Bundle?) {
        loadContentOnUI()
        btnExplore.setOnClickListener() {
            PreferenceUtils(context).put(PreferenceUtils.DASHBOARD_ACTION, Constant.ACTION_DASHBOARD_TO_PLAN_PICKER)
            val directions = SignUpInputOTPFragmentDirections.actionSignUpDashboardFragment()
            findNavController().navigate(directions)
        }
    }
    private fun loadContentOnUI() {
        val binding: FragmentSignUpCongratulationsBinding? = DataBindingUtil.bind(fragmentView!!)
        val lang = LocaleManager.getLanguage(context)
        LocaleManager.changeLocateContext(context, lang) {
            binding?.congratulation = it?.getString(R.string.congratulations)
            binding?.youAreOurMemberWelcomeClub = it?.resources?.getString(R.string.you_are_our_member_welcome_club)
        }
    }

}