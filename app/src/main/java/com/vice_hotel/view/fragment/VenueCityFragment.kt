package com.vice_hotel.view.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.SearchVenueAdapter
import kotlinx.android.synthetic.main.fragment_search_venue.*
import kotlinx.android.synthetic.main.fragment_search_venue.actionBack
import kotlinx.android.synthetic.main.fragment_search_venue.rclVenues
import kotlinx.android.synthetic.main.fragment_venue_city.*
import java.util.*
import kotlin.Comparator

class VenueCityFragment : BaseFragment() {

    val args: VenueCityFragmentArgs by navArgs()

    var adapter: SearchVenueAdapter? = SearchVenueAdapter()

    override fun getLayoutRes(): Int = R.layout.fragment_venue_city

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() {
            activity?.onBackPressed()
        }
        adapter?.setOnSelectedVenueListener {
            val direction = VenueCityFragmentDirections.actionVenueCityFragmentToVenueDetailFragment(it.id)
            findNavController().navigate(direction)
        }
        RecyclerUtil.setUp(context, rclVenues, adapter, 2)
        getVenues()
        swipeRefresh.setOnRefreshListener { getVenues() }
    }

    private fun getVenues() {
        swipeRefresh.isRefreshing = true
        ViewModelProviders.of(this).get(VenueModel::class.java)
            .getVenuesByCity(context, args.cityCode).observeOnce(this, Observer {
                swipeRefresh.isRefreshing = false
                if (it.isSuccess()) {
                    val list = it.data?.venues
                    adapter?.addAll(list)
                    AppUtil.notifyDataChanged(rclVenues)
                }
            })
    }
}