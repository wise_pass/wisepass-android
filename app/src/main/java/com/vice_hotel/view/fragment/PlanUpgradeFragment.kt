package com.vice_hotel.view.fragment

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.presenter.plan_payment.PlanType
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.CardAdapter
import com.vice_hotel.view.adapter.support.ShadowTransformer
import com.vice_hotel.view.fragment.plan_payment.CardDiscoveryFragment
import com.vice_hotel.view.fragment.plan_payment.CardPremiumFragment
import com.vice_hotel.view.fragment.plan_payment.CardStandardFragment
import kotlinx.android.synthetic.main.fragment_plan_picker.*

class PlanUpgradeFragment: BaseFragment() {

    private val args: PlanUpgradeFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_plan_picker

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        Handler().postDelayed({
            getPlansFromServer()
        }, 300)
    }

    private fun getPlansFromServer() {
        ViewModelProviders.of(this).get(PlanModel::class.java).getPlans(context).observeOnce(this, Observer {
            progressBar.visibility = View.GONE
            if (it.statusCode== HTTP_OK) {
                val plans = it.data?.payment_plans
                if (plans!=null) {
                    val cardAdapter = CardAdapter(childFragmentManager)
                    for (plan in plans) {
                        plan.isUpgrade = true
                        when (plan.type) {
                            PlanType.STANDARD -> { cardAdapter.addFragment(CardStandardFragment.newInstance(plan)) }
                            PlanType.PREMIUM -> { cardAdapter.addFragment(CardPremiumFragment.newInstance(plan)) }
                            else -> {
                                cardAdapter.addFragment(CardDiscoveryFragment.newInstance(plan))
                            }
                        }
                    }
                    val transformerShadow = ShadowTransformer(context, viewPager, cardAdapter)
                    viewPager.setPageTransformer(false, transformerShadow)
                    viewPager.offscreenPageLimit = 3
                    viewPager.adapter = cardAdapter
                    dots_indicator.setViewPager(viewPager)
                }

            }
        })
    }




}