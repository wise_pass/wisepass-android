package com.vice_hotel.view.fragment

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.data.Challenge
import com.vice_hotel.model.data.ScanType
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.RewardModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.ChallengesAdapter
import com.vice_hotel.view.adapter.RewardAdapter
import kotlinx.android.synthetic.main.fragment_challenges.*

class ChallengesFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_challenges

    override fun onViewCreated(savedInstanceState: Bundle?) {
        swipeRefresh.setOnRefreshListener {
            getChallenges()
        }
        getChallenges()
    }

    private fun getChallenges() {
        swipeRefresh.isRefreshing = true
        ViewModelProviders.of(this).get(RewardModel::class.java).getChallenges(context).observeOnce(this, Observer {
            swipeRefresh.isRefreshing = false
            if (it.isSuccess()) {
                val challenges = it.data?.challenges
                if (challenges!=null && challenges.isNotEmpty()) {
                    val adapter = ChallengesAdapter()
                    RecyclerUtil.setUp(context, rcl, adapter)
                    adapter.addAll(challenges)
                    adapter.setOnItemSelectedListener {
                        val direction = ChallengesRewardsFragmentDirections.actionChallengesRewardsFragmentToChallengeDetailFragment(it)
                        findNavController().navigate(direction)
                    }
                    return@Observer
                }
            }
        })
    }

}