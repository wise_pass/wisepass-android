package com.vice_hotel.view.fragment.menu

import android.os.Bundle
import com.vice_hotel.R
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import android.content.Intent
import com.vice_hotel.presenter.helper.Constant
import android.app.Activity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vice_hotel.databinding.FragmentProfileBinding
import com.vice_hotel.model.data.UserAccount
import com.vice_hotel.presenter.view_model.AccountModel
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.lib.glide.GlideUtil

class ProfileFragment: BaseFragment() {
    override fun getLayoutRes(): Int = R.layout.fragment_profile

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener () { activity?.onBackPressed() }
        imgAvatar.setOnClickListener() {
            val intent = Intent(Intent.ACTION_PICK).apply {
                type = "image/*"
                putExtra("scale", true)
                putExtra("aspectX", 1)
                putExtra("aspectY", 1)
            }
            startActivityForResult(intent, Constant.REQUEST_SELECT_PICTURE)
        }
        loadProfile()
    }

    private fun loadProfile() {
        ViewModelProviders.of(this).get(AccountModel::class.java).getProfile(context).observeOnce(this, Observer {
            if (it.isSuccess()) {
                val binding: FragmentProfileBinding? = DataBindingUtil.bind(fragmentView!!)
                binding?.userAccount = it.data?.user_profile
            } else {
                AppUtil.toast(context, it.message, false)
                activity?.onBackPressed()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                Constant.REQUEST_SELECT_PICTURE -> {
                    val selectedImage = data?.data
                    DialogUtil.OpenLoadingDialog(activity)
                    ViewModelProviders.of(this).get(AccountModel::class.java)
                        .updateProfile(context, UserAccount(image_base64 = AppUtil.encodeImage(context, selectedImage)))
                        .observe(this, Observer {
                            DialogUtil.dismiss()
                            if (it.isSuccess()) {
                                GlideUtil.loadCircle(context, it.data?.user_profile?.image_url, imgAvatar)
                            } else {
                                AppUtil.toast(context, it.message, false)
                            }
                        })
                }
            }

    }
}