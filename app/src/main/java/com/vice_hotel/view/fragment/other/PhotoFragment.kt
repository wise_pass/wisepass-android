package com.vice_hotel.view.fragment.other

import android.os.Bundle
import com.vice_hotel.R
import com.vice_hotel.presenter.lib.glide.GlideUtil
import com.vice_hotel.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_photo.*

private const val KEY = "key_photo_fragment"
class PhotoFragment: BaseFragment() {

    companion object {
        fun newInstance(photoUrl: String) = PhotoFragment().apply {
            arguments = Bundle().apply {
                putString(KEY, photoUrl)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_photo

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val url = arguments?.getString(KEY, "")
        if (url!!.isNotEmpty()) {
            GlideUtil.load(context, url.trim(), img)
        }
    }
}