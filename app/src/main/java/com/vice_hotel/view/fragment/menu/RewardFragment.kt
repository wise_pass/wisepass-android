package com.vice_hotel.view.fragment.menu

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.data.ScanType
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.RewardModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.RewardAdapter
import com.vice_hotel.view.fragment.ChallengesRewardsFragment
import com.vice_hotel.view.fragment.ChallengesRewardsFragmentDirections
import kotlinx.android.synthetic.main.fragment_reward.*

class RewardFragment: BaseFragment() {

    private lateinit var adapter: RewardAdapter

    override fun getLayoutRes(): Int = R.layout.fragment_reward

    override fun onViewCreated(savedInstanceState: Bundle?) {
        adapter = RewardAdapter()
        RecyclerUtil.setUp(context, rcl, adapter)
        adapter.setOnItemSelectedListener {
            val direction = ChallengesRewardsFragmentDirections.actionChallengesRewardsFragmentToScanQrCodeFragment(ScanType(
                type = ScanType.REWARD,
                promoId = it
            ))
            findNavController().navigate(direction)
        }
        swipeRefresh.setOnRefreshListener {
            getRewards()
        }
        getRewards()
    }

    private fun getRewards() {
        swipeRefresh.isRefreshing = true
        ViewModelProviders.of(this).get(RewardModel::class.java).getRewards(context).observeOnce(this, Observer {
            swipeRefresh.isRefreshing = false
            if (it.isSuccess()) {
                val promos = it.data?.promo
                if (promos!=null && promos.isNotEmpty()) {
                    adapter.addAll(it.data.promo)
                    return@Observer
                }
            }
        })
    }

}