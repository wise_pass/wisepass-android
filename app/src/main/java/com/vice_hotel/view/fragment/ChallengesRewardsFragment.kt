package com.vice_hotel.view.fragment

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.menu.PlanBillingFragmentDirections
import com.vice_hotel.view.fragment.menu.RewardFragment
import kotlinx.android.synthetic.main.fragment_challenges_rewards.*

class ChallengesRewardsFragment : BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_challenges_rewards

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        radioGroupMenu.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                rbChallenge.id -> {
                    fragmentManager?.beginTransaction()?.replace(R.id.containerRewards, ChallengesFragment())?.commit()
                }
                rbReward.id -> {
                    fragmentManager?.beginTransaction()?.replace(R.id.containerRewards, RewardFragment())?.commit()
                }
            }
        }
        Handler().postDelayed({
            radioGroupMenu.check(rbReward.id)
        }, 500)
    }
}