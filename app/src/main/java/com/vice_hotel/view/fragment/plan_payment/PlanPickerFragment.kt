package com.vice_hotel.view.fragment.plan_payment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.braintreepayments.api.dropin.DropInRequest
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.CardAdapter
import com.vice_hotel.view.adapter.support.ShadowTransformer
import kotlinx.android.synthetic.main.fragment_card_detail.*
import kotlinx.android.synthetic.main.fragment_plan_picker.*
import kotlinx.android.synthetic.main.fragment_plan_picker.actionBack
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInResult
import android.app.Activity.RESULT_OK
import android.util.Log
import android.view.View
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.plan_payment.PlanType

class PlanPickerFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_plan_picker

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        Handler().postDelayed({
            getPlansFromServer()
        }, 300)
    }

    private fun getPlansFromServer() {
        ViewModelProviders.of(this).get(PlanModel::class.java).getPlans(context).observeOnce(this, Observer {
            progressBar.visibility = View.GONE
            if (it.statusCode== HTTP_OK) {
                val plans = it.data?.payment_plans
                if (plans!=null) {
                    val cardAdapter = CardAdapter(childFragmentManager)
                    for (plan in plans) {
                        when (plan.type) {
                            PlanType.STANDARD -> { cardAdapter.addFragment(CardStandardFragment.newInstance(plan)) }
                            PlanType.PREMIUM -> { cardAdapter.addFragment(CardPremiumFragment.newInstance(plan)) }
                            else -> {
                                cardAdapter.addFragment(CardDiscoveryFragment.newInstance(plan))
                            }
                        }

                    }
                    val transformerShadow = ShadowTransformer(context, viewPager, cardAdapter)
                    viewPager.setPageTransformer(false, transformerShadow)
                    viewPager.offscreenPageLimit = 3
                    viewPager.adapter = cardAdapter
                    dots_indicator.setViewPager(viewPager)
                }

            }
        })
    }




}