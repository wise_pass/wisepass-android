package com.vice_hotel.view.fragment.menu

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.data.CardInfor
import com.vice_hotel.model.data.CheckOut
import com.vice_hotel.model.data.PlanCheckOut
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.plan_payment.CreditCardFragment
import kotlinx.android.synthetic.main.fragment_credit_card_active.*


class CreditCardActiveFragment: BaseFragment() {

    companion object {
        fun newInstance() = CreditCardActiveFragment()
    }

    override fun getLayoutRes(): Int = R.layout.fragment_credit_card_active

    override fun onViewCreated(savedInstanceState: Bundle?) {
        getCard()
        btnUpdateCard.setOnClickListener() {
            findNavController().navigate(R.id.action_planBillingFragment_to_addCardsFragment)
        }
    }

    private fun getCard() {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).getPaymentMethodsHistories(context).observeOnce(this, Observer {
            DialogUtil.dismiss()
            if (it.isSuccess()) {
                val cards = it.data?.cards
                if (!cards.isNullOrEmpty()) {
                    for (card in cards) {
                        if (card.isCardDefault) {
                            childFragmentManager.beginTransaction().replace(R.id.container, CreditCardFragment.newInstance(card)).commit()
                            enableRemoveCard(card)
                        }
                    }
                }

            }
        })
    }

    private fun enableRemoveCard(cardInfor: CardInfor) {
        btnRemoveCard.visibility = View.VISIBLE
        btnRemoveCard.setOnClickListener() {
            DialogUtil.OpenLoadingDialog(activity)
            ViewModelProviders.of(this).get(CheckoutModel::class.java).removeCard(context, cardInfor)
                .observe(this, Observer {
                    DialogUtil.dismiss()
                    if (it.isSuccess()) {
                        activity?.onBackPressed()
                    } else {
                        DialogUtil.OpenCustomDialogError(activity, null, it.message, R.string.close, null) {
                            DialogUtil.dismiss()
                        }
                    }
                })
        }
    }

}