package com.vice_hotel.view.fragment

import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentSelectLanguageBinding
import com.vice_hotel.model.data.RequestLogin
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.presenter.view_model.SignUpModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.PagerAdapter
import kotlinx.android.synthetic.main.fragment_select_language.*
import kotlinx.android.synthetic.main.pager_item.*
import org.json.JSONObject
import java.lang.reflect.Array
import java.util.*
import kotlin.collections.ArrayList


const val TITLE_KEY_FOR_PAGER = "title_for_pager"
const val FIRST_POSITION = 400
class SelectLanguageFragment : BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_select_language

    override fun onViewCreated(savedInstanceState: Bundle?) {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        //check current language
        checkLanguageCurrent()
        listener()
        //animation
        imgLogo.animate().translationY(-AppUtil.dpToPx(context, 150)!!).start()
        val adapter = PagerAdapter(childFragmentManager)
        adapter.clearAll()
        adapter.addFragment(PagerTitleFragment.newInstance(R.string.select_language_text_01))
        adapter.addFragment(PagerTitleFragment.newInstance(R.string.select_language_text_02))
        adapter.addFragment(PagerTitleFragment.newInstance(R.string.select_language_text_03))
        pager.adapter = adapter
        dots_indicator.setViewPager(pager)
        runVideoBackground()
    }

    private fun runVideoBackground() {
        //val uri = Uri.parse("https://wisepass.co/Content/uploads/static/video_cover1.mp4")
        val uri = Uri.parse("android.resource://"+activity?.packageName+"/"+R.raw.intro)
        val heightScreen = activity?.resources?.displayMetrics?.heightPixels!! + AppUtil.dpToPx(context, 100)!!
//        val widthScreen = activity?.resources?.displayMetrics?.widthPixels!!
//
//        val widthVideo: Float = (1280f / 720f) * heightScreen
//        val left: Float = (widthVideo - widthScreen) / 2 - AppUtil.dpToPx(context, 80)!!
//        videoBackgroundFrame.layoutParams.width = widthVideo.toInt()
//        videoBackgroundFrame.x = -left
//        videoBackgroundFrame.requestLayout()
        videoBackgroundFrame.layoutParams.height = heightScreen.toInt()
        videoBackgroundFrame.requestLayout()
        videoBackgroundFrame.setVideoURI(uri)
        fristFrame.animate().alpha(0f).start()
        videoBackgroundFrame.setOnPreparedListener {
            it.setVolume(0f, 0f)
            it.isLooping = true
        }
        videoBackgroundFrame.start()
        val hasBar = AppUtil.hasNavBar(context)
        if (hasBar!=null && hasBar) {
            bottomView.animate().translationY(-AppUtil.dpToPx(context, 50)!!).start()
        }
    }

    override fun onPause() {
        super.onPause()
        videoBackgroundFrame.pause()
    }

    override fun onResume() {
        super.onResume()
        videoBackgroundFrame.start()
    }

    private fun checkLanguageCurrent() {
        val appDefaultLocale = LocaleManager.getLanguage(context)
        rgLanguages.check(
            when (appDefaultLocale) {
                Locale.ENGLISH.language -> raEnglish.id
                else -> raThailand.id
            }
        )
        loadContentOnUI(appDefaultLocale)
        rgLanguages.setOnCheckedChangeListener { group, checkedId ->
            val lang: String? = when (checkedId) {
                raEnglish.id -> LocaleManager.LANGUAGE.ENGLISH
                raChina.id -> LocaleManager.LANGUAGE.CHINESE
                else -> LocaleManager.LANGUAGE.THAILAND
            }
            loadContentOnUI(lang)
        }
    }

    private fun loadContentOnUI(lang: String?) {
        val binding: FragmentSelectLanguageBinding? = DataBindingUtil.bind(fragmentView!!)
        LocaleManager.changeLocateContext(context, lang) {
            binding?.alreadyAnUser = it?.resources?.getString(R.string.already_an_user)
            binding?.login = it?.resources?.getString(R.string.log_in)
        }
    }

    private lateinit var callbackManager: CallbackManager

    private fun listener() {
        // hide frame contain languages options
        callbackManager = CallbackManager.Factory.create()
        val loginManager = LoginManager.getInstance()
        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                DialogUtil.OpenLoadingDialog(activity)
                val requestLogin = AppManager.instance.getRequestLogin().apply {
                    facebookId = result?.accessToken?.userId
                    facebookAccessToken = result?.accessToken?.token
                }
                AppManager.instance.setRequestLogin(requestLogin)
                ViewModelProviders.of(this@SelectLanguageFragment)
                    .get(SignUpModel::class.java).loginWithFacebook(context, requestLogin)
                    .observe(this@SelectLanguageFragment, androidx.lifecycle.Observer {
                        DialogUtil.dismiss()
                        if (it.isSuccess()) {
                            if (it.data?.account!=null) {
                                val account = it.data.account
                                AppManager.instance.loginSuccess(context, account) {
                                    findNavController().navigate(R.id.action_selectLanguageFragment_to_dashboardFragment)
                                }
                            } else {
                                getEmailFromFacebook(result?.accessToken)
                            }
                        } else {
                            DialogUtil.OpenCustomDialogError(activity, getString(R.string.error), it.message, R.string.close, null) {
                                DialogUtil.dismiss()
                            }
                        }
                    })

            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {
                error?.localizedMessage
            }

        })
        btnSwitchLanguages.setOnClickListener() {
            if (frameSelectLanguages.y == 0f) {
                frameSelectLanguages.animate().translationY(-AppUtil.dpToPx(context, FIRST_POSITION)!!).start()
            } else {
                frameSelectLanguages.animate().translationY(0f).start()
            }
        }
        videoBackgroundFrame.setOnClickListener() { frameSelectLanguages.animate().translationY(-AppUtil.dpToPx(context, FIRST_POSITION)!!).start() }
        btnSignUp.setOnClickListener() { findNavController().navigate(R.id.action_selectLanguageFragment_to_nav_sign_up) }
        btnFacebook.setOnClickListener() { loginManager.logInWithReadPermissions(this, arrayListOf("email")) }
        if (AccessToken.getCurrentAccessToken()!=null) {
            LoginManager.getInstance().logOut()
            AppManager.instance.getRequestLogin().apply {
                facebookId = null
                facebookAccessToken = null
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        LoginManager.getInstance().unregisterCallback(callbackManager)
    }

    private fun getEmailFromFacebook(accessToken: AccessToken?) {
        val request = GraphRequest.newMeRequest(accessToken) { _, response ->
            val emailFacebook = response.jsonObject.getString("email")
            if (!emailFacebook.isNullOrEmpty()) {
                PreferenceUtils(context).put(PreferenceUtils.EMAIL_REGISTRATION, emailFacebook)
            }
            findNavController().navigate(R.id.action_selectLanguageFragment_to_nav_sign_up)
        }
        val permissions = Bundle().apply {
            putString("fields","id,name,email")
        }
        request.parameters = permissions
        request.executeAsync()
    }

    class PagerTitleFragment : BaseFragment() {

        companion object {
            fun newInstance(resIdString: Int): PagerTitleFragment = PagerTitleFragment().apply {
                arguments = Bundle().apply {
                    putInt(TITLE_KEY_FOR_PAGER, resIdString)
                }
            }
        }

        override fun getLayoutRes(): Int = R.layout.pager_item

        override fun onViewCreated(savedInstanceState: Bundle?) {
            val resIdString = arguments?.getInt(TITLE_KEY_FOR_PAGER)
            textView.setText(resIdString!!)
        }

    }

}