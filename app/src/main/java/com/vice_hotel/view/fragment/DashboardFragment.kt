package com.vice_hotel.view.fragment

import android.os.Bundle
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.Constant
import com.vice_hotel.presenter.helper.DialogUtil
import com.vice_hotel.presenter.helper.PreferenceUtils
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import com.vice_hotel.view.fragment.dashboard.HistoryFragment
import com.vice_hotel.view.fragment.dashboard.MapFragment
import com.vice_hotel.view.fragment.dashboard.VenuesFragment
import com.vice_hotel.view.fragment.dashboard.MenuFragment
import kotlinx.android.synthetic.main.fragment_dashboard.*

class DashboardFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_dashboard


    override fun onViewCreated(savedInstanceState: Bundle?) {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        initHome()
    }

    private fun initHome() {
        rgMenuBottom.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                raVenues.id -> directAppListener()
                raMap.id -> fragmentManager?.beginTransaction()?.replace(R.id.dashboardContainer, MapFragment())?.commit()
                raHistory.id -> fragmentManager?.beginTransaction()?.replace(R.id.dashboardContainer, HistoryFragment())?.commit()
                raMenu.id -> fragmentManager?.beginTransaction()?.replace(R.id.dashboardContainer,
                    MenuFragment()
                )?.commit()
            }
        }
        raVenues.isChecked = true
        btnOpenScanQrCode.setOnClickListener() {
            findNavController().navigate(R.id.action_dashboardFragment_to_nav_scan_qr_code)
        }
    }

    private fun directAppListener() {
        val action = PreferenceUtils(context).get(PreferenceUtils.DASHBOARD_ACTION, String.javaClass) as String
        if (action.isNotEmpty()) {
            PreferenceUtils(context).remove(PreferenceUtils.DASHBOARD_ACTION)
            if (action.contentEquals(Constant.ACTION_DASHBOARD_TO_PLAN_PICKER)) {
                fragmentManager?.beginTransaction()?.replace(R.id.dashboardContainer, VenuesFragment.newInstance(true))?.commit()
                return
            }
        }
        fragmentManager?.beginTransaction()?.replace(R.id.dashboardContainer, VenuesFragment.newInstance(false))?.commit()
    }

}