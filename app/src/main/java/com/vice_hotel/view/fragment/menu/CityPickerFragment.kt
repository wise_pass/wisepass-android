package com.vice_hotel.view.fragment.menu

import android.os.Build
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.BuildConfig
import com.vice_hotel.R
import com.vice_hotel.model.data.AppConfig
import com.vice_hotel.model.data.WifiCountry
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import com.vice_hotel.view.adapter.CityPickerAdapter
import kotlinx.android.synthetic.main.fragment_city_picker.*

class CityPickerFragment: BaseFragment() {

    override fun getLayoutRes(): Int = R.layout.fragment_city_picker

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        getCities()
    }

    private fun setUpListCountries(wifiCountries: ArrayList<WifiCountry>?) {
        if (wifiCountries!=null) {
            val adapter = CityPickerAdapter()
            adapter.addAll(wifiCountries)
            adapter.setOnSelectItem {
                val direction = CityPickerFragmentDirections.actionCityPickerFragmentToVenueCityFragment(it.cityCode)
                findNavController().navigate(direction)
            }
            RecyclerUtil.setUp(context, rclCities, adapter)
        }
    }
    private fun getCities() {
        ViewModelProviders.of(this).get(VenueModel::class.java)
            .getCities(context).observeOnce(this, Observer {
                if (it.isSuccess()) {
                    val list = it.data?.cities
                    setUpListCountries(list)
                }
            })
    }

}