package com.vice_hotel.view.fragment.venue

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.model.data.SearchData
import com.vice_hotel.presenter.helper.AppManager
import com.vice_hotel.presenter.helper.AppUtil
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.lib.retrofit.HTTP_OK
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.SearchVenueAdapter
import kotlinx.android.synthetic.main.fragment_search_venue.*
import java.util.*


class SearchVenuesFragment: BaseFragment() {

    private val searchData = SearchData()

    var adapter: SearchVenueAdapter? = SearchVenueAdapter()

    override fun getLayoutRes(): Int = R.layout.fragment_search_venue

    override fun onViewCreated(savedInstanceState: Bundle?) {
        edSearch.setText(searchData.searchText)
        btnFilter.setOnClickListener() {
            findNavController().navigate(R.id.action_searchVenuesFragment_to_categoryPickerFragment)
        }
        actionFilter.setOnClickListener() {
            findNavController().navigate(R.id.action_searchVenuesFragment_to_filterFragment)
        }
        RecyclerUtil.setUp(context, rclVenues, adapter, 2)
        listener()
        Handler().postDelayed({ searchVenues() }, 500)
    }

    private fun listener() {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        adapter?.setOnSelectedVenueListener {
            val direction =
                SearchVenuesFragmentDirections.actionSearchVenuesFragmentToVenueDetailFragment(it.id)
            findNavController().navigate(direction)
        }
        edSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId==EditorInfo.IME_ACTION_SEARCH) {
                searchData.searchText = v.text.toString()
                searchVenues()
                AppUtil.hideKeyboard(activity as BaseActivity)
            }
            false
        }
        edCloseSearch.setOnClickListener {
            edSearch.setText(null)
            searchData.searchText = null
            searchVenues()
        }
    }

    private fun searchVenues() {
        progressBar.visibility = View.VISIBLE
        if (searchData.brands!=null) {
            searchData.brands!!.clear()
        }
        searchData.brands = AppManager.instance.getSearchData().brands
        searchData.venue_status = AppManager.instance.getSearchData().venue_status
        ViewModelProviders.of(this).get(VenueModel::class.java).searchVenues(context, searchData).observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.statusCode == HTTP_OK) {
                val list = it.data?.venues
                Collections.sort(list, kotlin.Comparator { o1, o2 ->
                    o2.isOpen().compareTo(o1.isOpen())
                })
                adapter?.addAll(list)
                AppUtil.notifyDataChanged(rclVenues)
            }
        })
    }

}