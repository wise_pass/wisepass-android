package com.vice_hotel.view.fragment

import android.graphics.Color
import android.os.Bundle
import android.util.LayoutDirection
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AFInAppEventType
import com.appsflyer.AppsFlyerLib
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import com.braintreepayments.api.models.ClientToken
import com.vice_hotel.R
import com.vice_hotel.model.data.CardInfor
import com.vice_hotel.model.data.CheckOut
import com.vice_hotel.presenter.helper.*
import com.vice_hotel.presenter.lib.retrofit.ApiService
import com.vice_hotel.presenter.lib.retrofit.RetrofitUtil
import com.vice_hotel.presenter.plan_payment.BrainTreeUtil
import com.vice_hotel.presenter.plan_payment.CheckoutModel
import com.vice_hotel.presenter.plan_payment.PlanModel
import com.vice_hotel.presenter.plan_payment.PlanType
import com.vice_hotel.view.BaseActivity
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.activity.MainActivity
import com.vice_hotel.view.adapter.PaymentMethodsAdapter
import com.vice_hotel.view.fragment.menu.CreditCardActiveFragment
import com.vice_hotel.view.fragment.plan_payment.CardDiscoverySubscribeFragment
import com.vice_hotel.view.fragment.plan_payment.CardPremiumSubscribeFragment
import com.vice_hotel.view.fragment.plan_payment.CardStandardSubscribeFragment
import com.vice_hotel.view.widget.AppTextView
import kotlinx.android.synthetic.main.bottom_sheet_cvv.*
import kotlinx.android.synthetic.main.fragment_add_cards.*

class AddCardsFragment: BaseFragment() {

    private lateinit var adapter: PaymentMethodsAdapter

    override fun getLayoutRes(): Int = R.layout.fragment_add_cards

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener() { activity?.onBackPressed() }
        adapter = PaymentMethodsAdapter()
        adapter.setOnItemSelected { cardInfor ->
            DialogUtil.OpenCustomDialogCvv(activity) { cvvNumber ->
                DialogUtil.dismiss()
                if (cvvNumber.isNotEmpty()) {
                    setDefault(cardInfor.apply {
                        cvv = cvvNumber
                    })
                }
            }
        }
        actionAdd.setOnClickListener() {
            BrainTreeUtil.requestPayment(activity as MainActivity, this) {
                if (it) {
                    val eventValue = HashMap<String, Any>()
                    val email = PreferenceUtils(activity).get(PreferenceUtils.EMAIL_REGISTRATION, String.javaClass) as String
                    eventValue[AFInAppEventParameterName.CUSTOMER_USER_ID] = email
                    AppsFlyerLib.getInstance().trackEvent(activity!!.application, AFInAppEventType.ADD_TO_CART, eventValue)
                    activity?.onBackPressed()
                }
            }
        }
        RecyclerUtil.setUp(context, rclCards, adapter)
        getPlanBilling()

    }

    private fun getPlanBilling() {
        DialogUtil.OpenLoadingDialog(activity)
        ViewModelProviders.of(this).get(CheckoutModel::class.java).getPaymentMethodsHistories(context).observeOnce(this, Observer {
            DialogUtil.dismiss()
            if (it.isSuccess()) {
                val cards = it.data?.cards
                if (cards!=null && cards.size > 0) {
                    adapter.addAll(cards)
                } else {
                    actionAdd.performClick()
                }

            }
        })
    }

    private fun setDefault(cardInfor: CardInfor) {
        DialogUtil.OpenLoadingDialog(activity)
        val request = ApiService.create(context)?.setDefaultCard(cardInfor)
        RetrofitUtil.requestAPI(context, request) {
            DialogUtil.dismiss()
            if (it.isSuccess()) {
                activity?.onBackPressed()
            } else {
                AppUtil.toast(context, "CVV is not valid", false)
            }
        }
    }

}