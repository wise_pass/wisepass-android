package com.vice_hotel.view.fragment.venue

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.viewpager.widget.ViewPager
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.RecyclerUtil
import com.vice_hotel.presenter.helper.observeOnce
import com.vice_hotel.presenter.view_model.VenueModel
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.adapter.GalleryPhotoAdapter
import com.vice_hotel.view.adapter.PagerAdapter
import com.vice_hotel.view.fragment.other.PhotoFragment
import kotlinx.android.synthetic.main.fragment_venue_gallery.*

class VenueGalleryFragment: BaseFragment() {

    private val args: VenueGalleryFragmentArgs by navArgs()

    override fun getLayoutRes(): Int = R.layout.fragment_venue_gallery

    override fun onViewCreated(savedInstanceState: Bundle?) {
        actionBack.setOnClickListener { _ -> activity?.onBackPressed() }
        getVenueGalleries()
    }

    private fun getVenueGalleries() {
        ViewModelProviders.of(this).get(VenueModel::class.java).getVenueReadMoreById(context, args.venueId)
            .observeOnce(this, Observer {
                if (it.isSuccess()) {
                    when (it.data?.venue?.gallery!=null) {
                        true -> {
                            val photos = it.data?.venue?.gallery!!
                            setGallery(photos)
                            initPhotoPager(photos)
                        }
                    }

                }
            })
    }

    private fun initPhotoPager(photos: ArrayList<String>) {
        val adapter = PagerAdapter(childFragmentManager)
        for (url in photos) {
            adapter.addFragment(PhotoFragment.newInstance(url))
        }
        photoViewPager.adapter = adapter
        photoViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                rclGallery.smoothScrollToPosition(position)
            }

        })
    }

    private fun setGallery(photos: ArrayList<String>) {
        val galleryAdapter = GalleryPhotoAdapter()
        galleryAdapter.setOnClickItem {
            photoViewPager.setCurrentItem(it, true)
        }
        RecyclerUtil.setUpHorizontal(context, rclGallery, galleryAdapter)
        galleryAdapter.addPhotos(photos)
    }

    private fun getPhotos() {
        if (AccessToken.getCurrentAccessToken()!=null) {
            GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(),"/1715524032001146/photos/uploaded") { response ->
                response.toString()
                val json = response.jsonObject
                val ids = ArrayList<String>()
                if (json!=null) {
                    val jsonData = json.getJSONArray("data")
                    for (index in 0..jsonData.length()) {
                        val jsonPhoto = jsonData.getJSONObject(index)
                        val id = jsonPhoto.getString("id")
                        ids.add(id)
                    }
                }
                //https://graph.facebook.com/2410269329193276/picture
            }.executeAsync()
        }

    }
}