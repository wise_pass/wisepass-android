package com.vice_hotel.view.fragment.venue

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.vice_hotel.R
import com.vice_hotel.databinding.FragmentSlidePagerVenueBinding
import com.vice_hotel.model.data.Category
import com.vice_hotel.model.data.Venue
import com.vice_hotel.view.BaseFragment
import com.vice_hotel.view.fragment.DashboardFragmentDirections
import com.vice_hotel.view.widget.AppTextView
import kotlinx.android.synthetic.main.fragment_slide_pager_venue.*

private const val KEY = "slide_pager"
class SlidePagerVenueFragment: BaseFragment() {

    companion object {
        fun newInstance(venue: Venue?) = SlidePagerVenueFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY, venue)
            }
        }
    }

    override fun getLayoutRes(): Int = R.layout.fragment_slide_pager_venue

    override fun onViewCreated(savedInstanceState: Bundle?) {
        val venue = arguments?.getParcelable(KEY) as Venue
        val binding: FragmentSlidePagerVenueBinding? = DataBindingUtil.bind(fragmentView!!)
        binding?.venue = venue
        loadCategories(venue.categories)
        btnLoadDetail.setOnClickListener() {
            val directions = DashboardFragmentDirections.actionDashboardFragmentToVenueDetailFragment(venue.id)
            findNavController().navigate(directions)
        }
    }

    private fun loadCategories(categories: ArrayList<Category>?) {
        if (categories!=null) {
            val size = categories.size
            if (size > 0) {
                for (index in 0 until size) {
                    val tagView = LayoutInflater.from(context).inflate(R.layout.view_category_tag, null, false) as View
                    tagView.findViewById<AppTextView>(R.id.txtView).text = categories[index].displayName
                    containerTagCategories.addView(tagView)
                }
            }
        }
    }
}