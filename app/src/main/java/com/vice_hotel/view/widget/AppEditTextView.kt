package com.vice_hotel.view.widget

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import com.vice_hotel.presenter.helper.FontUtil

open class AppEditTextView(context: Context?, attrs: AttributeSet?): EditText(context, attrs) {

    init {
        typeface = FontUtil.textRegular(context)
    }


}