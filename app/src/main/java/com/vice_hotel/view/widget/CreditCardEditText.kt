package com.vice_hotel.view.widget

import android.content.Context
import android.util.AttributeSet
import android.util.SparseArray
import androidx.core.widget.doAfterTextChanged
import java.util.regex.Pattern
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import android.text.Editable
import androidx.core.widget.doOnTextChanged
import com.vice_hotel.presenter.lib.braintree.CardType
import kotlinx.android.synthetic.main.fragment_card_detail.*

class CreditCardEditText(context: Context?, attrs: AttributeSet?): AppEditTextView(context, attrs) {

    private var detectCardType: ((CardType) -> Unit)? = null

    init {
//        setOnKeyListener { v, keyCode, event ->
//            setSelection(text.length)
//            false
//        }
    }


    override fun onTextChanged(s: CharSequence?, start: Int, lengthBefore: Int, after: Int) {
        super.onTextChanged(text, start, lengthBefore, after)
        // check type of Card
        detectCardType?.invoke(CardType.detect(s.toString().trim()))
        //
//        if (after==1) {
//            val charLast = s!!.substring(s.length - 1)
//            if (s.length==5 || s.length==12 || s.length==19) {
//                val str = "${s.substring(0, s.length-1)} - $charLast"
//                setText(str)
//            }
//        } else {
//            if (s!!.length==7 || s.length==14 || s.length==21) {
//                val str = "${s.substring(0, s.length-3)}"
//                setText(str)
//            }
//        }
//        setSelection(text.length)
    }

    fun setOnCompleteDetectCardType(cardType: ((CardType) -> Unit)?) {
        this.detectCardType = cardType
    }



}