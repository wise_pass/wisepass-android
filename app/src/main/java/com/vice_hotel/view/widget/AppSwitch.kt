package com.vice_hotel.view.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.FrameLayout
import androidx.appcompat.widget.SwitchCompat
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.AppUtil
import kotlinx.android.synthetic.main.view_switch.view.*
import java.util.*

class AppSwitch: FrameLayout {

    var view: View? = null
    var listener: ((Boolean) -> Unit)? = null

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs)

    init {
        view = LayoutInflater.from(context).inflate(R.layout.view_switch, null)
        addView(view, LayoutParams(AppUtil.dpToPx(context, 55)!!.toInt(), ViewGroup.LayoutParams.WRAP_CONTENT))
        val swOptions = view?.findViewById<SwitchCompat>(R.id.swOptions)
        val yes = view?.findViewById<AppTextView>(R.id.txtYes)
        val no = view?.findViewById<AppTextView>(R.id.txtNo)
        swOptions?.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                listener?.invoke(p1)
                if (p1) {
                    yes?.setTextColor(Color.parseColor("#ffffff"))
                    no?.setTextColor(Color.TRANSPARENT)
                } else {
                    no?.setTextColor(Color.parseColor("#482b88"))
                    yes?.setTextColor(Color.TRANSPARENT)
                }
            }

        })
        tag = UUID.randomUUID().toString()
    }

    fun setOnAppCheckedChangeListener(listener: ((Boolean) -> Unit)?) {
        this.listener = listener
    }

    fun getValue(): Boolean = swOptions.isChecked

    fun setValue(isYes: Boolean) {
        swOptions.isChecked = isYes
    }

}