package com.vice_hotel.view.widget

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView
import com.vice_hotel.R
import com.vice_hotel.presenter.helper.FontUtil

class AppTextView(context: Context?, attrs: AttributeSet?): TextView(context, attrs) {

    init {
        custom(context, attrs)
    }

    fun custom(context: Context?, attrs: AttributeSet?) {
        typeface = FontUtil.textSemibold(context)
        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.widget,
            0, 0)

        try {
            val font = a?.getInteger(R.styleable.widget_app_font, 0)
            if (font!=null) {
                typeface = when (font) {
                    6 -> FontUtil.textRegular(context)
                    7 -> FontUtil.textMedium(context)
                    8 -> FontUtil.textLight(context)
                    9 -> FontUtil.textBold(context)
                    10 -> FontUtil.textSemibold(context)
                    else -> FontUtil.textRegular(context)
                }
            }

        } finally {
            a?.recycle()
        }
    }

}