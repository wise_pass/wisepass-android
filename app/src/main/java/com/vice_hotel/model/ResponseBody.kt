package com.vice_hotel.model

import com.vice_hotel.presenter.lib.retrofit.HTTP_OK

data class ResponseBody (
    var message: String? = null,
    var statusCode: Int? = null,
    var statusCodeExactly: Int? = null,
    val data: ResponseData? = null
) {
    fun isSuccess(): Boolean = when (statusCode) {
        HTTP_OK -> true
        else -> false
    }
}