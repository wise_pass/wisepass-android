package com.vice_hotel.model

import com.vice_hotel.model.data.*

data class ResponseData (
    val update_code: Int? = null,
    val account: UserAccount? = null,
    val user_profile: UserAccount? = null,
    val validate_barcode: ScanType? = null,
    val scan_venue_items: Venue? = null,
    val history_items: HistoryItem? = null,
    val available_coins: AvailableCoin? = null,
    val scan_confirm: ScanConfirm? = null,
    val plan: Plan? = null,
    val payment_plans_promo: Plan? = null,
    val card: CardInfor? = null,
    val cards: ArrayList<CardInfor>? = null,
    val payment_plans: ArrayList<Plan>? = null,
    val venues: ArrayList<Venue>? = null,
    val slideMenu: ArrayList<Venue>? = null,
    val suggestions: ArrayList<Venue>? = null,
    val categories: ArrayList<Category>? = null,
    val nearby: ArrayList<Venue>? = null,
    val trending: ArrayList<Venue>? = null,
    val favorites: ArrayList<Venue>? = null,
    val recentlyAdded: ArrayList<Venue>? = null,
    val listItems: ArrayList<Venue>? = null,
    val bestSpots: ArrayList<Venue>? = null,
    val discovery: ArrayList<Venue>? = null,
    val brands: ArrayList<Brand>? = null,
    val promo: ArrayList<Reward>? = null,
    val venue: Venue? = null,
    val token: String? = null,
    val letPass: LetPass? = null,
    val checkout: PlanCheckOut? = null,
    val userReferral: UserReferral? = null,
    var challenge: Challenge? = null,
    var challenges: ArrayList<Challenge>? = null,
    var cities: ArrayList<WifiCountry>? = null
)