package com.vice_hotel.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WorkingHour (
    var dayInWeek: String? = null,
    var time: String? = null
): Parcelable