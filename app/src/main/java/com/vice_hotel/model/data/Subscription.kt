package com.vice_hotel.model.data

data class Subscription (
    var subscriptionId: String? = null
)