package com.vice_hotel.model.data

data class LetPass (
    var type: String? = null
) {
    companion object {
        const val PLAN = "plan"
        const val REWARD = "reward"
    }
}