package com.vice_hotel.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WifiCountry (
    var httpCode: Int? = 0,
    val countryCode: String? = null,
    val country: String? = null,
    val flagUrl: String? = null,
    val city: String? = null,
    var cityCode: String? = null,
    var status: Boolean? = null,
    var lat: Double? = null,
    var lon: Double? = null
): Parcelable