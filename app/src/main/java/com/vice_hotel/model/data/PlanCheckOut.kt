package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData

data class PlanCheckOut (
    var id: String? = null,
    var name: String? = null,
    var passAvailable: String? = null,
    var dateRenew: String? = null,
    var price: String? = null,
    var priceNumber: Number? = null,
    var currencyText: String? = null,
    var billing: String? = null,
    var promotion: String? = null,
    var total: String? = null
): BaseData()