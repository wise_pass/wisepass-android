package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData

data class Payment (
    var planId: String? = null,
    var paymentMethodToken: String? = null
):BaseData()