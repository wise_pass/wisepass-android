package com.vice_hotel.model.data

import android.os.Parcelable
import com.vice_hotel.model.BaseData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Plan (
    var id: String? = null,
    var type: String? = null,
    var status: String? = null,
    var usedTime: String? = null,
    var validity: String? = null,
    var business: String? = null,
    var accessPoint: String? = null,
    var promotion: String? = null,
    var planNextText: String? = null,
    var price: String? = null,
    var currency: String? = null,
    var timeAvailable: String? = null,
    var expired: String? = null,
    var currencyPerTime: String? = null,
    var countryName: String? = null,
    var subscriptionId: String? = null,
    var subscription: String? = null,
    var isAutorenew: Boolean? = null,
    var isUpgrade: Boolean? = null
): BaseData(), Parcelable {
    val isNonePrice: Boolean
    get() = when (price) {
        "0" -> true
        else -> false
    }
    val isChangeNextBilling: Boolean
    get() = when (planNextText?.trim()?.toLowerCase()) {
        type?.trim()?.toLowerCase() -> false
        else -> true
    }
}
