package com.vice_hotel.model.data

data class AppConfig (
    var push_token : String? = null,
    var device_id: String? = null,
    var app_version: String? = null,
    var app_version_code: Int? = null,
    var os_name: String? = null,
    val platform: String = "android",
    var country_code: String? = null,
    var latitude : Double? = null,
    var longitude : Double? = null
)