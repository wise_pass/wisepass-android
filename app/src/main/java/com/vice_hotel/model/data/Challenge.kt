package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData
import com.vice_hotel.presenter.helper.AppUtil
import java.text.DecimalFormat

data class Challenge(
    var id: String? = null,
    var memberChallengeId: String? = null,
    var name: String? = null,
    var level: String? = null,
    var availableDate: String? = null,
    var progress: Int? = null,
    var maxValue: Int? = null,
    var description: String? = null,
    var expired: String? = null,
    var claimAvailable: Boolean? = null,
    var imageUrl: String? = null,
    var conditions: ArrayList<Condition>? = null
): BaseData() {
    val expiredString: String?
    get() = AppUtil.convertDateFromServer(expired)

    val percent: String
    get() {
        if (progress!=null && maxValue!=null) {
            val percent: Double = (progress!!.toDouble() / maxValue!!.toDouble()) * 100
            val percentString = DecimalFormat("0.0").format(percent)
            return "$percentString%"
        }
        return "0.00%"
    }
}