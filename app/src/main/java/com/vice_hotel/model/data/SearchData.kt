package com.vice_hotel.model.data

data class SearchData (
    var searchText: String? = null,
    var latitude: String? = null,
    var longitude: String? = null,
    var countryCode: String? = null,
    var keyType: String? = null,
    var brands: ArrayList<Brand>? = null,
    var venue_status: String? = null,
    var distanceFrom: String? = null,
    var distanceTo: String? = null,
    var pageSize: String? = null,
    var pageNum: String? = null
)