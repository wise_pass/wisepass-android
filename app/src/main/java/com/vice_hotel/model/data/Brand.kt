package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData

data class Brand (
    var logo: String? = null,
    var background: String? = null,
    var display: String? = null,
    var keyword: String? = null,
    var section: String? = null,
    var checked: Boolean? = null
): BaseData() {
    fun isCheck(): Boolean? = when (checked) {
        null -> false
        else -> checked
    }
}