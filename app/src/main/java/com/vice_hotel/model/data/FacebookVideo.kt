package com.vice_hotel.model.data

data class FacebookVideo (
    var urlImage: String? = null,
    var source: String? = null
)