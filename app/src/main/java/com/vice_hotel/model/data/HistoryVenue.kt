package com.vice_hotel.model.data

data class HistoryVenue (
    var productName: String? = null,
    var productType: String? = null,
    var productImage: String? = null,
    var createdTime: String? = null,
    var passValue: String? = null,
    var venueName: String? = null,
    var venueImage: String? = null,
    var categoryKeyword: String? = null

)