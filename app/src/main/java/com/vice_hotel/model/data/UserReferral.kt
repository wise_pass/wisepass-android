package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData

data class UserReferral (
    var title: String? = null,
    var imageUrl: String? = null,
    var code: String? = null,
    var content: String? = null,
    var termConditionLink: String? = null
): BaseData()