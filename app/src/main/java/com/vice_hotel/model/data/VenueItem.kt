package com.vice_hotel.model.data

import android.os.Parcelable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.vice_hotel.model.BaseData
import com.vice_hotel.presenter.lib.glide.GlideUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VenueItem (
    var id: String? = null,
    var userType: String? = null,
    var userTypeCode: String? = null,
    var name: String? = null,
    var imageUrl: String? = null,
    var isAvailable: Boolean? = null,
    var timeAvailable: String? = null,
    var dateAvailable: String? = null,
    var categoryKeyword: String? = null,
    var categoryId: String? = null
): BaseData(), Parcelable