package com.vice_hotel.model.data

data class BTNonce (
    var nonce: String? = null
)