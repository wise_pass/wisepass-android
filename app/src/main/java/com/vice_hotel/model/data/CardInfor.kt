package com.vice_hotel.model.data

import android.os.Parcelable
import com.vice_hotel.model.BaseData
import com.vice_hotel.presenter.helper.Constant
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardInfor(
    var id: String? = null,
    var name: String? = null,
    var expired: String? = null,
    var cardDefault: String? = null,
    var braintreeUserId: String? = null,
    var cardholderName: String? = null,
    var last4Digits: String? = null,
    var customerId: String? = null,
    var uniqueNumberIdentifier: String? = null,
    var cardNumber: String? = null,
    var cardTypeID: Int? = null,
    var cvv: String? = null,
    var tokenID: String? = null,
    var isDefault: Boolean? = null,
    var expirationDate: String? = null,
    var cardId: String? = null,
    var imageUrl: String? = null,
    var token: String? = null,
    var lastFour: String? = null,
    var maskedNumber: String? = null,
    var expiredDate: String? = null
) : BaseData(), Parcelable {

    companion object {
        const val DEFAULT_YES = "YES"
        const val DEFAULT_NO = "NO"
    }

    val cardName: String
    get() = when(cardholderName.isNullOrEmpty()) {
        true -> ""
        false -> cardholderName!!
    }
    val isCardDefault: Boolean
    get() = when (isDefault) {
        null -> false
        else -> isDefault!!
    }


}