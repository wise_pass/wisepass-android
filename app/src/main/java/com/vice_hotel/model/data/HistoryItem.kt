package com.vice_hotel.model.data

data class HistoryItem (
    var pageNumber: Int? = 0,
    var total: Int? = 0,
    var pageSize:Int? = 0,
    var listHistory: ArrayList<HistoryVenue>? = null
)