package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData

data class CheckOut (
    var planId: String? = null,
    var token: String? = null
)