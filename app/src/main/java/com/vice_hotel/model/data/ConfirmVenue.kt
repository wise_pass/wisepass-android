package com.vice_hotel.model.data

data class ConfirmVenue (
    var productInBusinessVenueId: String? = null
)