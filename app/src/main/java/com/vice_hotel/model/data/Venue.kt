package com.vice_hotel.model.data

import android.os.Parcelable
import com.vice_hotel.model.BaseData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Venue (
    var id: String? = null,
    var venueId: String? = null,
    var logo: String? = null,
    var background: String? = null,
    var name: String? = null,
    var address: String? = null,
    var phoneNo: String? = null,
    var timeOpenning: String? = null,
    var status: String? = null,
    var description: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var conditionTitle: String? = null,
    var conditionContent: String? = null,
    var conditionEmail: String? = null,
    var conditionPhoneNo: String? = null,
    var keyType: String? = null,
    var isOpen: Boolean? = null,
    var categories: ArrayList<Category>? = null,
    var gallery: ArrayList<String>? = null,
    var details: String? = null,
    var facebook: String? = null,
    var workingHour: ArrayList<WorkingHour>? = null
): BaseData(), Parcelable {
    fun isOpen(): Boolean = when (status?.toLowerCase()) {
        "OPEN".toLowerCase() -> true
        "CLOSE".toLowerCase() -> false
        else -> when (isOpen) {
            null -> false
            else -> isOpen!!
        }
    }

    fun categoriesDisplay(): String? {
        var string = ""
        if (categories!=null) {
            for (index in 0 until categories!!.size) {
                if (index == 0) {
                    string = categories!![index].displayName!!
                } else {
                    string += " - ${categories!![index].displayName}"
                }
            }
        }
        return string
    }
}