package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData

data class UserAccount (
    var status: String? = null,
    var token: String? = null,
    var user_id: String? = null,
    var name: String? = null,
    var full_name: String? = null,
    var package_name: String? = null,
    var email: String? = null,
    var profile_img: String? = null,
    var image_base64: String? = null,
    var image_url: String? = null,
    var exp_date: String? = null,
    var phone_number: String? = null,
    var birthday: String? = null,
    var gender: String? = null,
    var is_auto_renewal: Boolean? = null,
    var referal_code: String? = null,
    var is_new_subscriber: Boolean? = null
): BaseData()