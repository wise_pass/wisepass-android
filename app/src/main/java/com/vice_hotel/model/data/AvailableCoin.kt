package com.vice_hotel.model.data

data class AvailableCoin (
    var coins: Int? = null,
    var message: String? = null
)