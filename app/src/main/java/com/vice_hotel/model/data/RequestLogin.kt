package com.vice_hotel.model.data

data class RequestLogin (
    var email: String? = null,
    var otp: String? = null,
    var facebookId: String? = null,
    var facebookAccessToken: String? = null,
    var facebookEmail: String? = null,
    var udid: String? = null
)