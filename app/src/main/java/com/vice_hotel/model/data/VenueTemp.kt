package com.vice_hotel.model.data

data class VenueTemp (
    var id: String? = null,
    var logo: String? = null,
    var background: String? = null,
    var name: String? = null,
    var isOpen: Boolean? = null
    )