package com.vice_hotel.model.data

data class Reward (
    var id: String? = null,
    var name: String? = null,
    var level: String? = null,
    var availableDate: String? = null,
    var percently: Int? = null,
    var seconds: Long? = null,
    var details: String? = null,
    var imageUrl: String? = null
)