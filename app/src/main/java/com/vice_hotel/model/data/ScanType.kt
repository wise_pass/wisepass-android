package com.vice_hotel.model.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScanType (
    var type: String? = null,
    var id: String? = null,
    var venueId: String? = null,
    var promoId: String? = null,
    var productInBusinessVenueId: String? = null
): Parcelable {
    companion object {
        const val PROMO = "promo"
        const val REWARD = "reward"
        const val VENUE = "venue"
        const val PLAN = "plan"
        const val USER = "user"
    }
}