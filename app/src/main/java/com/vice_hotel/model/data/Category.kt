package com.vice_hotel.model.data

import android.os.Parcelable
import com.vice_hotel.model.BaseData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category (
    var keyword: String? = null,
    var displayName: String? = null,
    var logo: String? = null,
    var name: String? = null,
    var section: String? = null,
    var checked: Boolean? = null,
    var items: ArrayList<VenueItem>? = null
): BaseData(), Parcelable