package com.vice_hotel.model.data

data class ValidateBarcode (
    var id: String? = null,
    var type: String? = null
)