package com.vice_hotel.model.data

import com.vice_hotel.model.BaseData
import com.vice_hotel.presenter.helper.AppUtil

data class Condition (
    var Id: String? = null,
    var currentProcess: Int? = null,
    var maxProcess: Int? = null,
    var name: String? = null,
    var progress: Int? = null,
    var maxValue: Int? = null,
    var expired: String? = null,
    var description: String? = null
): BaseData() {
    val expiredString: String?
        get() = AppUtil.convertDateFromServer(expired)
}