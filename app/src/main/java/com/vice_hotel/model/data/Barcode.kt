package com.vice_hotel.model.data

data class Barcode (
    var barcode: String? = null,
    var promoCode: String? = null
)