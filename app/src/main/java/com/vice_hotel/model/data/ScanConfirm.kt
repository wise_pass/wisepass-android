package com.vice_hotel.model.data

import android.os.Parcelable
import com.vice_hotel.model.BaseData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScanConfirm (
    var id: String? = null,
    var idLoggingScanPass: String? = null,
    var productName: String? = null,
    var productConfirmType: String? = null,
    var countDownSeconds: Int? = 0,
    var imageUrl: String? = null,
    var createdDate: String? = null,
    var code: String? = null
): BaseData(), Parcelable